# smogcli2 is a command-line rtl-sdr to IQ file saver and telemetry decoder for the pocketqube satellites SMOG-1, SMOG-P (MO-105) and ATL-1 (MO-106)

## Quick start notes:
Currently, SMOG-1 is up and running well. tle-new.txt from celestrak.com provides good TLEs. Based on current observations, object #47943 (2021-022M) provides good agreement. smogcli can use tle-new.txt and will recognize IQ recordings starting with the name 2021-022 during the upload sequence. The following commands enable automated IQ recording, decoding and uploading (assuming you've set up the appropriate qthfile.txt, see below):

```
smog1_rtl_rx -i 47943 -k -n
smog1_decode -l -p <filename.cf32>
python <path_to_the_scripts_directory>/smog_upload.py <filename.pkts>
```

## General description

It will automatically save the Doppler-compensated received samples in CF32 (complex 32-bit interleaved floats) 
format whenever the satellite is above the horizon. The decoder uses sophisticated synchronization algorithms to find and
extract possible telemetry frames with potentially large carrier frequency offset. It will demodulate the GFSK modulated signal 
using the Viterbi algorithm, then decode all four possible FEC frame format using high quality FEC decoders. It can upload the
decoded frames to the BME telemetry collection server as well.
smogcli2 runs on Linux (including Raspberry Pi 2 and newer), OS X and Windows, and can handle multiple low-cost rtl-sdr dongles to 
build simple, fully autonomous telemetry receiver stations.

## Other decoding options
SMOG-1 is officially supported by the [gr-satellites](https://github.com/daniestevez/gr-satellites) collection under GNU Radio (both in the master and maint-3.8 branches). 

It is also possible to record IQ files using your favorite SDR receiving tool and have the recording decoded by smogcli2. The recording needs to be converted to raw format (interleaved 32-bit floats, the so-called cf32 format). smogcli2 includes a simple tool to convert WAV files to cf32 format, see below. 

## About the satellites

[SMOG-1]
SMOG-1 is expected to launch in March 2021 aboard a Soyuz-2 rocket.

[SMOG-P (MO-105) AND ATL-1 (MO-106)](https://www.amsat.org/smog-p-and-atl-1-designated-magyar-oscar-105-mo-105-and-magyar-oscar-106-mo-106/)

On December 6, 2019, Budapest University of Technology and Economics (BME) SMOG-P and ATL-1 PocketQubes were launched on an Electron launch vehicle from the Mahia Launch Complex in New Zealand. SMOG-P and ATL-1 were developed as part of the university curriculum and operated in cooperation with the HA5MRC Technical University amateur radio club. The satellites carry spectrum monitoring payloads and are currently active. At the request of the Technical University of Budapest, AMSAT hereby designates SMOG-P as Magyar-OSCAR 105 (MO-105), and ATL-1 as Magyar-OSCAR 106 (MO-106). We congratulate the owners and operators, thank them for their contribution to the amateur satellite community, and wish them a long mission and continued success on this and future projects. 73,Drew Glasbrenner, KO4MA, AMSAT VP Operations / OSCAR Number Administrator
SMOG-P (MO-105)
is a 1p PocketQube (5x5x5 cm, 250 grams), a fully redundant tiny satellite with an actual scientific payload: a flying spectrum analyzer. It measures the scattered RF energy over the UHF band (specifically, in the digital terrestrial TV band) that can be detected in space. It has decayed in 2020.

ATL-1 (MO-106)
is a larger 2p PocketQube featuring the same spectrum analyzer experiment. It has decayed in 2020.

All three satellites transmit (used to transmit) almost identical telemetry data. In addition to basic CW telemetry carrying callsign, battery voltage and temperature, there is a digital telemetry with variable data rate and coding scheme. Most frequently, modulation is 1250 or 5000 bps GMSK. The data is encoded either by the well-known "AO-40" FEC, or a shorter, proprietary variant of it, but they can also use a more powerful, state-of-art repeat-accumulate (RA) coding scheme.

# Build guide

## Linux
Install the `rtl-sdr` development packages, and `curl` development packages (on Ubuntu, 
`apt install librtlsdr-dev libcurl4-gnutls-dev cmake git libfftw3-dev libjsoncpp-dev`). 
Use `cmake` to build:
<pre><code>git clone https://gitlab.com/phorvath/smogcli2.git
cd smogcli2
mkdir build
cd build
cmake ..
make
sudo make install
</code></pre>
Run the `smog_tests` binary to see some performance stats.

## OS X

### Using Homebrew/macports
Install libusb, librtlsdr and libcurl from Homebrew or macports (or build them from source). Then
<pre><code>mkdir build
cd build
cmake ..
make
make install
</code></pre>

## Raspberry Pi 3 or newer:

[More detailed instructions below](#raspberry-pi-notes)

Install the `rtl-sdr` development packages, and `curl` development packages (on Raspbian, 
`apt install libcurl4-gnutls-dev cmake git libfftw3-dev libjsoncpp-dev`). 
Use `cmake` to build:
<pre><code>git clone https://gitlab.com/phorvath/smogcli2.git
cd smogcli2
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE=cmake/arm_cortex_a53-native.cmake ..
make
sudo make install
</code></pre>

WARNING: Older Pi hardware is probably not powerful enough to record two channels simultaneously. Raspberry Pi 2 has been tested and
works well with a single satellite. Pi Zero seems to be okay as well, however, the original Pi is not good enough, as it lacks the
necessary vectorized math extensions (NEON).

# Windows
A cmake toolchain file is included for the brave to be used with 64 bit versions of MinGW. However, libusb, librtlsdr and libcurl need to be installed properly. 
Windows binaries for the faint-hearted can be found here:
[link](https://home.hvt.bme.hu/~ha5cqa/smogcli/win64/)
The zip file at this link contains everything you'll need to run the software. Decompress it to a folder somewhere, and run the programs from the
command prompt (or, even better, from Windows PowerShell) as described below. Wildcard file names might not work on Windows, we are investigating, why.
For the packet upload you'll need Python 3 and the requests package. Recent Win10 versions will guide you to the Windows Store and offer
a download of Python 3 when you try to launch python3 from the command line:
<pre><code>
python3
</code></pre>
Install python3 from the Windows Store, then request the installation of the requests package:
<pre><code>
pip3 install requests
</code></pre>

# Usage

NOTE: the obsolete commands for the two decayed satellites start with smogp_xxx (i.e., `smogp_rtl_rx` etc.) whereas the current commands start with smog1_xxx (i.e., `smog1_rtl_rx`, `smog1_decode`, `smog1_packets`).

The typical usage involves three steps: 
1. Recording the passes using `smog1_rtl_rx`
2. Decoding the frames with `smog1_decode`  
3. Upload the decoded packets to the server at Budaoest University of Technology and Economics
Uploading requires you to register a user name and password at [link](https://gnd.bme.hu:8080/index). The number of decoded packets is shown
on our [leaderboard](https://gnd.bme.hu:8080/leaderboard)
More details on these steps are given below. 
4. Investigate the telemetry locally in a human-readable format using `smog1_packets`

## Specify your location

In order for smog_rtl_rx to calculate the Doppler compensation, the location of your station is required.
Create a file called qthfile.txt with your geographical location in the same directory as `smog1_rtl_rx` is run from. 
A sample [qthfile.txt](https://gitlab.com/phorvath/smogcli2/-/blob/master/qthfile.txt) is included for reference. 
qthfile.txt should contain
- A free-form identifier (e.g., your callsign) - this will appear verbatim at the end of the file name in every recording
- Latitude in decimal format (N: positive value, S: negative value)
- Longitude in decimal format (E: negative value, W: positive value)
- Altitude in meters
- Your BME username as obtained from the registration process at [link](https://gnd.bme.hu:8080/index)
- Your BME password
The last two lines are not mandatory if you don't want to upload the packets to us. However, it would be a huge help especially if you live
outside Europe where we do not have too many regular observers.

## Provide TLE data

The program tries to read `tle.txt`, containing 2-line Kepler elements, in the same directory. With the `-k` switch, the program will try 
to download the most recent TLE file `cubesat.txt` and store it as `tle.txt` in the dirrent directory. If the switch `-a` is also given, 
the complete TLE database `active.txt` is downloaded instead. (Beware, it is over 400KB.)
By default, the TLE file will be automatically re-downloaded a couple of times per day.
Also, `smog1_rtl_rx` watches the TLE file and re-reads it when a change is detected. 

## Record

<pre><code>pi@raspberrypi:~/smogcli2/build $ ./smog1_rtl_rx -k -g 45 -p -1
INFO: trying to download TLE from celestrak.com...
INFO: TLE (active.txt) downloaded successfully
INFO: TLE and QTH files loaded
INFO: found TLE data for SMOG-P (44832)
INFO: found TLE data for ATL-1 (44830)
INFO: TLE age is 0 days
INFO: found 1 device(s):
0: Realtek RTL2838UHIDIR, SN: 00000001
INFO: using device 0: Generic RTL2832U OEM
Found Rafael Micro R820T tuner
Exact sample rate is: 2000000.052982 Hz
[R82XX] PLL not locked!
INFO: setting dongle sample rate to 2000000
INFO: setting fixed gain 445
INFO: disabled bias-T on GPIO PIN 0
INFO: setting dongle center frequency to 437.095 MHz
INFO: setting dongle ppm correction to -1
INFO: real center frequency is 437095000.0 Hz
INFO: starting dongle thread with 1048576 bytes of buffer
Allocating 15 zero-copy buffers
INFO: SMOG-P azimuth: 2.17108, elevation: -40.1313, doppler: 8058.68 Hz
INFO: ATL-1 azimuth: 359.268, elevation: -50.4449, doppler: 6680.11 Hz
INFO: maximum signal amplitude: 0.25
</code></pre>

The program will start writing a binary IQ file whenever one of the satellites is above -2 degrees elevation. 
The file name is automagically generated from the name of the satellite, the absolute time and the QTH name. 
When the satellite passes, the file will be closed and the program waits for a new pass. The default mode of operation 
assumes a sample rate of 1.6 Msps, which will be decimated to yield 50 ksps at the output. The received passband is flat 
up to 18 kHz and is steeply cut above that frequency. If the switch `-S` is also given, the sample rate will be increased 
to 2 Msps to obtain an output sample rate of 62.5 ksps. The output format is 
FC32 (i.e., interleaved 32-bit float IQ values). [Inspectrum](https://github.com/miek/inspectrum) is highly recommended to 
inspect the contents of the output files.

<pre><code>
SMOG-1 recorder for RTL2832 based DVB-T receivers

Usage: smog1_rtl_rx [-options]
	-d device_index (default: 0)
	-T enable bias-T on GPIO PIN 0 (works for rtl-sdr.com v3 dongles)
	-g tuner gain (default: automatic, NOT RECOMMENDED)
	-p kalibrate-sdr reported fractional ppm error (default: 0.0)
	-f forced continuous recording, no Doppler correction
	-i track the given primary satellite ID (default: 0)
	-F downlink frequency for primary sat (default: 437345000.0 Hz)
	-k download TLE data from celestrak.com
	-S use 2 Msps/62.5 ksps mode (default: 1.6 Msps/50 ksps)
	-O dump downconverted samples to STDOUT in binary cf32 format
	-b disable the 1/128 rescaling of raw samples
	-n download tle-new.txt instead of cubesat.txt from celestrak.com
	-a download active.txt instead of cubesat.txt from celestrak.com
	-l enable azimuth/elevation logging (default: false)
	-s disable printing sat and signal statistics (default: false)
	-B file buffer size, useful for slow SD cards (default: 4 Mb)
	-e elevation limit for start recording (default: -2 deg)
	-h prints this help message
</code></pre>

Other options are self-explanatory. The dongle clock correction can be **fractional**. 
You can use [kalibrate-rtl](https://github.com/steve-m/kalibrate-rtl) to obtain a calibration value for your dongle. 
(And better buy a TCXO model. TCXO dongles will probably not need any calibration or correction.) The decoder can cope with a large
carrier frequency offset though, therefore ppm compensation is not really necessary for most uses.

You can specify any other satellite with its corresponding downlink frequency (useful for testing purposes). The program is also 
able to write WAV files if you want to replay them i.e., in SDRSharp (or whatever its current name is). It might be better to stick
with the CF32 output format, and convert it to WAV if necessary (use the provided tool `smog_fc2wav`)

By default, the dongle will operate in automatic gain control mode. The AGC of the rtl-sdr is meant to handle wideband signals, 
therefore it might act weirdly on weak narrowband signals. Experience shows that, in the absence of signals, auto gain will yield a 
somewhat higher gain setting than the maximum gain one can set manually. You can specify a fixed manual gain and probably you should do so.

You might leave the program running ad infinitum. We'd suggest running it from `tmux` or `screen` because the program keeps spitting out 
stuff to the stdout. Ctrl-C terminates the program. If you don't want to see the azimuth/elevation

The option -O enables an independent downconverter chain, and dumps the downconverted samples to the standard output. 
This feature might be used to pipe received samples to OpenWebRX for visualization. The center frequency is in the middle between the 
nominal frequencies of SMOG-P and ATL-1 (437.1625 MHz), the sample rate is 200 ksps, and the pass pand is flat to +/- 75 kHz. 
It is therefore recommended to clip the frequency plots at +/- 75 kHz from the center frequency.

## Decode

`smogp_decode` can be used to search for telemetry packets in the recording and decode them. 

<pre><code>
pi@raspberrypi:~/smogcli2/build $ smog1_decode -h
SMOG-1 demodulator and decoder

Usage: smog1_decode [-options] [filenames]
	-T runs internal test
	-b bits per second (default: 1250, 2500, 5000 and 12500 bps)
	-r sampling rate of input (default: 50000 sps)
	-t tone detection sensitivity in dB (default: 2 dB)
	-s sync detection sensitivity in dB (default: 1 dB)
	-d data detection sensitivity in dB (default: 0 dB)
	-C disable CRC checking (default: off)
	-D disable Viterbi demodulator (default: off)
	-p write packet files instead of stdout (default: off)
	-S print raw samples for each packet (default: off)
	-Y decode only sync packets (default: off)
	-l limit the search to freq offset (default: 25000 Hz)
	-h prints this help message
</code></pre>

Usage is straightforward. Supply the recording(s) as argument(s) (wildcards are accepted so that multiple files can be handled in a single pass). It will try to recognize the file format based on its extension (cf32 or cs16). By default, the program will search for every legitimate bit rate, but one can selectively enable just a selected one by the switch `-b`. The argument `-D` will DISABLE the use of a more sophisticated demodulation algorithm. The decoding can be significantly sped up by limiting the search range over which packets are sought for. The limit can be specified using the `-l` argument. Normally, the program will print out the decoded packets to the screen. The switch `-p` will write the packets to a file with identical name and extension .pkts. These files can be uploaded to the server using the Python3 script smog_upload.py. 

Most users will want to do something like
<pre><code>
pi@raspberrypi:~/smogcli2/build $ smog1_decode -p -v SMOG-P_16668*.cf32
pi@raspberrypi:~/smogcli2/build $ python3 ../scripts/smog_upload.py SMOG-P_16668*.pkts
</code></pre>

# Other tools

## `smog_fc2wav`

This tool converts 16-bit WAV files to .cf32-format recordings, suitable e.g. for converting wav files recorded by SDRSharp.

# Advanced usage

## Multiple dongles

`smog1_rtl_rx` accepts an argument, `-d`, which allows the user to specify which rtl-sdr device to use.
This switch accepts different formats (the device ID: 0, 1, ...; or the serial number string, or 
the beginning or end of a serial number string). `smog_rtl_rx` will automatically
append the device ID to the cf32 file name.
Don't run multiple `smog1_rtl_rx` instances from the same directory! TLE updates will
go wrong, or other horrible things might happen. Simply create separate directories for different
devices and run the receivers from their respective directories. We'll not "fix" this.

# Raspberry Pi notes

The instructions below have worked out for freshly installed Raspbian 10 machines to install the rtl-sdr 
development library with driver blacklisting, and build smogcli2. 

## Blacklist the stock rtl-sdr kernel driver
First, you need to ensure that the kernel
is not going to try to claim the rtl-sdr device, but lets librtlsdr to talk to it.
### Method 1
Create a file
`/etc/modprobe.d/no-rtl.conf`
with the contents
```
blacklist dvb_usb_rtl28xxu
blacklist rtl2832
blacklist rtl2830
```
You can use your favorite text editor to that end:
`sudo nano /etc/modprobe.d/no-rtl.conf`
Add the lines above, save, and skip to the rtl-sdr installation below. 

### Method 2
Alternatively, you can create the blacklist file using a "here document" as follows.
Copy all the following 5 lines AT ONCE:
```
cat << EOF >no-rtl.conf
blacklist dvb_usb_rtl28xxu
blacklist rtl2832
blacklist rtl2830
EOF
```
paste them into your command line, and press Enter. Finally, have the freshly created file moved
to its place with
`sudo mv no-rtl.conf /etc/modprobe.d/`

## Install the rtl-sdr software infrastructure
With the blacklisting in place, install the rtl-sdr support libraries by copy-pasting
every line below:
```
sudo apt-get install git cmake libusb-1.0-0-dev build-essential
git clone git://git.osmocom.org/rtl-sdr.git
cd rtl-sdr/
mkdir build
cd build
cmake ../ -DINSTALL_UDEV_RULES=ON -DENABLE_ZEROCOPY=ON
make
sudo make install
sudo ldconfig
cd ~
sudo cp ./rtl-sdr/rtl-sdr.rules /etc/udev/rules.d/
```
Reboot. You can check that everything around rtl-sdr works by running the stock
`rtl_test` program packaged with the rtl-sdr driver.  
```
sudo reboot
rtl_test
```
There should be no issues and
no excessive frame drops observed in its output. If everything looks fine, you now
have a working rtl-sdr setup that you can use for any other purpose as well.

## Install smogcli
It's time to move on to the part where we install smogcli. Copy and paste every line
```
sudo apt install libcurl4-gnutls-dev cmake libfftw3-dev libjsoncpp-dev
git clone https://gitlab.com/phorvath/smogcli2.git
cd smogcli2
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE=cmake/arm_cortex_a53-native.cmake ..
make
./smog_tests
sudo make install
sudo apt install tmux
```

## Run the tests
Run the `smog_tests` binary to see some performance stats.
<pre><code>pi@raspberrypi:~/smogcli2/build $ ./smog_tests
buffer_t: test passed
convert_u8_f32: test passed
convert_u8_f32: 116.536 msps
filter_fir_f32: test passed
filter_fir_f32: taps16 dec2 vec2 speed 2.78684 msps
filter_fir_vec2_f32: test passed
filter_fir_vec2_f32: taps16 dec2 neon 24.0684 msps
filter_fir_tap16_dec2_vec2_f32: test passed
filter_fir_tap16_dec2_vec2_f32: neon 70.3193 msps
sinusoid_source_cf32: test passed
sinusoid_source_cf32: 106.66 msps
fft_cf32: test passed
fft_cf32: win2048 str512 speed 5.63611 msps
norm_cf32: 168.986 msps
</code></pre>

On a Pi3+
<pre><code>pi@raspberrypi:~/smogcli2/build $ ./smog_tests
buffer_t: test passed
convert_u8_f32: test passed
convert_u8_f32: 104.792 msps
filter_fir_f32: test passed
filter_fir_f32: taps16 dec2 vec2 speed 3.35106 msps
filter_fir_vec2_f32: test passed
filter_fir_vec2_f32: taps16 dec2 neon 27.5301 msps
filter_fir_interp_vec2_f32: test passed
filter_fir_interp_vec2_f32: taps16 interp2 4.71385 msps
filter_fir_tap16_dec2_vec2_f32: test passed
filter_fir_tap16_dec2_vec2_f32: neon 73.7292 msps
sinusoid_source_cf32: test passed
sinusoid_source_cf32: 107.59 msps
fft_cf32: test passed
fft_cf32: win2048 str512 speed 6.40181 msps
norm_cf32: 146.421 msps
</code></pre>

On a Pi4 with the Cortex-A53 compiler flags:
<pre><code>buffer_t: test passed
convert_u8_f32: test passed
convert_u8_f32: 176.346 msps
filter_fir_f32: test passed
filter_fir_f32: taps16 dec2 vec2 speed 7.95102 msps
filter_fir_vec2_f32: test passed
filter_fir_vec2_f32: taps16 dec2 neon 61.4122 msps
filter_fir_interp_vec2_f32: test passed
filter_fir_interp_vec2_f32: taps16 interp2 14.1595 msps
filter_fir_tap16_dec2_vec2_f32: test passed
filter_fir_tap16_dec2_vec2_f32: neon 162.228 msps
sinusoid_source_cf32: test passed
sinusoid_source_cf32: 135.553 msps
fft_cf32: test passed
fft_cf32: win2048 str512 speed 12.3182 msps
norm_cf32: 316.983 msps
</code></pre>

# PlutoSDR + RPi install notes

smogcli2 has experimental support for the ADALM-PLUTO (aka PlutoSDR). Brief installation instructions if one intends to directly replace the rtl-sdr with the Pluto (i.e., do the recordings on the RPi, although Pluto also encapsulates an embedded Linux distro but unfortunately does not possess easily accessible storage or networking options).

<pre><code>
sudo apt-get install libxml2-dev bison flex libcdk5-dev cmake
sudo apt-get install libaio-dev libusb-1.0-0-dev libserialport-dev libavahi-client-dev
git clone https://github.com/analogdevicesinc/libiio.git
cd libiio
mkdir build
cd build
cmake ..
make
sudo make install
sudo ldconfig
</code></pre>
When these commands are successful, smogcli2 should be able to find libiio and build `smog1_pluto_rx` as well.
