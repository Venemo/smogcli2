#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2020 Peter Horvath.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.

from __future__ import print_function
import argparse
import sys
import os
import json
import time
import requests
from requests.auth import HTTPBasicAuth


def load_credentials(qthfile_path=None):
    if qthfile_path is None:
        qthfile_path = 'qthfile.txt'
    if not os.path.isfile(qthfile_path):
        print("File path {} does not exist. Exiting...".format(qthfile_path))
        sys.exit(1)

    with open(qthfile_path) as qthfile:
        qthfile_lines = qthfile.readlines(200)

    if len(qthfile_lines) < 6:
        print("Upload credentials missing from the QTH file. Exiting...")
        sys.exit(1)

    return (qthfile_lines[4].rstrip("\n\r"), qthfile_lines[5].rstrip("\n\r"))


def login(credentials, retries):
    auth = HTTPBasicAuth(credentials[0], credentials[1])
    auth_token = None

    for retry in range(retries):
        try:
            rauth = requests.post(
                'https://gnd.bme.hu:8080/api/tokens', auth=auth, timeout=30)
        except requests.exceptions.ConnectionError as err:
            print("Connection error:", err)
            continue

        if rauth.status_code == 200:
            auth_resp = rauth.json()
            # the token is valid for 60 minutes, after 45 a new one can be requested
            auth_token = auth_resp['token']
            print("Login successful.")
            break
        elif rauth.status_code == 401:
            print("Wrong credentials, please register at https://gnd.bme.hu:8080/!")
            break
        else:
            print("Authentication failed with error code:", rauth.status_code)
            if retry < retries - 1:
                print("Retrying to connect.")
            else:
                print("Could not connect to server.")
                sys.exit(3)

        time.sleep(1)

    return auth_token


def upload_packets(packets, auth_token, retries):
    auth_header = {'Authorization': 'Bearer ' + auth_token}

    good_set = set()
    bad_connect = 0

    for retry in range(retries):
        try:
            rpacket = requests.post('https://gnd.bme.hu:8080/api/packets/bulk',
                                    json={'packets': packets}, headers=auth_header, timeout=60)
        except requests.exceptions.ConnectionError as err:
            bad_connect += 1
            print("Connection error:", err)
            time.sleep(1)
            continue

        packet_resp = rpacket.json()
        if rpacket.status_code == 200:
            uploaded_packets = packet_resp["results"]
            pcnt = 0
            for p in uploaded_packets:
                if 'error' in p:
                    print('Checksum error')
                else:
                    print(p['location'])
                    good_set.add(pcnt)
                pcnt += 1

            if len(good_set) == len(packets):
                break
            elif retry < retries - 1:
                print("Packet(s) have been refused. Resending the last block.")
                time.sleep(1)

        elif retry < retries - 1:
            bad_connect += 1
            print("Packet upload failed with error code:", rpacket.status_code)
            time.sleep(1)

    num_good = len(good_set)
    num_bad = len(packets) - num_good

    if bad_connect >= retries:
        print("Failed to upload data to server")
        sys.exit(3)

    print("Chunk complete, accepted {} packets, bad checksum: {} packets.".format(
        num_good, num_bad))

    return (num_good, num_bad)


def main(args=None):
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-r', type=int, metavar='NUM',
                        default=5, help='number of network retries')
    parser.add_argument('-q', metavar='QTH',
                        default='qthfile.txt', help='path to the QTH file with station data')
    parser.add_argument('filenames', nargs='*', help="list of pkts filenames")
    args = parser.parse_args(args)

    credentials = load_credentials(args.q)

    for filename in args.filenames:
        print("Uploading {}".format(filename))

        basename = os.path.basename(filename)
        sat_name = "smogp" if basename.startswith("SMOG-P") else \
            "atl1" if basename.startswith("ATL-1") else \
            "smog1" if basename.startswith("SMOG-1") else \
            "smog1" if basename.startswith("2021-022") else \
            None

        if sat_name is None:
            print("Could not detect satellite name, exiting!")
            sys.exit(2)

        num_good = 0
        num_bad = 0

        try:
            pktfile = open(filename, 'r')
            auth_token = login(credentials, retries=args.r)
            if not auth_token:
                print("Could not login to server, exiting!")
                sys.exit(2)

            packets = []
            pcnt = 0
            for line in pktfile:
                packet = json.loads("{" + line.rstrip("\n\r") + "}")
                if packet["code_type"] != "syncpkt":
                    packets.append(
                        {'satellite': sat_name, 'packet': packet["data"]})
                    pcnt += 1

                    if pcnt == 30:
                        stats = upload_packets(
                            packets, auth_token, retries=args.r)
                        num_good += stats[0]
                        num_bad += stats[1]
                        pcnt = 0
                        packets = []

            if pcnt:
                stats = upload_packets(packets, auth_token, retries=args.r)
                num_good += stats[0]
                num_bad += stats[1]

            print("Finished {}, total accepted: {} packets, bad checksum: {} packets".format(
                filename, num_good, num_bad))

        except OSError as err:
            print("OS error: {0}".format(err))


if __name__ == '__main__':
    main()
