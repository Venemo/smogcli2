#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2020 Peter Horvath.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.

import json
import argparse
import sys
import os

def main(args=None):
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-q', metavar='QTH',
                        default='qthfile.txt', help='QTH file with station data')
    parser.add_argument('filenames', nargs='*', help="list of TLE filenames")
    args = parser.parse_args(args)
    
    for filename in args.filenames:
        print("Processing {}".format(filename))
        basename = os.path.basename(filename)
        basename = os.path.splitext(basename)[0]
        fn_base = os.path.splitext(filename)[0]
        
        sat_name = "SMOG-P" if basename.startswith("SMOG-P") else \
            "ATL-1" if basename.startswith("ATL-1") else None

        if sat_name is None:
            print("Could not detect satellite name, exiting!")
            sys.exit(2)

        downlink = 437150000 if sat_name == 'SMOG-P' else 437175000

        daynum = basename.split("_")[1]
        metaname = fn_base + '.meta'

        metadata = {}
        tledata = {}
        stadata = {}

        try:
            tle_lines = [line.rstrip() for line in open(filename)]

            assert(tle_lines[0] == sat_name)

            tledata['name'] = tle_lines[0]
            tledata['line1'] = tle_lines[1]
            tledata['line2'] = tle_lines[2]
            tledata['downlink'] = downlink
            tledata['daynum'] = float(daynum)

            metadata['tle'] = tledata

            qth_lines = [line.rstrip() for line in open(args.q)]

            stadata['callsign'] = qth_lines[0]
            stadata['latitude'] = float(qth_lines[1])
            stadata['longitude'] = float(qth_lines[2])
            stadata['altitude'] = int(qth_lines[3])

            metadata['station'] = stadata

            with open(metaname, 'w') as metafile:
                json.dump(metadata, metafile)

        except OSError as err:
            print("OS error: {0}".format(err))


if __name__ == '__main__':
    main()

