/*
 * Copyright 2019-2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef FILTER_HPP
#define FILTER_HPP

#include "blocks.hpp"
#include <fftw3.h>
#include <array>
#include <cassert>
#include <complex>
#include <vector>

static constexpr float pi = 3.14159265358979323846f;

std::vector<float> window_gaussian(size_t length, float sigma);

std::vector<float> window_hanning(size_t length);

float signal_rms(const std::vector<std::complex<float>>& sig);

std::vector<std::complex<float>> signal_norm(const std::vector<std::complex<float>>& sig);

std::vector<std::complex<float>> signal_conj(const std::vector<std::complex<float>>& sig);

std::vector<std::complex<float>> signal_scale(const std::vector<std::complex<float>>& sig,
                                              float scale);

static inline std::vector<float> window_rectangular(size_t length)
{
    return std::vector<float>(length, 1.0f);
}

class filter_fir_f32 : public history_base<float>
{
public:
    filter_fir_f32(const std::vector<float>& taps, size_t decim, size_t vlen);

    const std::vector<float>& get_taps() const { return taps; }
    size_t get_decim() const { return decim; }
    size_t get_vlen() const { return vlen; }

    // input: length * decim * vlen, output: length * vlen
    void work(const float* input, float* output, size_t length);

    void work(const std::vector<float>& input, std::vector<float>& output)
    {
        assert(input.size() == output.size() * decim);
        assert(output.size() % vlen == 0);
        work(input.data(), output.data(), output.size() / vlen);
    }

    void work(const std::vector<std::complex<float>>& input,
              std::vector<std::complex<float>>& output)
    {
        assert(input.size() == output.size() * decim);
        assert(vlen % 2 == 0 && output.size() % (vlen / 2) == 0);
        work(reinterpret_cast<const float*>(input.data()),
             reinterpret_cast<float*>(output.data()),
             output.size() / (vlen / 2));
    }

protected:
    const std::vector<float> taps;
    const size_t decim;
    const size_t vlen;
};

class filter_fir_cf32 : public history_base<std::complex<float>>
{
public:
    filter_fir_cf32(const std::vector<std::complex<float>>& taps,
                    size_t decim,
                    size_t vlen);

    const std::vector<std::complex<float>>& get_taps() const { return taps; }
    size_t get_decim() const { return decim; }
    size_t get_vlen() const { return vlen; }

    // input: length * decim * vlen, output: length * vlen
    void
    work(const std::complex<float>* input, std::complex<float>* output, size_t length);

    void work(const std::vector<std::complex<float>>& input,
              std::vector<std::complex<float>>& output)
    {
        assert(input.size() == output.size() * decim);
        assert(output.size() % vlen == 0);
        work(input.data(), output.data(), output.size() / vlen);
    }

protected:
    const std::vector<std::complex<float>> taps;
    const size_t decim;
    const size_t vlen;
};

class filter_fir_vec1_cf32 : public history_base<std::complex<float>>
{
public:
    filter_fir_vec1_cf32(const std::vector<std::complex<float>>& taps, size_t decim);

    const std::vector<std::complex<float>>& get_taps() const { return taps; }
    size_t get_decim() const { return decim; }

    // input: length * decim, output: length
    void
    work(const std::complex<float>* input, std::complex<float>* output, size_t length);

    void work(const std::vector<std::complex<float>>& input,
              std::vector<std::complex<float>>& output)
    {
        assert(input.size() == output.size() * decim);
        work(input.data(), output.data(), output.size());
    }

protected:
    const std::vector<std::complex<float>> taps;
    const size_t decim;
};

class filter_match1_vec1_cf32 : public history_base<std::complex<float>>
{
public:
    filter_match1_vec1_cf32(const std::vector<std::complex<float>>& taps, size_t decim);

    const std::vector<std::complex<float>>& get_taps() const { return taps; }
    size_t get_decim() const { return decim; }

    // input: length * decim, output: length
    void work(const std::complex<float>* input, float* output, size_t length);

    void work(const std::vector<std::complex<float>>& input, std::vector<float>& output)
    {
        assert(input.size() == output.size() * decim);
        work(input.data(), output.data(), output.size());
    }

protected:
    const std::vector<std::complex<float>> taps;
    const size_t decim;
};

class filter_match2_vec1_cf32 : public history_base<std::complex<float>>
{
public:
    filter_match2_vec1_cf32(const std::vector<std::complex<float>>& taps,
                            size_t decim,
                            size_t block_len);

    const std::vector<std::complex<float>>& get_taps() const { return taps; }
    size_t get_decim() const { return decim; }

    // input: length * decim, output: length
    void work(const std::complex<float>* input, float* output, size_t length);

    void work(const std::vector<std::complex<float>>& input, std::vector<float>& output)
    {
        assert(input.size() == output.size() * decim);
        work(input.data(), output.data(), output.size());
    }

protected:
    const std::vector<std::complex<float>> taps;
    const size_t decim;
    const size_t block_len;
    const float scale;
};

class filter_fir_interp_vec2_f32 : public history_base<float>
{
public:
    filter_fir_interp_vec2_f32(const std::vector<float>& taps, size_t interp);

    const std::vector<float>& get_taps() const { return orig_taps; }
    size_t get_interp() const { return interp; }

    // input: length * 2, output: length * interp * 2
    void work(const float* input, float* output, size_t length);

    void work(const std::vector<float>& input, std::vector<float>& output)
    {
        assert(input.size() * interp == output.size());
        assert(input.size() % 2 == 0);
        work(input.data(), output.data(), input.size() / 2);
    }

    void
    work(const std::complex<float>* input, std::complex<float>* output, size_t length)
    {
        work(reinterpret_cast<const float*>(input),
             reinterpret_cast<float*>(output),
             length);
    }

    void work(const std::vector<std::complex<float>>& input,
              std::vector<std::complex<float>>& output)
    {
        assert(input.size() * interp == output.size());
        work(input.data(), output.data(), input.size());
    }

protected:
    static std::vector<float> extend_taps(const std::vector<float>& taps, size_t interp);

    const std::vector<float> orig_taps;
    const std::vector<float> taps;
    const size_t interp;
};

class filter_fir_vec2_f32
{
public:
    filter_fir_vec2_f32(const std::vector<float>& taps, size_t decim);

    const std::vector<float>& get_taps() const { return orig_taps; }
    size_t get_decim() const { return decim; }

    void clear_history();

    // input: length * decim * 2, output: length * 2
    void work(const float* input, float* output, size_t length);

    void work(const std::vector<float>& input, std::vector<float>& output)
    {
        assert(input.size() == output.size() * decim);
        assert(output.size() % 2 == 0);
        work(input.data(), output.data(), output.size() / 2);
    }

    void
    work(const std::complex<float>* input, std::complex<float>* output, size_t length)
    {
        work(reinterpret_cast<const float*>(input),
             reinterpret_cast<float*>(output),
             length);
    }

    void work(const std::vector<std::complex<float>>& input,
              std::vector<std::complex<float>>& output)
    {
        assert(input.size() == output.size() * decim);
        work(input.data(), output.data(), output.size());
    }

protected:
    static std::vector<float> extend_taps(const std::vector<float>& taps);
    void work_straight(const float* input, float* output, size_t length);

    const std::vector<float> orig_taps;
    const std::vector<float> taps;
    const size_t decim;

    std::vector<float> history;
};

class filter_fir_tap16_dec2_vec2_f32
{
public:
    filter_fir_tap16_dec2_vec2_f32(const std::vector<float>& taps);

    const std::vector<float>& get_taps() const { return orig_taps; }

    // input: length * 2 * 2, output: length * 2
    void work(const float* input, float* output, size_t length);

    void work(const std::vector<float>& input, std::vector<float>& output)
    {
        assert(input.size() == output.size() * 2);
        assert(output.size() % 2 == 0);
        work(input.data(), output.data(), output.size() / 2);
    }

    void work(const std::vector<std::complex<float>>& input,
              std::vector<std::complex<float>>& output)
    {
        assert(input.size() == output.size() * 2);
        work(reinterpret_cast<const float*>(input.data()),
             reinterpret_cast<float*>(output.data()),
             output.size());
    }

protected:
    static std::array<float, 16> extend_taps(const std::vector<float>& taps);

    const std::vector<float> orig_taps;
    const std::array<float, 16> taps;

    std::array<float, 28> history;
};

class fft_cf32 : public history_base<std::complex<float>>
{
public:
    fft_cf32(std::vector<float> window, size_t stride, bool forward, bool shift_output);
    fft_cf32(const fft_cf32& other);
    ~fft_cf32();

    const std::vector<float>& get_window() const { return window; }
    size_t get_stride() const { return history_stride; }

    // input: length * stride, output: length * window.size()
    void
    work(const std::complex<float>* input, std::complex<float>* output, size_t length);

    void work(const std::vector<float>& input, std::vector<float>& output)
    {
        assert(input.size() % (2 * history_stride) == 0);
        size_t length = input.size() / (2 * history_stride);
        assert(output.size() == 2 * length * window.size());
        work(reinterpret_cast<const std::complex<float>*>(input.data()),
             reinterpret_cast<std::complex<float>*>(output.data()),
             length);
    }

    void work(const std::vector<std::complex<float>>& input,
              std::vector<std::complex<float>>& output)
    {
        assert(input.size() % history_stride == 0);
        size_t length = input.size() / history_stride;
        assert(output.size() == length * window.size());
        work(input.data(), output.data(), length);
    }

protected:
    const std::vector<float> window;
    const bool forward;
    const bool shift_output;

    std::complex<float>* fft_in;
    std::complex<float>* fft_out;
    fftwf_plan fft_plan;
};

#endif // FILTER_HPP
