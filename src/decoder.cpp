/*
 * Copyright 2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "ao40/ao40_decode_message.h"
#include "ao40/ao40_enc.h"
#include "ao40/ao40short_decode_message.h"
#include "ao40/ao40short_enc.h"
#include "decoder.hpp"
#include "ra/ra_config.h"
#include "ra/ra_decoder_gen.h"
#include "ra/ra_encoder.h"
#include <cassert>
#include <cmath>
#include <cstring>
#include <iomanip>
#include <iostream>

std::vector<uint8_t> soft_decoder::sync1_bytes = {
    0x97, 0xfd, 0xd3, 0x7b, 0x0f, 0x1f, 0x6d, 0x08, 0xf7, 0x83, 0x5d, 0x9e, 0x59,
    0x82, 0xc0, 0xfd, 0x1d, 0xca, 0xad, 0x3b, 0x5b, 0xeb, 0xd4, 0x93, 0xe1, 0x4a,
    0x04, 0xd2, 0x28, 0xdd, 0xf9, 0x01, 0x53, 0xd2, 0xe6, 0x6c, 0x5b, 0x25, 0x65,
    0x31, 0xc5, 0x7c, 0xe7, 0xf1, 0x38, 0x61, 0x2d, 0x5c, 0x03, 0x3a, 0xc6, 0x88,
    0x90, 0xdb, 0x8c, 0x8c, 0x42, 0xf3, 0x51, 0x75, 0x43, 0xa0, 0x83, 0x93
};

std::vector<uint8_t> soft_decoder::sync2_bytes = {
    0xa3, 0x9e, 0x1a, 0x55, 0x6b, 0xcb, 0x5c, 0x2f, 0x2a, 0x5c, 0xad, 0xd5, 0x32,
    0xfe, 0x85, 0x1d, 0xdc, 0xe8, 0xbc, 0xe5, 0x13, 0x7e, 0xba, 0xbd, 0x9d, 0x44,
    0x31, 0x51, 0x3c, 0x92, 0x26, 0x6c, 0xf3, 0x68, 0x98, 0xda, 0xa3, 0xba, 0x7f,
    0x84, 0x86, 0x32, 0x95, 0xac, 0x8d, 0x4e, 0x66, 0x8b, 0x7f, 0x7b, 0xe0, 0x14,
    0xe2, 0x3c, 0x49, 0x45, 0x32, 0xe4, 0x5c, 0x44, 0xf5, 0x6d, 0x2d, 0x0a
};

std::vector<uint8_t> soft_decoder::sync3_bytes = {
    0xe0, 0x7f, 0x06, 0x9a, 0x24, 0xe2, 0x57, 0x5b, 0x37, 0x7e, 0x39, 0xc9, 0x4b,
    0xc0, 0x7a, 0xee, 0x13, 0xfb, 0xfa, 0x9e, 0xf8, 0x37, 0x13, 0x44, 0xe2, 0x2b,
    0x04, 0x13, 0x20, 0xc0, 0x24, 0xf9, 0xb5, 0x92, 0x0f, 0x1e, 0xb7, 0x4f, 0x54,
    0xc5, 0x9d, 0x16, 0x73, 0xd0, 0x22, 0x45, 0x07, 0xd2, 0x4b, 0xc2, 0x47, 0xfa,
    0x85, 0x21, 0xa7, 0x0c, 0xb6, 0xbc, 0x0b, 0x74, 0x61, 0xcd, 0xeb, 0xf6
};

std::vector<bool> soft_decoder::create_sync_bits(const std::vector<uint8_t>& bytes)
{
    std::vector<bool> bits;

    for (size_t i = 0; i < bytes.size(); i++) {
        for (int j = 7; j >= 0; j--)
            bits.push_back(((bytes[i] >> j) & 0x1) != 0);
    }

    for (size_t i = 0; i < 6 * 8; i++)
        bits.push_back(false);

    return bits;
}

soft_decoder::soft_decoder(bool check_crc)
    : check_crc(check_crc),
      sync1_code(create_sync_bits(sync1_bytes)),
      sync2_code(create_sync_bits(sync2_bytes)),
      sync3_code(create_sync_bits(sync3_bytes))
{
}

soft_decoder::packet_t soft_decoder::work(const float* input, size_t length)
{
    packet_t packet = work_sync(input, length);

    packet_t packet2 = work_data(input, length);
    update_if_better(packet, packet2);

    return packet;
}

soft_decoder::packet_t soft_decoder::work_data(const float* input, size_t length)
{
    packet_t packet;

    // for RA positive soft bit is 0 hard bit, bit order is reversed
    ra_soft.resize(length);
    for (size_t i = 0; i < length; i += 8)
        for (size_t j = 0; j < 8; j++)
            ra_soft[i + 7 - j] = -input[i + j];

    if (length >= 260 * 8) {
        packet_t packet2 = work_ra("ra128", 64);
        update_if_better(packet, packet2);
    }

    if (length >= 514 * 8) {
        packet_t packet2 = work_ra("ra256", 128);
        update_if_better(packet, packet2);
    }

    if (length >= 1028 * 8) {
        packet_t packet2 = work_ra("ra512", 256);
        update_if_better(packet, packet2);
    }

    if (length >= 2050 * 8) {
        packet_t packet2 = work_ra("ra1024", 512);
        update_if_better(packet, packet2);
    }

    if (length >= 4100 * 8) {
        packet_t packet2 = work_ra("ra2048", 1024);
        update_if_better(packet, packet2);
    }

    if (length >= AO40SHORT_CODE_LENGTH * 8) {
        packet_t packet2 = work_ao40(input, true);
        update_if_better(packet, packet2);
    }

    if (length >= AO40_CODE_LENGTH * 8) {
        packet_t packet2 = work_ao40(input, false);
        update_if_better(packet, packet2);
    }

    return packet;
}

soft_decoder::packet_t soft_decoder::work_sync(const float* input, size_t length)
{
    packet_t packet;

    if (length >= sync1_code.size())
        packet = work_sync(input, sync1_bytes[0], sync1_code);

    if (length >= sync2_code.size()) {
        packet_t packet2 = work_sync(input, sync2_bytes[0], sync2_code);
        update_if_better(packet, packet2);
    }

    if (length >= sync3_code.size()) {
        packet_t packet2 = work_sync(input, sync3_bytes[0], sync3_code);
        update_if_better(packet, packet2);
    }

    return packet;
}

soft_decoder::packet_t
soft_decoder::work_sync(const float* input, uint8_t first_byte, std::vector<bool>& code)
{
    assert(code.size() > 6 * 8);

    unsigned int data = 0;
    for (unsigned int i = 0; i < 6; i++) {
        size_t k = code.size() - 6 * 8;
        float a = 0.0f;
        for (unsigned int j = 0; j < 8; j++)
            a += input[k + 8 * i + j];

        data = (data << 1) | (a >= 0.0f ? 0x1 : 0x0);

        for (unsigned int j = 0; j < 8; j++)
            code[k + 8 * i + j] = data & 0x1;
    }

    packet_t packet;
    packet.type = "syncpkt";
    packet.data.resize(2);
    packet.data[0] = first_byte;
    packet.data[1] = data;
    calculate_ber_snr(input, code, packet);
    return packet;
}

soft_decoder::packet_t soft_decoder::work_ra(const char* type, size_t words)
{
    assert(words <= RA_MAX_DATA_LENGTH);

    ra_length_init(words);

    ra_data.resize(ra_data_length);
    ra_decoder_gen(ra_soft.data(), ra_data.data(), 40);

    ra_hard.resize(static_cast<size_t>(ra_code_length) * 16);
    ra_encoder_init(ra_data.data());
    for (size_t i = 0; i < ra_code_length; i++) {
        uint16_t word = ra_encoder_next();
        for (size_t j = 0; j < 16; j++) {
            bool b = (word & (1 << j)) != 0;
            ra_hard[i * 16 + j] = !b;
        }
    }

    packet_t packet;
    packet.type = type;
    packet.data.resize(ra_data.size() * 2);
    std::memcpy(packet.data.data(), ra_data.data(), packet.data.size());
    calculate_ber_snr(ra_soft.data(), ra_hard, packet);
    if (check_crc)
        calculate_crc(packet);
    return packet;
}

soft_decoder::packet_t soft_decoder::work_ao40(const float* input, bool is_short)
{
    ao40_soft.resize(is_short ? AO40SHORT_RAW_SIZE : AO40_RAW_SIZE);

    float a = 0.0f;
    for (size_t i = 0; i < ao40_soft.size(); i++)
        a += std::abs(input[i]);
    a /= ao40_soft.size();
    a = 32.0f / a;

    for (size_t i = 0; i < ao40_soft.size(); i++)
        ao40_soft[i] = std::min(std::max(a * input[i] + 127.5f, 0.0f), 255.0f);

    ao40_data.resize(is_short ? AO40SHORT_DATA_SIZE : AO40_DATA_SIZE);
    int8_t error[2];
    if (is_short)
        ao40short_decode_data(ao40_soft.data(), ao40_data.data(), error);
    else
        ao40_decode_data(ao40_soft.data(), ao40_data.data(), error);

    ao40_code.resize(is_short ? AO40SHORT_CODE_LENGTH : AO40_CODE_LENGTH);
    if (is_short)
        encode_data_ao40short(ao40_data.data(), ao40_code.data());
    else
        encode_data_ao40(ao40_data.data(), ao40_code.data());

    assert((ao40_soft.size() + 7) / 8 == ao40_code.size());
    ao40_hard.resize(ao40_soft.size());
    for (size_t i = 0; i < ao40_hard.size(); i++) {
        uint8_t b = 1 << (7 - (i % 8));
        ao40_hard[i] = (ao40_code[i / 8] & b) != 0;
    }

    packet_t packet;
    packet.type = is_short ? "ao40short" : "ao40";
    packet.data = ao40_data;
    calculate_ber_snr(input, ao40_hard, packet);
    if (check_crc)
        calculate_crc(packet);
    return packet;
}

void soft_decoder::print_packet(const packet_t& packet, std::stringstream& msg) const
{
    msg << std::setprecision(3) << std::fixed << "\"code_type\": \"" << packet.type
        << "\", \"data_snr\": " << packet.snr << ", \"data_ber\": " << packet.ber
        << ", \"crc\": " << (packet.crc ? "true" : "false") << ", \"data\": \""
        << std::setfill('0') << std::hex << std::uppercase;

    for (size_t i = 0; i < packet.data.size(); i++)
        msg << std::setw(2) << static_cast<int>(packet.data[i]);

    msg << "\"" << std::dec;
}

void soft_decoder::print_packet(const packet_t& packet) const
{
    std::stringstream msg;
    print_packet(packet, msg);
    msg << std::endl;
    std::cout << msg.str();
}

void soft_decoder::calculate_ber_snr(const float* input,
                                     const std::vector<bool>& code,
                                     packet_t& packet)
{
    if (!code.empty()) {
        float a = 0.0f;
        for (size_t i = 0; i < code.size(); i++)
            a += code[i] ? input[i] : -input[i];
        a /= code.size(); // average signal amplitude
        a = std::abs(a);  // should be positive

        float b = 0.0f;
        float n = 1e-40f;
        for (size_t i = 0; i < code.size(); i++) {
            b += (input[i] >= 0.0f) == code[i] ? 0.0f : 1.0f;
            float d = input[i] - (code[i] ? a : -a);
            n += d * d;
        }

        packet.ber = b / code.size();
        packet.snr = 10.0f * std::log10(code.size() * a * a / n);
    } else {
        packet.ber = 1.0f;
        packet.snr = -99.0f;
    }
}

void soft_decoder::calculate_crc(packet_t& packet)
{
    uint16_t crc = 0;
    for (size_t i = 0; i < packet.data.size(); i++) {
        // we do not reverse the bits, so 0x8005 becomes 0xa001
        crc ^= packet.data[i];

        for (int j = 0; j < 8; j++) {
            if ((crc & 0x0001) != 0) {
                crc = (crc >> 1) ^ 0xa001;
            } else {
                crc = crc >> 1;
            }
        }
    }
    packet.crc = crc == 0;
}

void soft_decoder::update_if_better(packet_t& packet1, const packet_t& packet2)
{
    if ((packet2.crc == packet1.crc && packet2.snr > packet1.snr) ||
        (packet2.crc && !packet1.crc)) {
        packet1 = packet2;
    }
}
