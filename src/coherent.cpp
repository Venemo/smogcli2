/*
 * Copyright 2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "coherent.hpp"
#include <cassert>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <limits>
#include <utility>

std::vector<std::complex<float>>
gmsk_encode_bits(const std::vector<bool>& bits, unsigned int spb, float bt)
{
    assert(spb >= 1);

    const float c = std::sqrt(std::log(2.0f)) / (pi * 4.0f);
    std::vector<float> taps = window_gaussian(4 * spb + 1, c / bt);

    float tapsum = 0.0f;
    for (size_t i = 0; i < taps.size(); i++)
        tapsum += taps[i];

    const float unit = 0.5f * pi / (spb * tapsum);
    std::vector<std::complex<float>> sig(bits.size() * spb + 1);

    float angle = 0.0f;
    for (size_t i = 0; i < sig.size(); i++) {
        sig[i] = std::complex<float>(std::cos(angle), std::sin(angle));

        float sample = 0.0f;
        for (size_t j = 0; j < taps.size(); j++) {
            if (i + j < taps.size() / 2)
                continue;
            size_t k = i + j - taps.size() / 2;
            if (k >= bits.size() * spb)
                continue;

            k /= spb;
            float b = bits[k] ? unit : -unit;
            sample += b * taps[j];
        }
        angle = std::fmod(angle + sample, 2.0f * pi);
    }

    return sig;
}

std::vector<std::complex<float>>
gmsk_encode_bytes(const std::vector<uint8_t>& bytes, unsigned int spb, float bt)
{
    std::vector<bool> bits(bytes.size() * 8);
    for (size_t k = 0; k < bits.size(); k++)
        bits[k] = (bytes[k / 8] & (1 << (7 - (k % 8)))) != 0;
    return gmsk_encode_bits(bits, spb, bt);
}

// Takes size * spb + 1 many samples, and returns size many soft bits
std::vector<float> gmsk_soft_decode2(const std::vector<std::complex<float>>& samples,
                                     unsigned int spb)
{
    assert(spb > 0 && (samples.size() - 1) % spb == 0);

    std::vector<float> bits;
    for (size_t i = 0; i + spb < samples.size(); i += spb) {
        std::complex<float> a = std::conj(samples[i]) * samples[i + spb];
        bits.push_back(a.imag());
    }

    return bits;
}

std::vector<uint8_t> soft_to_hard(const std::vector<float>& bits)
{
    assert(bits.size() % 8 == 0);

    std::vector<uint8_t> bytes;
    for (size_t i = 0; i + 8 <= bits.size(); i += 8) {
        uint8_t a = 0;
        for (unsigned int j = 0; j < 8; j++) {
            uint8_t b = bits[i + j] >= 0.0f ? 1 : 0;
            a |= b << (7 - j);
        }
        bytes.push_back(a);
    }

    return bytes;
}

std::vector<uint8_t> gmsk_hard_decode2(const std::vector<std::complex<float>>& samples,
                                       unsigned int spb)
{
    return soft_to_hard(gmsk_soft_decode2(samples, spb));
}

const std::vector<float> signal_downsampler::taps_spb50 = {
    -1.51714469e-03, -1.64344901e-04, -1.71257964e-04, -1.77267672e-04, -1.81808041e-04,
    -1.85189963e-04, -1.86849605e-04, -1.87091874e-04, -1.85396055e-04, -1.82062190e-04,
    -1.76582042e-04, -1.69342516e-04, -1.59787268e-04, -1.48389393e-04, -1.34582808e-04,
    -1.18842828e-04, -1.00602459e-04, -8.04185603e-05, -5.76512509e-05, -3.28998548e-05,
    -5.51656315e-06, 2.37491706e-05,  5.55825275e-05,  8.89795992e-05,  1.24659127e-04,
    1.61365214e-04,  1.99996077e-04,  2.39160942e-04,  2.80555110e-04,  3.22549691e-04,
    3.68260799e-04,  4.10197150e-04,  4.53539429e-04,  4.98814698e-04,  5.41863972e-04,
    5.85592010e-04,  6.27632059e-04,  6.69123660e-04,  7.08591075e-04,  7.46614161e-04,
    7.82018232e-04,  8.15187739e-04,  8.45052498e-04,  8.71884370e-04,  8.94770232e-04,
    9.13833385e-04,  9.28303531e-04,  9.38306179e-04,  9.43106843e-04,  9.42758028e-04,
    9.36589153e-04,  9.24648393e-04,  9.06302246e-04,  8.81660461e-04,  8.50222027e-04,
    8.12202985e-04,  7.67229208e-04,  7.15485369e-04,  6.56424737e-04,  5.90194418e-04,
    5.16384730e-04,  4.36140002e-04,  3.49124552e-04,  2.54246359e-04,  1.53592688e-04,
    4.60492149e-05,  -6.74804201e-05, -1.86947543e-04, -3.11839105e-04, -4.41846242e-04,
    -5.76394259e-04, -7.15112560e-04, -8.57284737e-04, -1.00241551e-03, -1.14978991e-03,
    -1.29869302e-03, -1.44839809e-03, -1.59818139e-03, -1.74721652e-03, -1.89466131e-03,
    -2.03964710e-03, -2.18125426e-03, -2.31852622e-03, -2.45047325e-03, -2.57615910e-03,
    -2.69460747e-03, -2.80493890e-03, -2.90609172e-03, -2.99705239e-03, -3.07667890e-03,
    -3.14411023e-03, -3.19848733e-03, -3.23902023e-03, -3.26398416e-03, -3.27342886e-03,
    -3.26599937e-03, -3.24088193e-03, -3.19748544e-03, -3.13489304e-03, -3.05254944e-03,
    -2.94976218e-03, -2.82595973e-03, -2.68064769e-03, -2.51343012e-03, -2.32383527e-03,
    -2.11177949e-03, -1.87691352e-03, -1.61918192e-03, -1.33846672e-03, -1.03491225e-03,
    -7.08530008e-04, -3.59634540e-04, 1.15346277e-05,  4.04436671e-04,  8.18681165e-04,
    1.25359875e-03,  1.70867328e-03,  2.18301774e-03,  2.67584053e-03,  3.18604573e-03,
    3.71277045e-03,  4.25487688e-03,  4.81131909e-03,  5.38042629e-03,  5.96127513e-03,
    6.55238617e-03,  7.15207982e-03,  7.75913337e-03,  8.37179002e-03,  8.98853102e-03,
    9.60769442e-03,  1.02275429e-02,  1.08463724e-02,  1.14624901e-02,  1.20739764e-02,
    1.26792993e-02,  1.32765640e-02,  1.38640135e-02,  1.44399092e-02,  1.50025577e-02,
    1.55501834e-02,  1.60811153e-02,  1.65937143e-02,  1.70864463e-02,  1.75577620e-02,
    1.80061663e-02,  1.84302041e-02,  1.88285355e-02,  1.91999379e-02,  1.95432348e-02,
    1.98573050e-02,  2.01410631e-02,  2.03936207e-02,  2.06142528e-02,  2.08021846e-02,
    2.09566594e-02,  2.10775021e-02,  2.11640116e-02,  2.12160784e-02,  2.12334491e-02,
    2.12160784e-02,  2.11640116e-02,  2.10775021e-02,  2.09566594e-02,  2.08021846e-02,
    2.06142528e-02,  2.03936207e-02,  2.01410631e-02,  1.98573050e-02,  1.95432348e-02,
    1.91999379e-02,  1.88285355e-02,  1.84302041e-02,  1.80061663e-02,  1.75577620e-02,
    1.70864463e-02,  1.65937143e-02,  1.60811153e-02,  1.55501834e-02,  1.50025577e-02,
    1.44399092e-02,  1.38640135e-02,  1.32765640e-02,  1.26792993e-02,  1.20739764e-02,
    1.14624901e-02,  1.08463724e-02,  1.02275429e-02,  9.60769442e-03,  8.98853102e-03,
    8.37179002e-03,  7.75913337e-03,  7.15207982e-03,  6.55238617e-03,  5.96127513e-03,
    5.38042629e-03,  4.81131909e-03,  4.25487688e-03,  3.71277045e-03,  3.18604573e-03,
    2.67584053e-03,  2.18301774e-03,  1.70867328e-03,  1.25359875e-03,  8.18681165e-04,
    4.04436671e-04,  1.15346277e-05,  -3.59634540e-04, -7.08530008e-04, -1.03491225e-03,
    -1.33846672e-03, -1.61918192e-03, -1.87691352e-03, -2.11177949e-03, -2.32383527e-03,
    -2.51343012e-03, -2.68064769e-03, -2.82595973e-03, -2.94976218e-03, -3.05254944e-03,
    -3.13489304e-03, -3.19748544e-03, -3.24088193e-03, -3.26599937e-03, -3.27342886e-03,
    -3.26398416e-03, -3.23902023e-03, -3.19848733e-03, -3.14411023e-03, -3.07667890e-03,
    -2.99705239e-03, -2.90609172e-03, -2.80493890e-03, -2.69460747e-03, -2.57615910e-03,
    -2.45047325e-03, -2.31852622e-03, -2.18125426e-03, -2.03964710e-03, -1.89466131e-03,
    -1.74721652e-03, -1.59818139e-03, -1.44839809e-03, -1.29869302e-03, -1.14978991e-03,
    -1.00241551e-03, -8.57284737e-04, -7.15112560e-04, -5.76394259e-04, -4.41846242e-04,
    -3.11839105e-04, -1.86947543e-04, -6.74804201e-05, 4.60492149e-05,  1.53592688e-04,
    2.54246359e-04,  3.49124552e-04,  4.36140002e-04,  5.16384730e-04,  5.90194418e-04,
    6.56424737e-04,  7.15485369e-04,  7.67229208e-04,  8.12202985e-04,  8.50222027e-04,
    8.81660461e-04,  9.06302246e-04,  9.24648393e-04,  9.36589153e-04,  9.42758028e-04,
    9.43106843e-04,  9.38306179e-04,  9.28303531e-04,  9.13833385e-04,  8.94770232e-04,
    8.71884370e-04,  8.45052498e-04,  8.15187739e-04,  7.82018232e-04,  7.46614161e-04,
    7.08591075e-04,  6.69123660e-04,  6.27632059e-04,  5.85592010e-04,  5.41863972e-04,
    4.98814698e-04,  4.53539429e-04,  4.10197150e-04,  3.68260799e-04,  3.22549691e-04,
    2.80555110e-04,  2.39160942e-04,  1.99996077e-04,  1.61365214e-04,  1.24659127e-04,
    8.89795992e-05,  5.55825275e-05,  2.37491706e-05,  -5.51656315e-06, -3.28998548e-05,
    -5.76512509e-05, -8.04185603e-05, -1.00602459e-04, -1.18842828e-04, -1.34582808e-04,
    -1.48389393e-04, -1.59787268e-04, -1.69342516e-04, -1.76582042e-04, -1.82062190e-04,
    -1.85396055e-04, -1.87091874e-04, -1.86849605e-04, -1.85189963e-04, -1.81808041e-04,
    -1.77267672e-04, -1.71257964e-04, -1.64344901e-04, -1.51714469e-03
};

const std::vector<float> signal_downsampler::taps_spb40 = {
    -1.55934151e-03, -2.08047795e-04, -2.18046862e-04, -2.25880701e-04, -2.30831491e-04,
    -2.33040390e-04, -2.31813068e-04, -2.27344816e-04, -2.18873160e-04, -2.06746840e-04,
    -1.90178287e-04, -1.69601158e-04, -1.44384694e-04, -1.14962011e-04, -8.08067731e-05,
    -4.25741473e-05, 1.95045199e-07,  4.68166403e-05,  9.76281566e-05,  1.51773173e-04,
    2.09678718e-04,  2.70387374e-04,  3.34265628e-04,  4.00090958e-04,  4.68190617e-04,
    5.36745313e-04,  6.06097604e-04,  6.73881400e-04,  7.41197585e-04,  8.05875841e-04,
    8.71756768e-04,  9.30298057e-04,  9.84670857e-04,  1.03591360e-03,  1.07923280e-03,
    1.11671095e-03,  1.14550251e-03,  1.16625929e-03,  1.17690813e-03,  1.17776647e-03,
    1.16719238e-03,  1.14527763e-03,  1.11087718e-03,  1.06392411e-03,  1.00350691e-03,
    9.29742394e-04,  8.41977675e-04,  7.40367886e-04,  6.24623658e-04,  4.95254720e-04,
    3.52218934e-04,  1.96230823e-04,  2.75276033e-05,  -1.52961725e-04, -3.44823347e-04,
    -5.46740088e-04, -7.57908172e-04, -9.76514780e-04, -1.20143878e-03, -1.43098568e-03,
    -1.66433177e-03, -1.89867731e-03, -2.13166239e-03, -2.36304397e-03, -2.58887863e-03,
    -2.80804161e-03, -3.01770229e-03, -3.21584582e-03, -3.39981980e-03, -3.56738346e-03,
    -3.71600167e-03, -3.84318214e-03, -3.94669740e-03, -4.02403230e-03, -4.07294250e-03,
    -4.09125511e-03, -4.07688697e-03, -4.02771238e-03, -3.94187378e-03, -3.81768266e-03,
    -3.65356621e-03, -3.44817483e-03, -3.20036957e-03, -2.90921865e-03, -2.57394422e-03,
    -2.19404869e-03, -1.76928265e-03, -1.29971879e-03, -7.85822061e-04, -2.27962563e-04,
    3.73086707e-04,  1.01625525e-03,  1.69946775e-03,  2.42206610e-03,  3.18139457e-03,
    3.97539169e-03,  4.80171269e-03,  5.65743624e-03,  6.53980569e-03,  7.44542596e-03,
    8.37121591e-03,  9.31317318e-03,  1.02679852e-02,  1.12314448e-02,  1.21995710e-02,
    1.31682676e-02,  1.41334114e-02,  1.50905841e-02,  1.60355041e-02,  1.69638815e-02,
    1.78714228e-02,  1.87538221e-02,  1.96069548e-02,  2.04267107e-02,  2.12091830e-02,
    2.19504826e-02,  2.26470162e-02,  2.32952470e-02,  2.38921636e-02,  2.44346136e-02,
    2.49199001e-02,  2.53454684e-02,  2.57095399e-02,  2.60098706e-02,  2.62449728e-02,
    2.64139333e-02,  2.65154989e-02,  2.65495099e-02,  2.65154989e-02,  2.64139333e-02,
    2.62449728e-02,  2.60098706e-02,  2.57095399e-02,  2.53454684e-02,  2.49199001e-02,
    2.44346136e-02,  2.38921636e-02,  2.32952470e-02,  2.26470162e-02,  2.19504826e-02,
    2.12091830e-02,  2.04267107e-02,  1.96069548e-02,  1.87538221e-02,  1.78714228e-02,
    1.69638815e-02,  1.60355041e-02,  1.50905841e-02,  1.41334114e-02,  1.31682676e-02,
    1.21995710e-02,  1.12314448e-02,  1.02679852e-02,  9.31317318e-03,  8.37121591e-03,
    7.44542596e-03,  6.53980569e-03,  5.65743624e-03,  4.80171269e-03,  3.97539169e-03,
    3.18139457e-03,  2.42206610e-03,  1.69946775e-03,  1.01625525e-03,  3.73086707e-04,
    -2.27962563e-04, -7.85822061e-04, -1.29971879e-03, -1.76928265e-03, -2.19404869e-03,
    -2.57394422e-03, -2.90921865e-03, -3.20036957e-03, -3.44817483e-03, -3.65356621e-03,
    -3.81768266e-03, -3.94187378e-03, -4.02771238e-03, -4.07688697e-03, -4.09125511e-03,
    -4.07294250e-03, -4.02403230e-03, -3.94669740e-03, -3.84318214e-03, -3.71600167e-03,
    -3.56738346e-03, -3.39981980e-03, -3.21584582e-03, -3.01770229e-03, -2.80804161e-03,
    -2.58887863e-03, -2.36304397e-03, -2.13166239e-03, -1.89867731e-03, -1.66433177e-03,
    -1.43098568e-03, -1.20143878e-03, -9.76514780e-04, -7.57908172e-04, -5.46740088e-04,
    -3.44823347e-04, -1.52961725e-04, 2.75276033e-05,  1.96230823e-04,  3.52218934e-04,
    4.95254720e-04,  6.24623658e-04,  7.40367886e-04,  8.41977675e-04,  9.29742394e-04,
    1.00350691e-03,  1.06392411e-03,  1.11087718e-03,  1.14527763e-03,  1.16719238e-03,
    1.17776647e-03,  1.17690813e-03,  1.16625929e-03,  1.14550251e-03,  1.11671095e-03,
    1.07923280e-03,  1.03591360e-03,  9.84670857e-04,  9.30298057e-04,  8.71756768e-04,
    8.05875841e-04,  7.41197585e-04,  6.73881400e-04,  6.06097604e-04,  5.36745313e-04,
    4.68190617e-04,  4.00090958e-04,  3.34265628e-04,  2.70387374e-04,  2.09678718e-04,
    1.51773173e-04,  9.76281566e-05,  4.68166403e-05,  1.95045199e-07,  -4.25741473e-05,
    -8.08067731e-05, -1.14962011e-04, -1.44384694e-04, -1.69601158e-04, -1.90178287e-04,
    -2.06746840e-04, -2.18873160e-04, -2.27344816e-04, -2.31813068e-04, -2.33040390e-04,
    -2.30831491e-04, -2.25880701e-04, -2.18046862e-04, -2.08047795e-04, -1.55934151e-03
};

const std::vector<float> signal_downsampler::taps_spb25 = {
    -0.00204803, -0.00070831, -0.00067436, -0.0005102,  -0.00020086, 0.00025285,
    0.00083352,  0.00150329,  0.00220439,  0.00286215,  0.00338966,  0.00369356,
    0.00368553,  0.00328897,  0.00245329,  0.00115955,  -0.00056711, -0.00265535,
    -0.00498126, -0.00737554, -0.00962509, -0.01149247, -0.01272008, -0.01305775,
    -0.01227396, -0.01018315, -0.00666265, -0.00166919, 0.00476338,  0.01249213,
    0.02129054,  0.03084877,  0.04078115,  0.0506607,   0.0600306,   0.06844257,
    0.07547826,  0.08078219,  0.08407903,  0.08519815,  0.08407903,  0.08078219,
    0.07547826,  0.06844257,  0.0600306,   0.0506607,   0.04078115,  0.03084877,
    0.02129054,  0.01249213,  0.00476338,  -0.00166919, -0.00666265, -0.01018315,
    -0.01227396, -0.01305775, -0.01272008, -0.01149247, -0.00962509, -0.00737554,
    -0.00498126, -0.00265535, -0.00056711, 0.00115955,  0.00245329,  0.00328897,
    0.00368553,  0.00369356,  0.00338966,  0.00286215,  0.00220439,  0.00150329,
    0.00083352,  0.00025285,  -0.00020086, -0.0005102,  -0.00067436, -0.00070831,
    -0.00204803
};

const std::vector<float> signal_downsampler::taps_spb20 = {
    -1.77568238e-03, -4.36782743e-04, -4.56425943e-04, -4.50784643e-04, -4.15487382e-04,
    -3.47538062e-04, -2.44493839e-04, -1.05749654e-04, 6.81367113e-05,  2.74197625e-04,
    5.08958514e-04,  7.65399310e-04,  1.03633005e-03,  1.31137950e-03,  1.57976373e-03,
    1.82805286e-03,  2.04337981e-03,  2.21062182e-03,  2.31653376e-03,  2.34687605e-03,
    2.29051491e-03,  2.13641082e-03,  1.87768368e-03,  1.50764094e-03,  1.02601733e-03,
    4.34019236e-04,  -2.58656254e-04, -1.04119878e-03, -1.89541087e-03, -2.80649549e-03,
    -3.74061691e-03, -4.67124463e-03, -5.56693328e-03, -6.38799735e-03, -7.09930597e-03,
    -7.65992093e-03, -8.03241468e-03, -8.17786093e-03, -8.06282003e-03, -7.65411058e-03,
    -6.92676088e-03, -5.85917187e-03, -4.43819480e-03, -2.65734735e-03, -5.19771222e-04,
    1.96430736e-03,  4.77440998e-03,  7.88209700e-03,  1.12492783e-02,  1.48306843e-02,
    1.85733699e-02,  2.24192556e-02,  2.63042063e-02,  3.01613159e-02,  3.39209071e-02,
    3.75139975e-02,  4.08729622e-02,  4.39335013e-02,  4.66344502e-02,  4.89227410e-02,
    5.07533446e-02,  5.20873676e-02,  5.28994711e-02,  5.31717453e-02,  5.28994711e-02,
    5.20873676e-02,  5.07533446e-02,  4.89227410e-02,  4.66344502e-02,  4.39335013e-02,
    4.08729622e-02,  3.75139975e-02,  3.39209071e-02,  3.01613159e-02,  2.63042063e-02,
    2.24192556e-02,  1.85733699e-02,  1.48306843e-02,  1.12492783e-02,  7.88209700e-03,
    4.77440998e-03,  1.96430736e-03,  -5.19771222e-04, -2.65734735e-03, -4.43819480e-03,
    -5.85917187e-03, -6.92676088e-03, -7.65411058e-03, -8.06282003e-03, -8.17786093e-03,
    -8.03241468e-03, -7.65992093e-03, -7.09930597e-03, -6.38799735e-03, -5.56693328e-03,
    -4.67124463e-03, -3.74061691e-03, -2.80649549e-03, -1.89541087e-03, -1.04119878e-03,
    -2.58656254e-04, 4.34019236e-04,  1.02601733e-03,  1.50764094e-03,  1.87768368e-03,
    2.13641082e-03,  2.29051491e-03,  2.34687605e-03,  2.31653376e-03,  2.21062182e-03,
    2.04337981e-03,  1.82805286e-03,  1.57976373e-03,  1.31137950e-03,  1.03633005e-03,
    7.65399310e-04,  5.08958514e-04,  2.74197625e-04,  6.81367113e-05,  -1.05749654e-04,
    -2.44493839e-04, -3.47538062e-04, -4.15487382e-04, -4.50784643e-04, -4.56425943e-04,
    -4.36782743e-04, -1.77568238e-03
};

const std::vector<float> signal_downsampler::taps_spb10 = {
    -0.00177635, -0.00093232, -0.00091042, -0.00065056, -0.00011691, 0.00068379,
    0.00169062,  0.00278499,  0.00379564,  0.00451411,  0.00472184,  0.00422345,
    0.00288617,  0.00067191,  -0.00232613, -0.00587276, -0.00958824, -0.01296869,
    -0.01543019, -0.01636517, -0.01521057, -0.01152039, -0.00503896, 0.00424779,
    0.01608306,  0.02993988,  0.04503648,  0.06040968,  0.07499426,  0.08771145,
    0.09759169,  0.10385383,  0.10599987,  0.10385383,  0.09759169,  0.08771145,
    0.07499426,  0.06040968,  0.04503648,  0.02993988,  0.01608306,  0.00424779,
    -0.00503896, -0.01152039, -0.01521057, -0.01636517, -0.01543019, -0.01296869,
    -0.00958824, -0.00587276, -0.00232613, 0.00067191,  0.00288617,  0.00422345,
    0.00472184,  0.00451411,  0.00379564,  0.00278499,  0.00169062,  0.00068379,
    -0.00011691, -0.00065056, -0.00091042, -0.00093232, -0.00177635
};

const std::vector<float> signal_downsampler::taps_spb5 = {
    -2.30651187e-03, -1.82395916e-03, -1.79383249e-04, 3.47266327e-03,  7.67551402e-03,
    9.46762152e-03,  5.70796046e-03,  -4.78427637e-03, -1.93129199e-02, -3.09314451e-02,
    -3.03772500e-02, -9.93822734e-03, 3.23356226e-02,  9.01797041e-02,  1.49973862e-01,
    1.95054348e-01,  2.11826601e-01,  1.95054348e-01,  1.49973862e-01,  9.01797041e-02,
    3.23356226e-02,  -9.93822734e-03, -3.03772500e-02, -3.09314451e-02, -1.93129199e-02,
    -4.78427637e-03, 5.70796046e-03,  9.46762152e-03,  7.67551402e-03,  3.47266327e-03,
    -1.79383249e-04, -1.82395916e-03, -2.30651187e-03
};

const std::vector<float> signal_downsampler::taps_spb4 = {
    -0.00241924, -0.00222623, 0.00112816,  0.00758188,  0.01192763, 0.00665574,
    -0.01115288, -0.03314226, -0.03996454, -0.01146268, 0.05822013, 0.15135375,
    0.23179837,  0.263604,    0.23179837,  0.15135375,  0.05822013, -0.01146268,
    -0.03996454, -0.03314226, -0.01115288, 0.00665574,  0.01192763, 0.00758188,
    0.00112816,  -0.00222623, -0.00241924
};

const std::vector<float> taps_empty = { 1.0 };

const std::vector<float>& signal_downsampler::gmsk_filter_taps(size_t spb)
{
    if (spb == 50)
        return taps_spb50;
    else if (spb == 40)
        return taps_spb40;
    else if (spb == 25)
        return taps_spb25;
    else if (spb == 20)
        return taps_spb20;
    else if (spb == 10)
        return taps_spb10;
    else if (spb == 5)
        return taps_spb5;
    else if (spb == 4)
        return taps_spb4;
    else {
        assert(spb == 1);
        return taps_empty;
    }
}

bool signal_downsampler::is_supported(size_t input_spb, size_t output_spb)
{
    if (input_spb % output_spb != 0)
        return false;

    return input_spb == 50 || input_spb == 40 || input_spb == 25 || input_spb == 20 ||
           input_spb == 10 || input_spb == 5 || input_spb == 4 || input_spb == 1;
}

signal_downsampler::signal_downsampler(size_t input_spb, size_t output_spb)
    : input_spb(input_spb),
      sinusoid_block(0.0f),
      filter_block(gmsk_filter_taps(input_spb), input_spb / output_spb),
      sinusoid_data((1024 / get_decim() + 1) * get_decim()),
      multiply_data(sinusoid_data.size())
{
    assert(input_spb > 0 && output_spb > 0 && input_spb % output_spb == 0);

    // make sure delay is a whole number
    assert(gmsk_filter_taps(input_spb).size() % 2 == 1);

    // make sure delay is not negative
    assert(get_taps().size() / 2 >= (get_decim() - 1));
}

void signal_downsampler::work(const std::complex<float>* input,
                              std::complex<float>* output,
                              size_t length)
{
    size_t step = sinusoid_data.size() / get_decim();
    for (size_t n = 0; n < length; n += step) {
        size_t len = std::min(length - n, step);

        sinusoid_block.work(sinusoid_data.data(), len * get_decim());
        multiply_block.work(
            sinusoid_data.data(), input, multiply_data.data(), len * get_decim());
        filter_block.work(multiply_data.data(), output, len);

        input += len * get_decim();
        output += len;
    }
}

preamble_detector::preamble_detector(const std::vector<float>& window,
                                     size_t stride,
                                     size_t input_spb,
                                     size_t output_spb,
                                     size_t max_freq_bin,
                                     const std::vector<uint8_t>& sync_bytes,
                                     float tone_snr_db,
                                     float sync_snr_db,
                                     size_t output_bytes)
    : tone_halfband((window.size() + input_spb) / (2 * input_spb) - 2),
      input_spb(input_spb),
      output_spb(output_spb),
      max_freq_bin(std::min(max_freq_bin, window.size() / 2 - tone_halfband - 1)),
      sync_bytes(sync_bytes),
      tone_snr_scale(1.0f / std::pow(10.0f, 0.1f * tone_snr_db)),
      sync_snr_scale(1.0f / std::pow(10.0f, 0.1f * sync_snr_db)),
      pos_accuracy(input_spb),
      freq_accuracy(1.0f * input_spb / window.size()),
      output_bytes(output_bytes),
      history_size1(window.size() + (sync_bytes.size() + 1) * 8 * input_spb),
      history_size2(window.size() +
                    (std::max(sync_bytes.size(), output_bytes) + 1) * 8 * input_spb),
      position(0),
      history_block(stride, history_size2),
      fft_block(window, stride, true, true),
      freq_data(window.size()),
      norm_data(window.size()),
      downsampler_block(input_spb, output_spb),
      downsampler_data(history_size2 / downsampler_block.get_decim()),
      match_block(gmsk_encode_bytes(sync_bytes, output_spb), 1, output_spb * 8),
      match_data(history_size1 / downsampler_block.get_decim()),
      tone_detected(0),
      sync_detected(0)
{
    assert(window.size() % 2 == 0 && stride > 0 && input_spb >= 2);
    assert(tone_halfband >= 2 && 2 * tone_halfband + 1 <= window.size());
    assert(sync_bytes.size() > 0);
    assert(input_spb % output_spb == 0);
    assert(stride % (input_spb / output_spb) == 0);
}

const std::vector<preamble_detector::packet_t>&
preamble_detector::work(const std::complex<float>* input)
{
    position += get_stride();
    const std::complex<float>* history_output = history_block.history_feed(input);

    if (position >= history_block.get_output_length() + downsampler_block.get_delay()) {
        old_packets.swap(new_packets);
        new_packets.clear();

        find_tones(history_output);
        merge_detections();

        if (false) {
            for (const packet_t& pkt : old_packets)
                print_packet(pkt);
        }
    }

    sync_detected += old_packets.size();
    return old_packets;
}

void preamble_detector::find_tones(const std::complex<float>* history_output)
{
    fft_block.work(history_output, freq_data.data(), 1);
    norm_block.work(freq_data, norm_data);

    double sum = 0;
    for (size_t i = get_window_size() / 2 - max_freq_bin - tone_halfband;
         i < get_window_size() / 2 - max_freq_bin + tone_halfband;
         i++) {
        assert(i < get_window_size());
        sum += norm_data[i];
    }

    for (size_t i = get_window_size() / 2 - max_freq_bin;
         i <= get_window_size() / 2 + max_freq_bin;
         i++) {
        assert(tone_halfband <= i && i < get_window_size() - tone_halfband);
        float a0 = norm_data[i - tone_halfband];
        float a1 = norm_data[i - 1] + norm_data[i] + norm_data[i + 1];
        float a2 = norm_data[i + tone_halfband];

        sum += a2; // sum is over 2 * tone_halfband + 1 many norms

        if (a1 * tone_snr_scale > static_cast<float>(sum) - a1) {
            if (false) {
                for (size_t j = 0; j <= 2 * tone_halfband; j++)
                    std::cout << static_cast<ssize_t>(j - tone_halfband) << " "
                              << norm_data[i - tone_halfband + j] << std::endl;
            }

            float tone_freq = static_cast<float>(i) / get_window_size() - 0.5f;
            float tone_snr_db =
                10.0f * std::log10(a1 / std::max(static_cast<float>(sum) - a1, 1e-40f));

            tone_detected += 1;
            find_syncs(history_output, tone_freq, tone_snr_db);
        }

        sum -= a0;
    }
}

void preamble_detector::find_syncs(const std::complex<float>* history_output,
                                   float tone_freq,
                                   float tone_snr_db)
{
    downsampler_block.clear_history();
    downsampler_block.set_frequency(-tone_freq);
    downsampler_block.work(history_output, downsampler_data.data(), match_data.size());

    match_block.clear_history();
    match_block.work(downsampler_data.data(), match_data.data(), match_data.size());

    size_t num_taps = match_block.get_taps().size();
    assert(match_data.size() > num_taps && num_taps > 0);

    size_t best_pos = 0;
    float best_norm = match_data[num_taps - 1];
    for (size_t i = num_taps; i < match_data.size(); i++) {
        if (match_data[i] > best_norm) {
            best_norm = match_data[i];
            best_pos = i - (num_taps - 1);
        }
    }

    float sig_norm = 0.0f;
    for (size_t i = 0; i < num_taps; i++)
        sig_norm += std::norm(downsampler_data[best_pos + i]);

    if (best_norm * sync_snr_scale > sig_norm - best_norm) {
        float sync_snr_db =
            10.0f * std::log10(best_norm / std::max(sig_norm - best_norm, 1e-40f));

        // dark magic, think about it
        uint64_t pos = position - history_block.get_output_length() -
                       downsampler_block.get_delay() +
                       best_pos * downsampler_block.get_decim();
        assert(pos <= position); // check for overflow

        packet_t pkt;
        pkt.position = pos;
        pkt.freq_offset = tone_freq;
        pkt.tone_snr_db = tone_snr_db;
        pkt.sync_snr_db = sync_snr_db;

        // downsample the rest of the packet
        downsampler_block.work(history_output +
                                   match_data.size() * downsampler_block.get_decim(),
                               downsampler_data.data() + match_data.size(),
                               downsampler_data.size() - match_data.size());

        assert(best_pos + 8 * output_bytes * output_spb + 1 <= downsampler_data.size());
        pkt.samples.insert(pkt.samples.begin(),
                           downsampler_data.data() + best_pos,
                           downsampler_data.data() + best_pos +
                               8 * output_bytes * output_spb + 1);

        new_packets.push_back(pkt);

        if (false)
            print_packet(pkt);
    }
}

bool preamble_detector::equiv_det(const packet_t& pkt1, const packet_t& pkt2) const
{
    bool same = pkt1.position <= pkt2.position + pos_accuracy;
    same &= pkt2.position <= pkt1.position + pos_accuracy;
    same &= pkt1.freq_offset <= pkt2.freq_offset + freq_accuracy;
    same &= pkt2.freq_offset <= pkt1.freq_offset + freq_accuracy;
    return same;
}

bool preamble_detector::better_det(const packet_t& pkt1, const packet_t& pkt2) const
{
    return pkt1.sync_snr_db > pkt2.sync_snr_db + 0.01f ||
           (pkt1.sync_snr_db > pkt2.sync_snr_db - 0.01f &&
            pkt1.tone_snr_db >= pkt2.tone_snr_db);
}

void preamble_detector::merge_detections()
{
    std::vector<packet_t>::iterator iter1 = old_packets.end();
    while (iter1 != old_packets.begin()) {
        packet_t& pkt1 = *(--iter1);

        bool again = false;
        for (packet_t& pkt2 : new_packets) {
            if (equiv_det(pkt1, pkt2)) {
                again = true;
                if (better_det(pkt1, pkt2))
                    pkt2 = pkt1;
            }
        }
        if (again)
            iter1 = old_packets.erase(iter1);
    }

    iter1 = new_packets.end();
    while (iter1 != new_packets.begin()) {
        packet_t& pkt1 = *(--iter1);

        std::vector<packet_t>::iterator iter2 = new_packets.end();
        while (iter2 != new_packets.begin()) {
            packet_t& pkt2 = *(--iter2);
            if (iter1 != iter2 && equiv_det(pkt2, pkt1) && better_det(pkt2, pkt1)) {
                iter1 = new_packets.erase(iter1);
                break;
            }
        }
    }
}

void preamble_detector::print_packet(float samp_rate,
                                     const packet_t& packet,
                                     std::stringstream& msg) const
{
    msg << std::setprecision(3) << std::fixed << "\"pos\": " << packet.position
        << ", \"time\": " << packet.position / samp_rate
        << ", \"freq\": " << packet.freq_offset * samp_rate
        << ", \"rate\": " << samp_rate / input_spb
        << ", \"tone_snr\": " << packet.tone_snr_db
        << ", \"sync_snr\": " << packet.sync_snr_db << ", \"raw\": \""
        << std::setfill('0') << std::hex << std::uppercase;

    std::vector<uint8_t> bytes = gmsk_hard_decode2(packet.samples, output_spb);
    for (size_t i = 0; i < std::min(bytes.size(), static_cast<size_t>(11)); i++)
        msg << std::setw(2) << static_cast<int>(bytes[i]);

    if (bytes.size() > 11)
        msg << "...";

    msg << "\"" << std::dec;
}

void preamble_detector::print_samples(const packet_t& packet,
                                      std::stringstream& msg) const
{
    msg << "\"spb\": " << output_spb << ", \"samples\": [" << std::scientific
        << std::nouppercase << std::setprecision(2);
    for (size_t i = 0; i < packet.samples.size(); i++) {
        if (i > 0)
            msg << ",";
        msg << packet.samples[i].real() << "," << packet.samples[i].imag();
    }
    msg << "]";
}

void preamble_detector::print_packet(const packet_t& packet) const
{
    std::stringstream msg;
    print_packet(50000.0f, packet, msg);
    msg << std::endl;
    std::cout << msg.str();
}

packet_extractor::packet_extractor(size_t stride,
                                   size_t input_spb,
                                   size_t output_spb,
                                   size_t num_bytes)
    : stride(stride),
      input_spb(input_spb),
      output_spb(output_spb),
      num_bytes(num_bytes),
      position(0),
      downsampler_block(input_spb, output_spb),
      downsampler_data((num_bytes + 2) * 8 * output_spb + 1),
      history_block(
          stride, downsampler_block.get_taps().size() + (num_bytes + 10) * 8 * input_spb)
{
    assert(stride > 0 && num_bytes > 0);
    assert(input_spb > 0 && output_spb > 0);
    assert(input_spb % output_spb == 0);
}

const std::vector<packet_extractor::packet_t>& packet_extractor::work(
    const std::complex<float>* input,
    const std::vector<preamble_detector::packet_t>& new_detector_packets)
{
    for (const preamble_detector::packet_t& detector_packet : new_detector_packets)
        detector_packets.push_back(detector_packet);

    position += get_stride();
    const std::complex<float>* history_output = history_block.history_feed(input);

    if (position + 16 * input_spb >=
        history_block.get_output_length() + downsampler_block.get_delay()) {
        uint64_t pos = position + 16 * input_spb - history_block.get_output_length() -
                       downsampler_block.get_delay();

        while (!detector_packets.empty()) {
            preamble_detector::packet_t& detector_packet = detector_packets.front();

            if (detector_packet.position < pos) {
                detector_packets.pop_front();
                // std::cout << "too late" << std::endl;
            } else if (detector_packet.position < pos + get_stride()) {
                downsampler_block.clear_history();
                downsampler_block.set_frequency(-detector_packet.freq_offset);
                downsampler_block.work(history_output + (detector_packet.position - pos),
                                       downsampler_data.data(),
                                       downsampler_data.size());

                detector_packets.pop_front();
            } else {
                // std::cout << "too early" << std::endl;
                break;
            }
        }
    }

    return packets;
}

viterbi_demod::viterbi_demod(size_t spb, bool force_gen)
    : spb(spb), force_gen(force_gen), edge_samples(create_edge_samples(spb))
{
}

std::vector<std::complex<float>> viterbi_demod::create_edge_samples(size_t spb)
{
    std::vector<std::complex<float>> edge_samples;

    std::vector<bool> bits(EDGE_BITS);
    for (size_t edge = 0; edge < EDGES; edge++) {
        for (size_t i = 0; i < bits.size(); i++)
            bits[i] = ((edge >> (EDGE_BITS - 1 - i)) & 0x1) != 0;
        std::vector<std::complex<float>> samples = gmsk_encode_bits(bits, spb);
        for (std::complex<float>& s : samples)
            edge_samples.push_back(std::conj(s));
    }

    return edge_samples;
}

float viterbi_demod::get_edge_weight_gen(const std::complex<float>* samples,
                                         size_t step,
                                         size_t edge) const
{
    const std::complex<float>* input1 = samples + spb * step;
    const std::complex<float>* input2 =
        edge_samples.data() + edge * (EDGE_BITS * spb + 1);

    std::complex<float> w0(0.0f, 0.0f);
    std::complex<float> w1(0.0f, 0.0f);
    std::complex<float> w2(0.0f, 0.0f);
    std::complex<float> w3(0.0f, 0.0f);

    size_t i = 0;
    for (; i + 3 < EDGE_BITS * spb + 1; i += 4) {
        w0 += input1[i] * input2[i];
        w1 += input1[i + 1] * input2[i + 1];
        w2 += input1[i + 2] * input2[i + 2];
        w3 += input1[i + 3] * input2[i + 3];
    }

    if (i < EDGE_BITS * spb + 1)
        w0 += input1[i] * input2[i];
    if (i + 1 < EDGE_BITS * spb + 1)
        w1 += input1[i + 1] * input2[i + 1];
    if (i + 2 < EDGE_BITS * spb + 1)
        w2 += input1[i + 2] * input2[i + 2];

    return std::norm(w0 + w1 + w2 + w3);
}

float viterbi_demod::get_edge_weight_21(const std::complex<float>* samples,
                                        size_t step,
                                        size_t edge) const
{
    const std::complex<float>* input1 = samples + spb * step;
    const std::complex<float>* input2 = edge_samples.data() + edge * 21;

    std::complex<float> w0 = input1[0] * input2[0];
    std::complex<float> w1 = input1[1] * input2[1];
    std::complex<float> w2 = input1[2] * input2[2];
    std::complex<float> w3 = input1[3] * input2[3];
    w0 += input1[4] * input2[4];
    w1 += input1[5] * input2[5];
    w2 += input1[6] * input2[6];
    w3 += input1[7] * input2[7];
    w0 += input1[8] * input2[8];
    w1 += input1[9] * input2[9];
    w2 += input1[10] * input2[10];
    w3 += input1[11] * input2[11];
    w0 += input1[12] * input2[12];
    w1 += input1[13] * input2[13];
    w2 += input1[14] * input2[14];
    w3 += input1[15] * input2[15];
    w0 += input1[16] * input2[16];
    w1 += input1[17] * input2[17];
    w2 += input1[18] * input2[18];
    w3 += input1[19] * input2[19];
    w0 += input1[20] * input2[20];

    return std::norm(w0 + w1 + w2 + w3);
}

float viterbi_demod::get_edge_weight_25(const std::complex<float>* samples,
                                        size_t step,
                                        size_t edge) const
{
    const std::complex<float>* input1 = samples + spb * step;
    const std::complex<float>* input2 = edge_samples.data() + edge * 25;

    std::complex<float> w0 = input1[0] * input2[0];
    std::complex<float> w1 = input1[1] * input2[1];
    std::complex<float> w2 = input1[2] * input2[2];
    std::complex<float> w3 = input1[3] * input2[3];
    w0 += input1[4] * input2[4];
    w1 += input1[5] * input2[5];
    w2 += input1[6] * input2[6];
    w3 += input1[7] * input2[7];
    w0 += input1[8] * input2[8];
    w1 += input1[9] * input2[9];
    w2 += input1[10] * input2[10];
    w3 += input1[11] * input2[11];
    w0 += input1[12] * input2[12];
    w1 += input1[13] * input2[13];
    w2 += input1[14] * input2[14];
    w3 += input1[15] * input2[15];
    w0 += input1[16] * input2[16];
    w1 += input1[17] * input2[17];
    w2 += input1[18] * input2[18];
    w3 += input1[19] * input2[19];
    w0 += input1[20] * input2[20];
    w1 += input1[21] * input2[21];
    w2 += input1[22] * input2[22];
    w3 += input1[23] * input2[23];
    w0 += input1[24] * input2[24];

    return std::norm(w0 + w1 + w2 + w3);
}

float viterbi_demod::get_edge_weight_26(const std::complex<float>* samples,
                                        size_t step,
                                        size_t edge) const
{
    const std::complex<float>* input1 = samples + spb * step;
    const std::complex<float>* input2 = edge_samples.data() + edge * 26;

    std::complex<float> w0 = input1[0] * input2[0];
    std::complex<float> w1 = input1[1] * input2[1];
    std::complex<float> w2 = input1[2] * input2[2];
    std::complex<float> w3 = input1[3] * input2[3];
    w0 += input1[4] * input2[4];
    w1 += input1[5] * input2[5];
    w2 += input1[6] * input2[6];
    w3 += input1[7] * input2[7];
    w0 += input1[8] * input2[8];
    w1 += input1[9] * input2[9];
    w2 += input1[10] * input2[10];
    w3 += input1[11] * input2[11];
    w0 += input1[12] * input2[12];
    w1 += input1[13] * input2[13];
    w2 += input1[14] * input2[14];
    w3 += input1[15] * input2[15];
    w0 += input1[16] * input2[16];
    w1 += input1[17] * input2[17];
    w2 += input1[18] * input2[18];
    w3 += input1[19] * input2[19];
    w0 += input1[20] * input2[20];
    w1 += input1[21] * input2[21];
    w2 += input1[22] * input2[22];
    w3 += input1[23] * input2[23];
    w0 += input1[24] * input2[24];
    w1 += input1[25] * input2[25];

    return std::norm(w0 + w1 + w2 + w3);
}

float viterbi_demod::get_edge_weight_31(const std::complex<float>* samples,
                                        size_t step,
                                        size_t edge) const
{
    const std::complex<float>* input1 = samples + spb * step;
    const std::complex<float>* input2 = edge_samples.data() + edge * 31;

    std::complex<float> w0 = input1[0] * input2[0];
    std::complex<float> w1 = input1[1] * input2[1];
    std::complex<float> w2 = input1[2] * input2[2];
    std::complex<float> w3 = input1[3] * input2[3];
    w0 += input1[4] * input2[4];
    w1 += input1[5] * input2[5];
    w2 += input1[6] * input2[6];
    w3 += input1[7] * input2[7];
    w0 += input1[8] * input2[8];
    w1 += input1[9] * input2[9];
    w2 += input1[10] * input2[10];
    w3 += input1[11] * input2[11];
    w0 += input1[12] * input2[12];
    w1 += input1[13] * input2[13];
    w2 += input1[14] * input2[14];
    w3 += input1[15] * input2[15];
    w0 += input1[16] * input2[16];
    w1 += input1[17] * input2[17];
    w2 += input1[18] * input2[18];
    w3 += input1[19] * input2[19];
    w0 += input1[20] * input2[20];
    w1 += input1[21] * input2[21];
    w2 += input1[22] * input2[22];
    w3 += input1[23] * input2[23];
    w0 += input1[24] * input2[24];
    w1 += input1[25] * input2[25];
    w2 += input1[26] * input2[26];
    w3 += input1[27] * input2[27];
    w0 += input1[28] * input2[28];
    w1 += input1[29] * input2[29];
    w2 += input1[30] * input2[30];

    return std::norm(w0 + w1 + w2 + w3);
}

void viterbi_demod::find_weights(const std::complex<float>* samples, size_t samples_size)
{
    assert(samples_size >= 1 + NODE_BITS * spb && (samples_size - 1) % spb == 0);
    size_t num_bits = (samples_size - 1) / spb;

    weights.resize((num_bits - NODE_BITS) * EDGES);
    if (!force_gen && EDGE_BITS * spb + 1 == 21) {
        for (size_t i = 0; i < weights.size(); i++)
            weights[i] = get_edge_weight_21(samples, i / EDGES, i % EDGES);
    } else if (!force_gen && EDGE_BITS * spb + 1 == 25) {
        for (size_t i = 0; i < weights.size(); i++)
            weights[i] = get_edge_weight_25(samples, i / EDGES, i % EDGES);
    } else if (!force_gen && EDGE_BITS * spb + 1 == 26) {
        for (size_t i = 0; i < weights.size(); i++)
            weights[i] = get_edge_weight_26(samples, i / EDGES, i % EDGES);
    } else if (!force_gen && EDGE_BITS * spb + 1 == 31) {
        for (size_t i = 0; i < weights.size(); i++)
            weights[i] = get_edge_weight_31(samples, i / EDGES, i % EDGES);
    } else {
        for (size_t i = 0; i < weights.size(); i++)
            weights[i] = get_edge_weight_gen(samples, i / EDGES, i % EDGES);
    }
}

void viterbi_demod::find_paths(size_t init_node)
{
    assert(NODE_BITS >= 1);

    forward.resize(weights.size() / EDGES + 1);
    soft_bits.resize(forward.size() - 1);

    for (size_t node = 0; node < NODES; node++)
        forward[0][node] = -1e30f;

    assert(init_node < NODES);
    forward[0][init_node] = 0.0f;

    for (size_t step = 0; step < forward.size() - 1; step++) {
        for (size_t node = 0; node < NODES; node++) {
            size_t edge0 = node;
            float weight0 = forward[step][edge0 >> 1];
            weight0 += weights[step * EDGES + edge0];

            size_t edge1 = node + (1 << NODE_BITS);
            float weight1 = forward[step][edge1 >> 1];
            weight1 += weights[step * EDGES + edge1];

            forward[step + 1][node] = std::max(weight0, weight1);
        }
    }

    float* backward0 = this->backward0.data();
    float* backward1 = this->backward1.data();

    for (size_t node = 0; node < NODES; node++)
        backward0[node] = forward[forward.size() - 1][node];

    for (size_t step = forward.size() - 1; step > 0; step--) {
        float path0 = 0.0f;
        float path1 = 0.0f;

        for (size_t node = 0; node < NODES; node += 2) {
            path0 = std::max(path0, forward[step][node] + backward0[node]);
            path1 = std::max(path1, forward[step][node + 1] + backward0[node + 1]);

            size_t edge0 = node << 1;
            size_t edge1 = (node << 1) + 1;
            size_t edge2 = (node << 1) + 2;
            size_t edge3 = (node << 1) + 3;

            float weight0 =
                backward0[edge0 & (NODES - 1)] + weights[(step - 1) * EDGES + edge0];

            float weight1 =
                backward0[edge1 & (NODES - 1)] + weights[(step - 1) * EDGES + edge1];

            float weight2 =
                backward0[edge2 & (NODES - 1)] + weights[(step - 1) * EDGES + edge2];

            float weight3 =
                backward0[edge3 & (NODES - 1)] + weights[(step - 1) * EDGES + edge3];

            backward1[node] = std::max(weight0, weight1);
            backward1[node + 1] = std::max(weight2, weight3);
        }

        soft_bits[step - 1] = path1 - path0;
        std::swap(backward0, backward1);
    }
}

void viterbi_demod::print_stats() const
{
    std::cout << "forward" << std::endl;
    for (size_t step = 0; step < forward.size(); step++) {
        for (size_t node = 0; node < NODES; node++) {
            std::cout << step << " " << node << " " << forward[step][node] << std::endl;
        }
    }

    std::cout << "backward" << std::endl;
    for (size_t node = 0; node < NODES; node++) {
        std::cout << node << " " << backward0[node] << " " << backward1[node]
                  << std::endl;
    }

    std::cout << "soft bits" << std::endl;
    for (size_t bit = 0; bit < soft_bits.size(); bit++)
        std::cout << bit << " " << soft_bits[bit] << std::endl;
}

const std::vector<float>&
viterbi_demod::demodulate(const std::complex<float>* samples,
                          size_t samples_size,
                          const std::vector<uint8_t>& init_bytes)
{
    assert(init_bytes.size() * 8 >= NODE_BITS);
    assert(samples_size > init_bytes.size() * 8 * spb);

    size_t init_node = 0;
    for (size_t i = 0; i < init_bytes.size(); i++)
        init_node = (init_node << 8) + init_bytes[i];
    init_node &= NODES - 1;

    samples += (init_bytes.size() * 8 - NODE_BITS) * spb;
    samples_size -= (init_bytes.size() * 8 - NODE_BITS) * spb;

    find_weights(samples, samples_size);
    find_paths(init_node);
    return soft_bits;
}
