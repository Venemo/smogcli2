/*
 * Copyright 2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "blocks.hpp"
#include <cmath>
#include <iostream>
#include <limits>
#include <sstream>

#if defined(__ARM_NEON__) && !defined(__NO_SIMD__)
#include <arm_neon.h>
#endif

file_sink::file_sink(format_t format, uint32_t sample_rate)
    : format(format),
      item_size(get_item_size(format)),
      sample_rate(sample_rate),
      write_to_stdout(false)
{
}

unsigned int file_sink::get_item_size(format_t format)
{
    switch (format) {
    case CU8:
        return 1;
    case CS16:
    case WAVCS16:
        return 2;
    case CF32:
    case WAVCF32:
        return 4;
    default:
        assert(false);
        return 0;
    }
}

void file_sink::open(const char* filename)
{
    count = 0;
    write_to_stdout = std::strcmp(filename, "-") == 0;
    if (write_to_stdout)
        return;

    file.open(filename, std::ofstream::binary);
    if (file.fail()) {
        std::stringstream msg;
        msg << "WARNING: could not open " << filename << " for writing" << std::endl;
        std::cerr << msg.str();
        file.close();
        return;
    }

    if (format == WAVCS16 || format == WAVCF32)
        prepare_wav();
}

void file_sink::close()
{
    if (write_to_stdout) {
        write_to_stdout = false;
        return;
    }

    if (format == WAVCS16 || format == WAVCF32)
        finalize_wav();

    file.close();
}

size_t file_sink::write(const void* input, size_t length)
{
    assert(length <= std::numeric_limits<std::streamsize>::max());

    if (write_to_stdout) {
        std::cout.write(static_cast<const char*>(input),
                        static_cast<std::streamsize>(length));
        return 0;
    }

    file.write(static_cast<const char*>(input), static_cast<std::streamsize>(length));

    if (file.good()) {
        count += length;
        return 0;
    } else
        return length;
}

void file_sink::prepare_wav()
{
    uint32_t bps =
        sample_rate * item_size * 2; // (Sample Rate * BitsPerSample * Channels) / 8

    file << "RIFF----WAVEfmt ";
    write_word(file, 16, 4);            // Length of format data
    write_word(file, 1, 2);             // PCM - integer samples
    write_word(file, 2, 2);             // Number of channels
    write_word(file, sample_rate, 4);   // Sample rate
    write_word(file, bps, 4);           // bps
    write_word(file, item_size * 2, 2); // (BitsPerSample * Channels) / 8
    write_word(file, item_size * 8, 2); // Bits per sample
    file << "data----";
}

void file_sink::finalize_wav()
{
    uint32_t file_length = file.tellp();
    file.seekp(4);
    write_word(file, file_length - 8, 4);
    file.seekp(40);
    write_word(file, file_length - 44, 4);
    file.close();
}

bool string_ends_with(const char* string, const char* ending)
{
    size_t len1 = std::strlen(string);
    size_t len2 = std::strlen(ending);
    if (len1 < len2)
        return false;

    for (size_t i = 0; i < len2; i++)
        if (string[len1 - len2 + i] != ending[i])
            return false;

    return true;
}

void file_source::open(const char* filename, format_t format)
{
    file.open(filename, std::ifstream::binary);
    count = 0;

    if (format == AUTO) {
        std::string name(filename);
        if (string_ends_with(filename, ".cf32") || string_ends_with(filename, ".f32"))
            format = CF32;
        else if (string_ends_with(filename, ".cs16") || string_ends_with(filename, "s16"))
            format = CS16;
        else if (string_ends_with(filename, "cu8") || string_ends_with(filename, ".u8"))
            format = CU8;
        else {
            std::stringstream msg;
            msg << "WARNING: unknown file extension, defaulting to CF32" << std::endl;
            std::cerr << msg.str();
            format = CF32;
        }
    }
    this->format = format;

    if (file.fail()) {
        std::stringstream msg;
        msg << "WARNING: could not open " << filename << " for reading" << std::endl;
        std::cerr << msg.str();
    }
}

void file_source::close() { file.close(); }

void file_source::rewind()
{
    if (file.is_open()) {
        file.clear();
        file.seekg(0, std::ios::beg);
        count = 0;
    }
}

size_t file_source::read(void* output, size_t length)
{
    assert(length <= std::numeric_limits<std::streamsize>::max());
    file.read(static_cast<char*>(output), static_cast<std::streamsize>(length));
    size_t length2 = static_cast<size_t>(file.gcount());
    assert(length2 <= length);

    count += length2;
    return length - length2;
}

size_t file_source::read_float(float* output, size_t length)
{
    if (format == CF32) {
        size_t unread = read(output, length * sizeof(float));
        return (unread + sizeof(float) - 1) / sizeof(float);
    } else if (format == CS16) {
        int16_t* output2 = reinterpret_cast<int16_t*>(output);
        size_t unread = read(output2, length * sizeof(int16_t));
        unread = (unread + sizeof(int16_t) - 1) / sizeof(int16_t);

        float scaling = 1.0f / 32768;
        size_t i = length - unread;
        while (i-- > 0)
            output[i] = output2[i] * scaling;

        return unread;
    } else if (format == CU8) {
        uint8_t* output2 = reinterpret_cast<uint8_t*>(output);
        size_t unread = read(output2, length);

        float scaling = 1.0f / 128;
        size_t i = length - unread;
        while (i-- > 0) {
            int a = output2[i];
            output[i] = (a - 128) * scaling;
        }

        return unread;
    } else {
        assert(false);
        return length;
    }
}

void convert_u8_f32::work(const uint8_t* input, float* output, size_t length)
{
    if (scaling != 1.0f) {
        for (size_t n = 0; n < length; n++) {
            int a0 = *(input++);
            *(output++) = (a0 - 128) * scaling;
        }
    } else {
        for (size_t n = 0; n < length; n++) {
            int a0 = *(input++);
            *(output++) = a0 - 128;
        }
    }
}

#if defined(__ARM_NEON__) && !defined(__NO_SIMD__)

void sum_max_u8::work(const uint8_t* input, size_t length)
{
    int8x8_t x = vdup_n_s8(-128);
    uint16x8_t m0 = vdupq_n_u16(0);
    uint32x4_t s0 = vdupq_n_u32(0);
    uint32x4_t s1 = vdupq_n_u32(0);

    while (length % 16 != 0) {
        unsigned int a = static_cast<unsigned int>(std::abs(*input - 128));
        m0[0] = std::max(m0[0], static_cast<uint16_t>(a));
        s0[0] += a;

        input += 1;
        length -= 1;
    }

    for (size_t i = 0; i < length; i += 16) {
        int8x8_t a = vadd_s8(vld1_s8((const int8_t*)input + i), x);
        uint16x8_t b = vreinterpretq_u16_s16(vabsq_s16(vmovl_s8(a)));
        m0 = vmaxq_u16(m0, b);
        s0 = vaddq_u32(s0, vmovl_u16(vget_low_u16(b)));
        s1 = vaddq_u32(s1, vmovl_u16(vget_high_u16(b)));

        a = vadd_s8(vld1_s8((const int8_t*)input + i + 8), x);
        b = vreinterpretq_u16_s16(vabsq_s16(vmovl_s8(a)));
        m0 = vmaxq_u16(m0, b);
        s0 = vaddq_u32(s0, vmovl_u16(vget_low_u16(b)));
        s1 = vaddq_u32(s1, vmovl_u16(vget_high_u16(b)));
    }

    uint32x4_t m1 = vmovl_u16(vmax_u16(vget_low_u16(m0), vget_high_u16(m0)));
    uint32x2_t m2 = vmax_u32(vget_low_u32(m1), vget_high_u32(m1));
    max = std::max(m2[0], m2[1]);

    uint32x4_t s2 = vaddq_u32(s0, s1);
    uint32x2_t s3 = vadd_u32(vget_low_u32(s2), vget_high_u32(s2));
    sum = s3[0] + s3[1];
}

#else

void sum_max_u8::work(const uint8_t* input, size_t length)
{
    sum = 0;
    max = 0;
    for (size_t i = 0; i < length; i++) {
        unsigned int a = static_cast<unsigned int>(std::abs(input[i] - 128));
        sum += a;
        max = std::max(max, a);
    }
}

#endif

void convert_i16_f32::work(const int16_t* input, float* output, size_t length)
{
    if (scaling != 1.0f) {
        for (size_t n = 0; n < length; n++)
            *(output++) = *(input++) * scaling;
    } else {
        for (size_t n = 0; n < length; n++)
            *(output++) = *(input++);
    }
}

void sinusoid_source_cf32::set_frequency(float frequency)
{
    assert(-0.5 <= frequency && frequency <= 0.5);
    this->frequency = frequency;

    float angle = frequency * 6.283185482025146484375f;
    std::complex<float> rotation(std::cos(angle), std::sin(angle));
    this->rotation = rotation;
}

void sinusoid_source_cf32::work(std::complex<float>* output, size_t length)
{
    std::complex<float> rotation(this->rotation);
    std::complex<float> sample(this->sample);

    sample /= std::abs(sample);
    for (size_t n = 0; n < length; n++) {
        sample *= rotation;
        output[n] = sample;
    }

    this->sample = sample;
}

void multiply_cf32::work(const std::complex<float>* input1,
                         const std::complex<float>* input2,
                         std::complex<float>* output,
                         size_t length)
{
    for (size_t n = 0; n < length; n++)
        *(output++) = *(input1++) * *(input2++);
}

void norm_cf32::work(const std::complex<float>* input, float* output, size_t length)
{
    for (size_t n = 0; n < length; n += 1)
        output[n] = std::norm(input[n]);
}
