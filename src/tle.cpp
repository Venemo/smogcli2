/*
 * Copyright 2020 Peter Horvath.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "predict.h"
#include "tle.hpp"
#include <curl/curl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>

const double sat_tracker::tle_refresh_days = 1.0 / 4.0;

size_t sat_tracker::curl_write_memory_callback(void* contents,
                                               size_t size,
                                               size_t nmemb,
                                               void* userp)
{
    size_t realsize = size * nmemb;
    memory_struct* mem = (memory_struct*)userp;

    char* ptr = (char*)std::realloc(mem->memory, mem->size + realsize + 1);
    if (ptr == NULL) {
        std::cout << "WARNING: not enough memory to download TLE update" << std::endl;
        return 0;
    }
    mem->memory = ptr;
    std::memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

int sat_tracker::download_tle(const std::string& tle_file_name)
{
    std::string tle_url = "http://www.celestrak.com/NORAD/elements/";
    tle_url += tle_file_name;

    std::cerr << "INFO: downloading TLE from " << tle_url << std::endl;

    struct memory_struct chunk;

    chunk.memory =
        (char*)std::malloc(1); /* will be grown as needed by the realloc above */
    chunk.size = 0;            /* no data at this point */

    const bool verbose = false;
    int ret = 0;

    if (curl_global_init(CURL_GLOBAL_ALL) != 0) {
        std::cerr << "WARNING: CURL global init failed, not downloading TLE updates."
                  << std::endl;
        ret = -2;
    } else {
        CURL* curl_handle = curl_easy_init();
        if (curl_handle == NULL) {
            std::cerr << "WARNING: CURL easy init failed, not downloading TLW updates."
                      << std::endl;
            ret = -3;
        } else {
            CURLcode code = curl_easy_setopt(curl_handle, CURLOPT_URL, tle_url.c_str());

            if (code == 0 && verbose)
                code = curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);

            if (code == 0)
                code = curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);

            if (code == 0)
                code = curl_easy_setopt(
                    curl_handle, CURLOPT_WRITEFUNCTION, curl_write_memory_callback);

            if (code == 0)
                code = curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void*)&chunk);

            if (code != 0) {
                std::cerr << "WARNING: CURL easy setopt failed, aborting TLE download."
                          << std::endl;
                ret = -4;
            } else {
                code = curl_easy_perform(curl_handle);
                if (code != CURLE_OK) {
                    std::cerr
                        << "WARNING: CURL easy perform failed, aborting TLE download."
                        << std::endl;
                    ret = -5;
                } else {
                    std::ofstream tlefile;
                    tlefile.open("tle.txt", std::ios::out);
                    if (tlefile.is_open()) {
                        tlefile.write(chunk.memory,
                                      static_cast<std::streamsize>(chunk.size));
                        tlefile.close();

                        std::cerr << "INFO: TLE (" << tle_file_name
                                  << ") downloaded successfully, saved as tle.txt"
                                  << std::endl;
                    } else {
                        std::cerr << "WARNING: could not open tle.txt for writing. "
                                     "Aborting TLE download."
                                  << std::endl;
                        ret = -1;
                    }
                }
            }

            curl_easy_cleanup(curl_handle);
        }
        curl_global_cleanup();
    }

    std::free(chunk.memory);

    return ret;
}

int sat_tracker::read_files()
{
    tracking_possible = false;
    std::vector<long> catnums;
    std::vector<const char*> names;

    for (const sat2_t& sat : sats) {
        if (sat.catnum != 0)
            catnums.push_back(sat.catnum);
        if (!sat.name.empty())
            names.push_back(sat.name.c_str());
    }

    catnums.push_back(0);
    names.push_back(NULL);

    int res = read_data_files(catnums.data(), names.data(), station);

    switch (res) {
    case 0:
        std::cerr << "WARNING: TLE and QTH files missing, disabling tracking"
                  << std::endl;
        return -1;
    case 1:
        std::cerr << "WARNING: TLE file missing, disabling tracking" << std::endl;
        return -1;
    case 2:
        std::cerr << "WARNING: QTH file missing, disabling tracking" << std::endl;
        return -1;
    case 3:
        std::cerr << "INFO: TLE and QTH files loaded" << std::endl;
        break;
    }

    for (sat2_t& sat : sats) {
        int idx = -1;
        if (sat.catnum != 0)
            idx = find_sat_idx_by_catnum(sat.catnum);
        if (idx < 0 && !sat.name.empty())
            idx = find_sat_idx_by_name(sat.name.c_str());

        sat.predict_idx = idx;
        if (idx >= 0) {
            sat.catnum = sat_catnum_by_idx(idx);
            sat.name = sat_name_by_idx(idx);
            sat.tle_line1 = sat_tle1_by_idx(idx);
            sat.tle_line2 = sat_tle2_by_idx(idx);
            std::cerr << "INFO: found TLE data for " << sat.name << " (" << sat.catnum
                      << ")" << std::endl;
        }
    }

    find_sat_idx_by_catnum(sats[0].catnum);
    double tle_age_days = calc_tle_age();
    PreCalc(0);
    std::cerr << "INFO: TLE age is " << tle_age_days << " days" << std::endl;

    tracking_possible = true;
    return 0;
}

double sat_tracker::current_daynum()
{
    // Read the system clock and return the number
    // of days since 31Dec79 00:00:00 UTC (daynum 0)

    struct timeval tptr;
    (void)gettimeofday(&tptr, NULL);

    double usecs = 0.000001 * (double)tptr.tv_usec;
    double seconds = usecs + (double)tptr.tv_sec;

    return ((seconds / 86400.0) - 3651.0);
}

sat_tracker::sat_tracker(const std::vector<struct sat_t>& sats,
                         bool update_tle,
                         const std::string& tle_file_name)
    : tle_file_name(tle_file_name), last_refresh_files(0.0), update_tle(update_tle)
{
    for (const sat_t& sat : sats) {
        sat2_t sat2;
        sat2.catnum = sat.catnum;
        sat2.downlink = sat.downlink;
        sat2.pl0 = -147.56 + 20.0 * log10(sat.downlink);
        sat2.predict_idx = -1;
        this->sats.push_back(sat2);
    }

    refresh_files(true);
}

sat_tracker::sat_tracker(const Json::Value& meta_root)
    : tle_file_name(""), last_refresh_files(0.0), tle_file_time(0), update_tle(false)
{
    if (meta_root == Json::Value::null) {
        tracking_possible = false;
        return;
    }

    const Json::Value tle = meta_root["tle"];

    sat2_t sat2;
    sat2.name = tle.get("name", "").asString();
    sat2.downlink = tle.get("downlink", 0).asUInt();
    sat2.tle_line1 = tle.get("line1", "").asString();
    sat2.tle_line2 = tle.get("line2", "").asString();
    sat2.catnum = std::stoi(sat2.tle_line1.substr(2, 5));
    sat2.predict_idx = 0;
    sat2.pl0 = -147.56 + 20.0 * log10(sat2.downlink);
    sats.push_back(sat2);
    json_to_sat_data(tle);

    const Json::Value station_root = meta_root["station"];
    json_to_station_data(station_root, station);

    PreCalc(0);
}

void sat_tracker::refresh_files(bool force_read)
{
    if (update_tle) {
        int ret = download_tle(tle_file_name);
        force_read |= ret == 0;
    } else if (!force_read) {
        std::time_t now;
        std::time(&now);

        struct stat info;
        int ret = stat("tle.txt", &info);

        if (now != (std::time_t)(-1) && ret == 0 && info.st_mtime < now - 10 &&
            info.st_mtime != tle_file_time) {

            std::cerr << "INFO: TLE file change was detected, reloading TLE data..."
                      << std::endl;
            force_read = true;
            tle_file_time = info.st_mtime;
        }
    }

    last_refresh_files = current_daynum();
    if (force_read)
        read_files();
}

void sat_tracker::calculate(bool print)
{
    bool update_is_safe = true;

    for (sat2_t& sat : sats) {
        if (sat.predict_idx < 0)
            continue;

        PreCalc(sat.predict_idx);
        sat.daynum = current_daynum();
        Calc(sat.daynum);
        sat.doppler = -(sat.downlink * ((sat_range_rate * 1000.0) / 299792458.0));
        sat.azimuth = sat_azi;
        sat.elevation = sat_ele;
        sat.range = sat_range;

        if (sat.elevation > -20.0)
            update_is_safe = false;

        if (print) {
            std::stringstream msg;
            msg << "INFO: " << sat.name << " azimuth: " << std::fixed
                << std::setprecision(1) << sat.azimuth << ", elevation: " << sat.elevation
                << ", doppler: " << int(sat.doppler + 0.5) << " Hz" << std::endl;
            std::cerr << msg.str();
        }
    }

    if (update_is_safe && current_daynum() - last_refresh_files > tle_refresh_days) {
        refresh_files(false);
    }
}

void sat_tracker::calculate(double daynum)
{
    Calc(daynum);
    sats[0].doppler = -(sats[0].downlink * ((sat_range_rate * 1000.0) / 299792458.0));
    sats[0].azimuth = sat_azi;
    sats[0].elevation = sat_ele;
    sats[0].range = sat_range;
    sats[0].path_loss = sats[0].pl0 + 20.0 * log10(sat_range * 1000);
}

std::string sat_tracker::get_filename(size_t idx, const char* device) const
{
    assert(idx < sats.size());
    const sat2_t& sat = sats[idx];

    std::stringstream filename;
    filename << sat.name << "_" << std::setprecision(8) << std::fixed << sat.daynum << "_"
             << station.callsign;

    if (device != NULL)
        filename << "_dev" << device;

    return filename.str();
}

void sat_tracker::add_json_data(size_t idx, Json::Value& root)
{
    const sat2_t& sat = sats[idx];

    Json::Value tle;
    tle["name"] = sat.name;
    tle["line1"] = sat.tle_line1;
    tle["line2"] = sat.tle_line2;
    tle["downlink"] = sat.downlink;
    tle["daynum"] = sat.daynum;
    root["tle"] = tle;

    Json::Value sta;
    sta["callsign"] = station.callsign;
    sta["latitude"] = station.stnlat;
    sta["longitude"] = station.stnlong;
    sta["altitude"] = station.stnalt;
    root["station"] = sta;
}

void sat_tracker::print_packet(double daynum_t1, double delta_t12, std::stringstream& msg)
{
    calculate(daynum_t1);
    msg << std::setprecision(1) << std::fixed << "\"az1\": " << sats[0].azimuth
        << ", \"el1\": " << sats[0].elevation << ", \"range1\": " << int(sats[0].range)
        << ", \"dopp1\": " << int(sats[0].doppler) << ", \"pl1\": " << sats[0].path_loss;

    calculate(daynum_t1 + delta_t12);
    msg << std::setprecision(1) << std::fixed << ", \"az2\": " << sats[0].azimuth
        << ", \"el2\": " << sats[0].elevation << ", \"range2\": " << int(sats[0].range)
        << ", \"dopp2\": " << int(sats[0].doppler);
}
