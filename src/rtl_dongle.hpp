/*
 * Copyright 2019-2020 Peter Horvath.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef RTL_DONGLE_HPP
#define RTL_DONGLE_HPP

#include "buffer.hpp"
#include <rtl-sdr.h>
#include <array>
#include <atomic>
#include <complex>
#include <thread>

class rtl_dongle
{
public:
    rtl_dongle(buffer_t& buffer,
               float scaling = 1.0f / 128.0f,
               const char* device_string = NULL);
    ~rtl_dongle();

    int set_tuner_gain(int gain = -1);
    int set_bias_tee(bool enable = false);
    int set_ppm_error(int corr = 0);

    int set_center_freq(uint32_t freq);
    uint32_t get_center_freq() const { return center_freq; }

    int set_sampling_rate(uint32_t rate);
    uint32_t get_sampling_rate() const { return sampling_rate; }

    void start();
    void stop();

    bool is_opened() const { return dev != NULL; }
    std::array<float, 2> get_statistics();
    unsigned int get_overruns() const { return overruns; }
    const char* get_device_name() const { return device_name.c_str(); }

protected:
    rtlsdr_dev_t* dev;
    std::string device_name;
    uint32_t center_freq;
    uint32_t sampling_rate;
    std::thread thread;
    convert_u8_f32 convert;
    sum_max_u8 sum_max;
    std::vector<float> temp;
    buffer_t& buffer;

    std::mutex mutex;
    uint64_t samples_max_sum;
    unsigned int samples_max_cnt;
    uint64_t samples_avg_sum;
    unsigned int samples_avg_cnt;
    unsigned int overruns;

    void worker();
    static void buffer_writer(uint8_t* buf, uint32_t len, void* ctx);
    void buffer_writer2(uint8_t* buf, uint32_t len);

    static int verbose_device_search(const char* device_string);
    int nearest_gain(int target_gain);
};

#endif // RTL_DONGLE_HPP
