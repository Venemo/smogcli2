/*
 * Copyright 2020 Peter Horvath.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "blocks.hpp"
#include <iostream>

int main(int argc, char* argv[])
{
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " <sample_rate> <infile.cf32>" << std::endl;
        return -1;
    }

    int sample_rate = std::atoi(argv[1]);
    if (sample_rate <= 0) {
        std::cerr << "Bad sample rate!" << std::endl;
        return -1;
    }

    std::cerr << "Using output sample rate " << sample_rate << std::endl;

    int argind = 2;

    while (argind < argc) {
        std::string filename = argv[argind++];

        size_t pos = filename.find_last_of('.');
        if (pos == std::string::npos)
            pos = filename.size();
        std::string filename2 = filename.substr(0, pos) + ".wav";

        std::cerr << "INFO: reading " << filename;
        std::cerr << ", writing " << filename2;
        std::cerr << std::endl;

        file_source src;
        file_sink sink(file_sink::WAVCS16, static_cast<uint32_t>(sample_rate));

        std::vector<float> inbuf(1024);
        std::vector<int16_t> outbuf(1024);

        src.open(filename.c_str(), file_source::AUTO);

        float max_abs = 0.0f;
        for (;;) {
            size_t unread = src.read_float(inbuf);
            for (size_t i = 0; i + unread < inbuf.size(); i++) {
                float act_abs = std::abs(inbuf[i]);
                if (act_abs > max_abs) {
                    max_abs = act_abs;
                }
            }
            if (unread != 0)
                break;
        }

        float norm_fact = 0.95f / max_abs;
        std::cerr << "Max. input sample: " << max_abs << ", normalizing by " << norm_fact
                  << std::endl;
        norm_fact *= 32767.0f;

        src.rewind();
        sink.open(filename2.c_str());

        for (;;) {
            size_t unread = src.read_float(inbuf);
            for (size_t i = 0; i + unread < inbuf.size(); i++) {
                outbuf[i] = static_cast<int16_t>(norm_fact * inbuf[i]);
            }
            sink.write(outbuf);
            if (unread != 0)
                break;
        }

        src.close();
        sink.close();

        std::cerr << "Written " << sink.byte_count() << " bytes." << std::endl;
    }
    return 0;
}
