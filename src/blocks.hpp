/*
 * Copyright 2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef BLOCKS_HPP
#define BLOCKS_HPP

#include <cassert>
#include <complex>
#include <cstring>
#include <fstream>
#include <vector>

class file_sink
{
public:
    enum format_t { CU8, CS16, CF32, WAVCS16, WAVCF32 };

    file_sink(format_t format, uint32_t sample_rate);
    void open(const char* filename);
    void close();
    bool is_open() const { return write_to_stdout || file.is_open(); }

    // returns the amount of bytes NOT written, zero if fully written
    size_t write(const void* input, size_t length);
    size_t byte_count() const { return count; }

    template <typename ELEM>
    size_t write(const std::vector<ELEM>& input)
    {
        size_t unwritten = write(input.data(), input.size() * sizeof(ELEM));
        return (unwritten + sizeof(ELEM) - 1) / sizeof(ELEM);
    }

protected:
    const format_t format;
    const unsigned int item_size;
    const uint32_t sample_rate;

    bool write_to_stdout;
    std::ofstream file;
    size_t count;

    static unsigned int get_item_size(format_t format);
    void prepare_wav();
    void finalize_wav();

    template <typename T>
    static std::ostream&
    write_word(std::ostream& outs, T value, unsigned size = sizeof(T))
    {
        for (; size; --size, value >>= 8) {
            outs.put(static_cast<char>(value & 0xff));
        }
        return outs;
    }
};

class file_source
{
public:
    enum format_t { RAW, AUTO, CU8, CS16, CF32 };

    void open(const char* filename, format_t format);
    void close();
    void rewind();
    bool is_open() const { return file.is_open(); }

    // returns the amount of bytes NOT read, zero if fully read
    size_t read(void* output, size_t length);
    size_t byte_count() const { return count; }

    template <typename ELEM>
    size_t read(std::vector<ELEM>& output)
    {
        size_t unread = read(output.data(), output.size() * sizeof(ELEM));
        return (unread + sizeof(ELEM) - 1) / sizeof(ELEM);
    }

    size_t read_float(float* output, size_t length);

    size_t read_float(std::vector<float>& output)
    {
        return read_float(output.data(), output.size());
    }

    size_t read_float(std::vector<std::complex<float>>& output)
    {
        float* output2 = reinterpret_cast<float*>(output.data());
        size_t unread = read_float(output2, 2 * output.size());
        return (unread + 1) / 2;
    }

protected:
    format_t format;
    std::ifstream file;
    size_t count;
};

class convert_u8_f32
{
public:
    const float scaling;

    convert_u8_f32(float scaling = 1.0f / 128.0f) : scaling(scaling) {}

    void work(const uint8_t* input, float* output, size_t length);

    void work(const std::vector<uint8_t>& input, std::vector<float>& output)
    {
        assert(input.size() == output.size());
        work(input.data(), output.data(), input.size());
    }

    void work(const std::vector<std::complex<uint8_t>>& input,
              std::vector<std::complex<float>>& output)
    {
        assert(input.size() == output.size());
        work(reinterpret_cast<const uint8_t*>(input.data()),
             reinterpret_cast<float*>(output.data()),
             input.size() * 2);
    }
};

class sum_max_u8
{
public:
    uint32_t sum; // this may overflow
    uint32_t max;

    // always overwrites the sum and max values
    void work(const uint8_t* input, size_t length);

    void work(const std::vector<uint8_t>& input) { work(input.data(), input.size()); }
};

class convert_i16_f32
{
public:
    const float scaling;

    convert_i16_f32(float scaling = 1.0f / 32768.0f) : scaling(scaling) {}

    void work(const int16_t* input, float* output, size_t length);

    void work(const std::vector<int16_t>& input, std::vector<float>& output)
    {
        assert(input.size() == output.size());
        work(input.data(), output.data(), input.size());
    }

    void work(const std::vector<std::complex<int16_t>>& input,
              std::vector<std::complex<float>>& output)
    {
        assert(input.size() == output.size());
        work(reinterpret_cast<const int16_t*>(input.data()),
             reinterpret_cast<float*>(output.data()),
             input.size() * 2);
    }
};

class sinusoid_source_cf32
{
public:
    sinusoid_source_cf32(float frequency) : sample(1.0f, 0.0f)
    {
        set_frequency(frequency);
    }

    void work(std::complex<float>* output, size_t length);

    void work(std::vector<std::complex<float>>& output)
    {
        work(output.data(), output.size());
    }

    void set_frequency(float frequency); // in [-0.5, 0.5] range
    float get_frequency() const { return frequency; }
    void reset() { sample = std::complex<float>(1.0f, 0.0f); }

protected:
    std::complex<float> sample;
    std::complex<float> rotation;
    float frequency; // in [-0.5, 0.5] range
};

class multiply_cf32
{
public:
    // input1: length, input2: length, output: length
    void work(const std::complex<float>* input1,
              const std::complex<float>* input2,
              std::complex<float>* output,
              size_t length);

    void work(const std::vector<std::complex<float>>& input1,
              const std::vector<std::complex<float>>& input2,
              std::vector<std::complex<float>>& output)
    {
        assert(input1.size() == output.size());
        assert(input2.size() == output.size());
        work(input1.data(), input2.data(), output.data(), output.size());
    }
};

template <typename ELEM>
class history_base
{
public:
    history_base(size_t history_stride, size_t output_length)
        : history_stride(history_stride),
          output_length(output_length),
          history_data(history_stride < output_length
                           ? output_length +
                                 4 * output_length / history_stride * history_stride
                           : 0),
          history_start(0)
    {
        assert(history_stride > 0 && output_length > 0);
    }

    size_t get_history_stride() const { return history_stride; }
    size_t get_output_length() const { return output_length; }

    // input: history_stride, output: output_length
    const ELEM* history_feed(const ELEM* input)
    {
        if (history_stride >= output_length)
            return input + history_stride - output_length;
        else if (history_start + output_length + history_stride <= history_data.size()) {
            std::memcpy(history_data.data() + history_start + output_length,
                        input,
                        history_stride * sizeof(ELEM));
            history_start += history_stride;
            return history_data.data() + history_start;
        } else {
            std::memmove(history_data.data(),
                         history_data.data() + history_start + history_stride,
                         (output_length - history_stride) * sizeof(ELEM));
            std::memcpy(history_data.data() + output_length - history_stride,
                        input,
                        history_stride * sizeof(ELEM));
            history_start = 0;
            return history_data.data();
        }
    }

    void clear_history()
    {
        if (output_length > history_stride)
            std::memset(static_cast<void*>(history_data.data() + history_stride),
                        0,
                        (output_length - history_stride) * sizeof(ELEM));
    }

protected:
    const size_t history_stride;
    const size_t output_length;

    std::vector<ELEM> history_data;
    size_t history_start;
};

class norm_cf32
{
public:
    // input: length, output: length
    void work(const std::complex<float>* input, float* output, size_t length);

    void work(const std::vector<std::complex<float>>& input, std::vector<float>& output)
    {
        assert(input.size() == output.size());
        work(input.data(), output.data(), output.size());
    }
};

#endif // BLOCKS_HPP
