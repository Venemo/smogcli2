/*
 * Copyright 2019-2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "filter.hpp"
#include <cassert>
#include <cstring>
#include <limits>

#if defined(__SSE__) && !defined(__NO_SIMD__)
#include <xmmintrin.h>
#elif defined(__ARM_NEON__) && !defined(__NO_SIMD__)
#include <arm_neon.h>
#ifdef __ARM_FEATURE_FMA
#define VFMAQ_F32(Q1, Q2, Q3) vfmaq_f32(Q1, Q2, Q3)
#else
#define VFMAQ_F32(Q1, Q2, Q3) vaddq_f32(Q1, vmulq_f32(Q2, Q3))
#endif
#endif

std::vector<float> window_gaussian(size_t length, float sigma)
{
    assert(length >= 1);
    float c = 0.5f * (length - 1);
    float d = 2.0f / (sigma * length); // was length - 1
    std::vector<float> window(length);
    for (size_t i = 0; i < (length + 1) / 2; i++) {
        float a = (i - c) * d;
        float b = std::exp(-0.5f * a * a);
        window[i] = b;
        window[window.size() - 1 - i] = b;
    }

    return window;
}

std::vector<float> window_hanning(size_t length)
{
    assert(length >= 1);
    float s = 2.0f * pi / (length - 1);
    std::vector<float> window(length);
    for (size_t i = 0; i < length; i++) {
        window[i] = 0.5f - 0.5f * std::cos(i * s);
    }
    return window;
}

float signal_rms(const std::vector<std::complex<float>>& sig)
{
    float a = 0.0f;
    for (std::complex<float> c : sig)
        a += std::norm(c);
    return std::sqrt(a / sig.size());
}

std::vector<std::complex<float>> signal_norm(const std::vector<std::complex<float>>& sig)
{
    float a = 0.0f;
    for (size_t i = 0; i < sig.size(); i++)
        a += std::norm(sig[i]);
    a = 1.0f / std::sqrt(a);

    std::vector<std::complex<float>> ret(sig.size());
    for (size_t i = 0; i < sig.size(); i++)
        ret[i] = sig[i] * a;
    return ret;
}

std::vector<std::complex<float>> signal_conj(const std::vector<std::complex<float>>& sig)
{
    std::vector<std::complex<float>> ret(sig.size());
    for (size_t i = 0; i < sig.size(); i++)
        ret[i] = std::conj(sig[i]);
    return ret;
}

std::vector<std::complex<float>> signal_scale(const std::vector<std::complex<float>>& sig,
                                              float scale)
{
    std::vector<std::complex<float>> ret(sig.size());
    for (size_t i = 0; i < sig.size(); i++)
        ret[i] = sig[i] * scale;
    return ret;
}

filter_fir_f32::filter_fir_f32(const std::vector<float>& taps, size_t decim, size_t vlen)
    : history_base(vlen * decim, vlen * taps.size()), taps(taps), decim(decim), vlen(vlen)
{
    assert(decim > 0 && vlen > 0);
}

void filter_fir_f32::work(const float* input, float* output, size_t length)
{
    for (size_t n = 0; n < length; n++) {
        for (size_t k = 0; k < vlen; k++)
            output[k] = 0.0f;

        const float* input2 = history_feed(input);
        for (size_t i = 0; i < taps.size(); i++) {
            float t = taps[i];
            for (size_t k = 0; k < vlen; k++)
                output[k] += t * input2[i * vlen + k];
        }

        input += vlen * decim;
        output += vlen;
    }
}

filter_fir_cf32::filter_fir_cf32(const std::vector<std::complex<float>>& taps,
                                 size_t decim,
                                 size_t vlen)
    : history_base(vlen * decim, vlen * taps.size()), taps(taps), decim(decim), vlen(vlen)
{
    assert(decim > 0 && vlen > 0);
}

void filter_fir_cf32::work(const std::complex<float>* input,
                           std::complex<float>* output,
                           size_t length)
{
    for (size_t n = 0; n < length; n++) {
        for (size_t k = 0; k < vlen; k++)
            output[k] = std::complex<float>(0.0f, 0.0f);

        const std::complex<float>* input2 = history_feed(input);
        for (size_t i = 0; i < taps.size(); i++) {
            std::complex<float> t = taps[i];
            for (size_t k = 0; k < vlen; k++)
                output[k] += t * input2[i * vlen + k];
        }

        input += vlen * decim;
        output += vlen;
    }
}

filter_fir_vec1_cf32::filter_fir_vec1_cf32(const std::vector<std::complex<float>>& taps,
                                           size_t decim)
    : history_base(decim, taps.size()), taps(taps), decim(decim)
{
    assert(decim > 0);
}

void filter_fir_vec1_cf32::work(const std::complex<float>* input,
                                std::complex<float>* output,
                                size_t length)
{
    for (size_t n = 0; n < length; n++) {
        std::complex<float> data(0.0f, 0.0f);

        const std::complex<float>* input2 = history_feed(input);
        for (size_t i = 0; i < taps.size(); i++)
            data += taps[i] * input2[i];

        input += decim;
        *(output++) = data;
    }
}

filter_match1_vec1_cf32::filter_match1_vec1_cf32(
    const std::vector<std::complex<float>>& taps, size_t decim)
    : history_base(decim, taps.size()), taps(signal_norm(signal_conj(taps))), decim(decim)
{
    assert(decim > 0);
    assert(taps.size() > 0);
}

void filter_match1_vec1_cf32::work(const std::complex<float>* input,
                                   float* output,
                                   size_t length)
{
    for (size_t n = 0; n < length; n++) {
        const std::complex<float>* input2 = history_feed(input);

        std::complex<float> data(0.0f, 0.0f);
        for (size_t i = 0; i < taps.size(); i++)
            data += taps[i] * input2[i];

        input += decim;
        *(output++) = std::norm(data);
    }
}

filter_match2_vec1_cf32::filter_match2_vec1_cf32(
    const std::vector<std::complex<float>>& taps, size_t decim, size_t block_len)
    : history_base(decim, taps.size()),
      taps(signal_norm(signal_conj(taps))),
      decim(decim),
      block_len(block_len),
      scale(1.0f * taps.size() / block_len)
{
    assert(decim > 0);
    assert(taps.size() > 0);
    assert(0 < block_len && block_len <= taps.size() && block_len % 8 == 0);
}

void filter_match2_vec1_cf32::work(const std::complex<float>* input,
                                   float* output,
                                   size_t length)
{
    for (size_t n = 0; n < length; n++) {
        const std::complex<float>* input2 = history_feed(input);
        float norm = 0.0f;

        for (size_t k = 0; k + block_len <= taps.size(); k += block_len) {
            std::complex<float> data(0.0f, 0.0f);
            for (size_t i = 0; i < block_len; i += 8) {
                data += taps[k + i + 0] * input2[k + i + 0];
                data += taps[k + i + 1] * input2[k + i + 1];
                data += taps[k + i + 2] * input2[k + i + 2];
                data += taps[k + i + 3] * input2[k + i + 3];
                data += taps[k + i + 4] * input2[k + i + 4];
                data += taps[k + i + 5] * input2[k + i + 5];
                data += taps[k + i + 6] * input2[k + i + 6];
                data += taps[k + i + 7] * input2[k + i + 7];
            }
            norm += std::norm(data);
        }

        input += decim;
        *(output++) = norm * scale;
    }
}

filter_fir_interp_vec2_f32::filter_fir_interp_vec2_f32(const std::vector<float>& taps,
                                                       size_t interp)
    : history_base(2, (taps.size() + interp - 1) / interp * 2),
      orig_taps(taps),
      taps(extend_taps(taps, interp)),
      interp(interp)
{
}

void filter_fir_interp_vec2_f32::work(const float* input, float* output, size_t length)
{
    for (size_t n = 0; n < length; n++) {
        for (size_t k = 0; k < interp; k++) {
            output[2 * k] = 0.0f;
            output[2 * k + 1] = 0.0f;
        }

        const float* input2 = history_feed(input);
        for (size_t i = 0; i < taps.size(); i += interp) {
            float a0 = *(input2++);
            float a1 = *(input2++);
            for (size_t k = 0; k < interp; k++) {
                float t = taps[i + k];
                output[2 * k] += t * a0;
                output[2 * k + 1] += t * a1;
            }
        }

        input += 2;
        output += 2 * interp;
    }
}

std::vector<float> filter_fir_interp_vec2_f32::extend_taps(const std::vector<float>& taps,
                                                           size_t interp)
{
    assert(interp > 0);
    std::vector<float> taps2((taps.size() + interp - 1) / interp * interp, 0.0f);

    for (size_t i = 0; i < taps.size(); i++)
        taps2[taps2.size() - taps.size() + i] = taps[i];

    return taps2;
}

filter_fir_vec2_f32::filter_fir_vec2_f32(const std::vector<float>& taps, size_t decim)
    : orig_taps(taps),
      taps(extend_taps(taps)),
      decim(decim),
      history((std::max(this->taps.size(), decim) - decim) * 2, 0.0f)
{
    assert(decim > 0);
}

std::vector<float> filter_fir_vec2_f32::extend_taps(const std::vector<float>& taps)
{
    std::vector<float> taps2((taps.size() + 3) / 4 * 4, 0.0f);

    for (size_t i = 0; i < taps.size(); i++)
        taps2[taps2.size() - taps.size() + i] = taps[i];

    return taps2;
}

void filter_fir_vec2_f32::clear_history()
{
    std::memset(history.data(), 0, history.size() * sizeof(float));
}

void filter_fir_vec2_f32::work(const float* input, float* output, size_t length)
{
    size_t pos = 0;
    size_t n = 0;
    while (pos < history.size() && n < length) {
        float res0 = 0.0f;
        float res1 = 0.0f;

        for (size_t i = 0; i < taps.size(); i++) {
            float dat0;
            float dat1;

            if (pos + i * 2 < history.size()) {
                dat0 = history[pos + i * 2];
                dat1 = history[pos + i * 2 + 1];
            } else {
                dat0 = input[pos + i * 2 - history.size()];
                dat1 = input[pos + i * 2 - history.size() + 1];
            }

            float tap = taps[i];
            res0 += tap * dat0;
            res1 += tap * dat1;
        }

        output[0] = res0;
        output[1] = res1;

        output += 2;
        pos += decim * 2;
        n += 1;
    }

    if (n < length) {
        work_straight(input + pos - history.size(), output, length - n);
        pos += (length - n) * decim * 2;
    }

    for (size_t i = 0; i < history.size(); i += 2) {
        float dat0;
        float dat1;

        if (pos + i < history.size()) {
            dat0 = history[pos + i];
            dat1 = history[pos + i + 1];
        } else {
            dat0 = input[pos + i - history.size()];
            dat1 = input[pos + i - history.size() + 1];
        }

        history[i] = dat0;
        history[i + 1] = dat1;
    }
}

#if defined(__ARM_NEON__) && !defined(__NO_SIMD__)

void filter_fir_vec2_f32::work_straight(const float* input, float* output, size_t length)
{
    for (size_t n = 0; n < length; n++) {
        float32x4_t res = vdupq_n_f32(0.0f);

        const float* data = input;
        for (size_t i = 0; i < taps.size(); i += 4) {
            float32x4_t temp = vld1q_f32(taps.data() + i);
            float32x4x2_t tap = vzipq_f32(temp, temp);

            float32x4_t data0 = vld1q_f32(data);
            float32x4_t data1 = vld1q_f32(data + 4);

            res = vmlaq_f32(res, tap.val[0], data0);
            res = vmlaq_f32(res, tap.val[1], data1);

            data += 8;
        }

        vst1_f32(output, vadd_f32(vget_low_f32(res), vget_high_f32(res)));

        input += decim * 2;
        output += 2;
    }
}

#else

void filter_fir_vec2_f32::work_straight(const float* input, float* output, size_t length)
{
    for (size_t n = 0; n < length; n++) {
        float res0 = 0.0f;
        float res1 = 0.0f;

        const float* data = input;
        for (size_t i = 0; i < taps.size(); i += 4) {
            float tap0 = taps[i];
            float tap1 = taps[i + 1];
            float tap2 = taps[i + 2];
            float tap3 = taps[i + 3];

            res0 += tap0 * data[0];
            res1 += tap0 * data[1];
            res0 += tap1 * data[2];
            res1 += tap1 * data[3];
            res0 += tap2 * data[4];
            res1 += tap2 * data[5];
            res0 += tap3 * data[6];
            res1 += tap3 * data[7];

            data += 8;
        }

        output[0] = res0;
        output[1] = res1;

        input += decim * 2;
        output += 2;
    }
}

#endif

filter_fir_tap16_dec2_vec2_f32::filter_fir_tap16_dec2_vec2_f32(
    const std::vector<float>& taps)
    : orig_taps(taps), taps(extend_taps(taps))
{
    for (size_t i = 0; i < 28; i++)
        history[i] = 0.0f;
}

std::array<float, 16>
filter_fir_tap16_dec2_vec2_f32::extend_taps(const std::vector<float>& taps)
{
    assert(taps.size() <= 16);

    std::array<float, 16> taps2;

    for (size_t i = 0; i < 16 - taps.size(); i++)
        taps2[i] = 0.0f;

    for (size_t i = 0; i < taps.size(); i++)
        taps2[16 - taps.size() + i] = taps[i];

    return taps2;
}

#if defined(__SSE__) && !defined(__NO_SIMD__)

void filter_fir_tap16_dec2_vec2_f32::work(const float* input,
                                          float* output,
                                          size_t length)
{
    __m128 tap0 = _mm_loadu_ps(taps.data() + 0);
    __m128 tap1 = _mm_loadu_ps(taps.data() + 4);
    __m128 tap2 = _mm_loadu_ps(taps.data() + 8);
    __m128 tap3 = _mm_loadu_ps(taps.data() + 12);

    __m128 data0 = _mm_loadu_ps(history.data() + 0);
    __m128 data1 = _mm_loadu_ps(history.data() + 4);
    __m128 data2 = _mm_loadu_ps(history.data() + 8);
    __m128 data3 = _mm_loadu_ps(history.data() + 12);
    __m128 data4 = _mm_loadu_ps(history.data() + 16);
    __m128 data5 = _mm_loadu_ps(history.data() + 20);
    __m128 data6 = _mm_loadu_ps(history.data() + 24);

    for (size_t n = 0; n < length; n++) {
        __m128 result = _mm_mul_ps(_mm_unpacklo_ps(tap0, tap0), data0);
        data0 = data1;
        result = _mm_add_ps(result, _mm_mul_ps(_mm_unpackhi_ps(tap0, tap0), data1));
        data1 = data2;

        result = _mm_add_ps(result, _mm_mul_ps(_mm_unpacklo_ps(tap1, tap1), data2));
        data2 = data3;
        result = _mm_add_ps(result, _mm_mul_ps(_mm_unpackhi_ps(tap1, tap1), data3));
        data3 = data4;

        result = _mm_add_ps(result, _mm_mul_ps(_mm_unpacklo_ps(tap2, tap2), data4));
        data4 = data5;
        result = _mm_add_ps(result, _mm_mul_ps(_mm_unpackhi_ps(tap2, tap2), data5));
        data5 = data6;

        result = _mm_add_ps(result, _mm_mul_ps(_mm_unpacklo_ps(tap3, tap3), data6));
        data6 = _mm_loadu_ps(input);
        result = _mm_add_ps(result, _mm_mul_ps(_mm_unpackhi_ps(tap3, tap3), data6));

        result = _mm_add_ps(result, _mm_movehl_ps(result, result));
        _mm_storel_pi(reinterpret_cast<__m64*>(output), result);

        input += 4;
        output += 2;
    }

    _mm_storeu_ps(history.data() + 0, data0);
    _mm_storeu_ps(history.data() + 4, data1);
    _mm_storeu_ps(history.data() + 8, data2);
    _mm_storeu_ps(history.data() + 12, data3);
    _mm_storeu_ps(history.data() + 16, data4);
    _mm_storeu_ps(history.data() + 20, data5);
    _mm_storeu_ps(history.data() + 24, data6);
}

#elif defined(__ARM_NEON__) && !defined(__NO_SIMD__)

void filter_fir_tap16_dec2_vec2_f32::work(const float* input,
                                          float* output,
                                          size_t length)
{
    float32x4_t tap0 = vld1q_f32(taps.data() + 0);
    float32x4_t tap1 = vld1q_f32(taps.data() + 4);
    float32x4_t tap2 = vld1q_f32(taps.data() + 8);
    float32x4_t tap3 = vld1q_f32(taps.data() + 12);

    float32x4_t data0 = vld1q_f32(history.data() + 0);
    float32x4_t data1 = vld1q_f32(history.data() + 4);
    float32x4_t data2 = vld1q_f32(history.data() + 8);
    float32x4_t data3 = vld1q_f32(history.data() + 12);
    float32x4_t data4 = vld1q_f32(history.data() + 16);
    float32x4_t data5 = vld1q_f32(history.data() + 20);
    float32x4_t data6 = vld1q_f32(history.data() + 24);

    size_t n = 0;
    for (; n < length; n += 2) {
        float32x4x2_t temp = vzipq_f32(tap0, tap0);
        float32x4_t res0 = vmulq_f32(temp.val[0], data0);
        float32x4_t res1 = vmulq_f32(temp.val[0], data1);
        res0 = vmlaq_f32(res0, temp.val[1], data1);
        res1 = vmlaq_f32(res1, temp.val[1], data2);

        temp = vzipq_f32(tap1, tap1);
        res0 = vmlaq_f32(res0, temp.val[0], data2);
        res1 = vmlaq_f32(res1, temp.val[0], data3);
        res0 = vmlaq_f32(res0, temp.val[1], data3);
        res1 = vmlaq_f32(res1, temp.val[1], data4);

        temp = vzipq_f32(tap2, tap2);
        res0 = vmlaq_f32(res0, temp.val[0], data4);
        res1 = vmlaq_f32(res1, temp.val[0], data5);
        res0 = vmlaq_f32(res0, temp.val[1], data5);
        res1 = vmlaq_f32(res1, temp.val[1], data6);

        float32x4_t data7 = vld1q_f32(input);
        float32x4_t data8 = vld1q_f32(input + 4);

        temp = vzipq_f32(tap3, tap3);
        res0 = vmlaq_f32(res0, temp.val[0], data6);
        res1 = vmlaq_f32(res1, temp.val[0], data7);
        res0 = vmlaq_f32(res0, temp.val[1], data7);
        res1 = vmlaq_f32(res1, temp.val[1], data8);

        data0 = data2;
        data1 = data3;
        data2 = data4;
        data3 = data5;
        data4 = data6;
        data5 = data7;
        data6 = data8;

        vst1_f32(output, vadd_f32(vget_low_f32(res0), vget_high_f32(res0)));
        vst1_f32(output + 2, vadd_f32(vget_low_f32(res1), vget_high_f32(res1)));

        input += 8;
        output += 4;
    }

    if (n < length) {
        float32x4x2_t temp = vzipq_f32(tap0, tap0);
        float32x4_t res0 = vmulq_f32(temp.val[0], data0);
        float32x4_t res1 = vmulq_f32(temp.val[1], data1);

        temp = vzipq_f32(tap1, tap1);
        res0 = vmlaq_f32(res0, temp.val[0], data2);
        res1 = vmlaq_f32(res1, temp.val[1], data3);

        float32x4_t data7 = vld1q_f32(input);

        temp = vzipq_f32(tap2, tap2);
        res0 = vmlaq_f32(res0, temp.val[0], data4);
        res1 = vmlaq_f32(res1, temp.val[1], data5);

        temp = vzipq_f32(tap3, tap3);
        res0 = vmlaq_f32(res0, temp.val[0], data6);
        res1 = vmlaq_f32(res1, temp.val[1], data7);

        data0 = data1;
        data1 = data2;
        data2 = data3;
        data3 = data4;
        data4 = data5;
        data5 = data6;
        data6 = data7;

        res0 = vaddq_f32(res0, res1);
        vst1_f32(output, vadd_f32(vget_low_f32(res0), vget_high_f32(res0)));
    }

    vst1q_f32(history.data() + 0, data0);
    vst1q_f32(history.data() + 4, data1);
    vst1q_f32(history.data() + 8, data2);
    vst1q_f32(history.data() + 12, data3);
    vst1q_f32(history.data() + 16, data4);
    vst1q_f32(history.data() + 20, data5);
    vst1q_f32(history.data() + 24, data6);
}

#else

void filter_fir_tap16_dec2_vec2_f32::work(const float* input,
                                          float* output,
                                          size_t length)
{
    size_t n = 0;
    for (; n + 2 <= length; n += 2) {
        float result[4];

        result[0] = taps[0] * history[0];
        result[1] = taps[0] * history[1];
        result[2] = taps[0] * history[4];
        result[3] = taps[0] * history[5];

        history[0] = history[8];
        history[1] = history[9];

        result[0] += taps[1] * history[2];
        result[1] += taps[1] * history[3];
        result[2] += taps[1] * history[6];
        result[3] += taps[1] * history[7];

        history[2] = history[10];
        history[3] = history[11];

        result[0] += taps[2] * history[4];
        result[1] += taps[2] * history[5];
        result[2] += taps[2] * history[8];
        result[3] += taps[2] * history[9];

        history[4] = history[12];
        history[5] = history[13];

        result[0] += taps[3] * history[6];
        result[1] += taps[3] * history[7];
        result[2] += taps[3] * history[10];
        result[3] += taps[3] * history[11];

        history[6] = history[14];
        history[7] = history[15];

        result[0] += taps[4] * history[8];
        result[1] += taps[4] * history[9];
        result[2] += taps[4] * history[12];
        result[3] += taps[4] * history[13];

        history[8] = history[16];
        history[9] = history[17];

        result[0] += taps[5] * history[10];
        result[1] += taps[5] * history[11];
        result[2] += taps[5] * history[14];
        result[3] += taps[5] * history[15];

        history[10] = history[18];
        history[11] = history[19];

        result[0] += taps[6] * history[12];
        result[1] += taps[6] * history[13];
        result[2] += taps[6] * history[16];
        result[3] += taps[6] * history[17];

        history[12] = history[20];
        history[13] = history[21];

        result[0] += taps[7] * history[14];
        result[1] += taps[7] * history[15];
        result[2] += taps[7] * history[18];
        result[3] += taps[7] * history[19];

        history[14] = history[22];
        history[15] = history[23];

        result[0] += taps[8] * history[16];
        result[1] += taps[8] * history[17];
        result[2] += taps[8] * history[20];
        result[3] += taps[8] * history[21];

        history[16] = history[24];
        history[17] = history[25];

        result[0] += taps[9] * history[18];
        result[1] += taps[9] * history[19];
        result[2] += taps[9] * history[22];
        result[3] += taps[9] * history[23];

        history[18] = history[26];
        history[19] = history[27];

        result[0] += taps[10] * history[20];
        result[1] += taps[10] * history[21];
        result[2] += taps[10] * history[24];
        result[3] += taps[10] * history[25];

        history[20] = input[0];
        history[21] = input[1];

        result[0] += taps[11] * history[22];
        result[1] += taps[11] * history[23];
        result[2] += taps[11] * history[26];
        result[3] += taps[11] * history[27];

        history[22] = input[2];
        history[23] = input[3];

        result[0] += taps[12] * history[24];
        result[1] += taps[12] * history[25];
        result[2] += taps[12] * input[0];
        result[3] += taps[12] * input[1];

        history[24] = input[4];
        history[25] = input[5];

        result[0] += taps[13] * history[26];
        result[1] += taps[13] * history[27];
        result[2] += taps[13] * input[2];
        result[3] += taps[13] * input[3];

        history[26] = input[6];
        history[27] = input[7];

        result[0] += taps[14] * input[0];
        result[1] += taps[14] * input[1];
        result[2] += taps[14] * input[4];
        result[3] += taps[14] * input[5];

        result[0] += taps[15] * input[2];
        result[1] += taps[15] * input[3];
        result[2] += taps[15] * input[6];
        result[3] += taps[15] * input[7];


        output[0] = result[0];
        output[1] = result[1];
        output[2] = result[2];
        output[3] = result[3];

        input += 8;
        output += 4;
    }

    for (; n < length; n++) {
        float result[4];

        result[0] = taps[0] * history[0];
        result[1] = taps[0] * history[1];
        result[2] = taps[1] * history[2];
        result[3] = taps[1] * history[3];

        history[0] = history[4];
        history[1] = history[5];
        history[2] = history[6];
        history[3] = history[7];

        result[0] += taps[2] * history[4];
        result[1] += taps[2] * history[5];
        result[2] += taps[3] * history[6];
        result[3] += taps[3] * history[7];

        history[4] = history[8];
        history[5] = history[9];
        history[6] = history[10];
        history[7] = history[11];

        result[0] += taps[4] * history[8];
        result[1] += taps[4] * history[9];
        result[2] += taps[5] * history[10];
        result[3] += taps[5] * history[11];

        history[8] = history[12];
        history[9] = history[13];
        history[10] = history[14];
        history[11] = history[15];

        result[0] += taps[6] * history[12];
        result[1] += taps[6] * history[13];
        result[2] += taps[7] * history[14];
        result[3] += taps[7] * history[15];

        history[12] = history[16];
        history[13] = history[17];
        history[14] = history[18];
        history[15] = history[19];

        result[0] += taps[8] * history[16];
        result[1] += taps[8] * history[17];
        result[2] += taps[9] * history[18];
        result[3] += taps[9] * history[19];

        history[16] = history[20];
        history[17] = history[21];
        history[18] = history[22];
        history[19] = history[23];

        result[0] += taps[10] * history[20];
        result[1] += taps[10] * history[21];
        result[2] += taps[11] * history[22];
        result[3] += taps[11] * history[23];

        history[20] = history[24];
        history[21] = history[25];
        history[22] = history[26];
        history[23] = history[27];

        result[0] += taps[12] * history[24];
        result[1] += taps[12] * history[25];
        result[2] += taps[13] * history[26];
        result[3] += taps[13] * history[27];

        history[24] = input[0];
        history[25] = input[1];
        history[26] = input[2];
        history[27] = input[3];

        result[0] += taps[14] * input[0];
        result[1] += taps[14] * input[1];
        result[2] += taps[15] * input[2];
        result[3] += taps[15] * input[3];

        output[0] = result[0] + result[2];
        output[1] = result[1] + result[3];

        input += 4;
        output += 2;
    }
}

#endif

fft_cf32::fft_cf32(std::vector<float> window,
                   size_t stride,
                   bool forward,
                   bool shift_output)
    : history_base(stride, window.size()),
      window(window),
      forward(forward),
      shift_output(shift_output)
{
    assert(window.size() >= 1 && stride >= 1);
    assert(window.size() < std::numeric_limits<int>::max());

    fft_in = reinterpret_cast<std::complex<float>*>(fftwf_alloc_complex(window.size()));
    fft_out = reinterpret_cast<std::complex<float>*>(fftwf_alloc_complex(window.size()));
    fft_plan = fftwf_plan_dft_1d(static_cast<int>(window.size()),
                                 reinterpret_cast<fftwf_complex*>(fft_in),
                                 reinterpret_cast<fftwf_complex*>(fft_out),
                                 forward ? FFTW_FORWARD : FFTW_BACKWARD,
                                 FFTW_ESTIMATE);
}

fft_cf32::fft_cf32(const fft_cf32& other)
    : history_base(other.get_history_stride(), other.window.size()),
      window(other.window),
      forward(other.forward),
      shift_output(other.shift_output)
{
    fft_in = reinterpret_cast<std::complex<float>*>(fftwf_alloc_complex(window.size()));
    fft_out = reinterpret_cast<std::complex<float>*>(fftwf_alloc_complex(window.size()));
    fft_plan = fftwf_plan_dft_1d(static_cast<int>(window.size()),
                                 reinterpret_cast<fftwf_complex*>(fft_in),
                                 reinterpret_cast<fftwf_complex*>(fft_out),
                                 forward ? FFTW_FORWARD : FFTW_BACKWARD,
                                 FFTW_ESTIMATE);
}

fft_cf32::~fft_cf32()
{
    fftwf_destroy_plan(fft_plan);
    fftwf_free(reinterpret_cast<fftwf_complex*>(fft_out));
    fftwf_free(reinterpret_cast<fftwf_complex*>(fft_in));
}

void fft_cf32::work(const std::complex<float>* input,
                    std::complex<float>* output,
                    size_t length)
{
    for (size_t n = 0; n < length; n++) {
        const std::complex<float>* input2 = history_feed(input);
        input += history_stride;

        for (size_t i = 0; i < window.size(); i++)
            fft_in[i] = window[i] * input2[i];

        fftwf_execute(fft_plan);

        if (!shift_output) {
            std::memcpy(output, fft_out, window.size() * sizeof(std::complex<float>));
        } else {
            std::memcpy(output + window.size() / 2,
                        fft_out,
                        (window.size() - window.size() / 2) *
                            sizeof(std::complex<float>));
            std::memcpy(output,
                        fft_out + window.size() - window.size() / 2,
                        (window.size() / 2) * sizeof(std::complex<float>));
        }
        output += window.size();
    }
}
