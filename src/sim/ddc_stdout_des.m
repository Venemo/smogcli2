clear
close all

fpass = 70;

% 1600->800
%h1 = firpm(15, [0 2*fpass/1600 800/1600 1], [1 1 0 0 ], [1, 1])
h1 = firpm(15, [0 2*fpass/1600 1000/1600 1], [1 1 0 0 ], [1, 1], [10 1])

%h2 = firpm(15, [0 2*fpass/800 400/800 1], [1 1 0 0])
h2 = firpm(15, [0 2*fpass/800 550/800 1], [1 1 0 0], [10 1])

%h3 = firpm(15, [0 2*fpass/400 200/400 1], [1 1 0 0])
h3 = firpm(39, [0 2*fpass/400 205/400 1], [1 1 0 0], [1 1])

h1s = dsp.FIRDecimator(2, h1);
h2s = dsp.FIRDecimator(2, h2);
h3s = dsp.FIRDecimator(2, h3);

FC = dsp.FilterCascade(h1s, h2s, h3s);
fvtool(FC, h1s, h2s,  h3s, 'Fs', [1600, 1600, 800, 400])
%fvtool(FC, h1s, 'Fs', [1600, 1600])
