#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2019-2020 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from __future__ import print_function
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt


def firdes_gaussian(num_taps, samples_per_bit=4, bandwidth_time=0.5):
    """Creates a gaussian filter based on the GNU Radio codebase."""
    assert isinstance(num_taps, int) and isinstance(samples_per_bit, int)
    # Use 1.0 instead of 0.5 to match GNU Radio, but that is not symmetric
    taps = 0.5 + np.arange(num_taps) - 0.5 * num_taps
    taps /= samples_per_bit * \
        np.sqrt(np.log(2.0)) / (2.0 * np.pi * bandwidth_time)
    taps = np.exp(-0.5 * (taps ** 2.0))
    return taps / np.sum(taps)


def test_firdes_gaussian():
    plt.plot(firdes_gaussian(9))
    plt.show()


def gmsk_encode_bits(bits, samples_per_bit=4, bandwidth_time=0.5):
    """Bits is a numpy array containing 1.0 and -1.0 values. We crop the
    signal after the filter to remove the filter boundary effects."""
    assert all(a == 1 or a == -1 for a in bits)
    taps = firdes_gaussian(4 * samples_per_bit,
                           samples_per_bit, bandwidth_time)
    sig = np.repeat(bits, samples_per_bit)
    sig = np.convolve(sig, taps)
    sig = sig[2 * samples_per_bit - 1:-2 * samples_per_bit]
    sig *= np.pi * 0.5 / samples_per_bit
    sig = np.exp(np.cumsum(sig) * 1j)
    return sig


def test_gmsk_encode_bits():
    sig = gmsk_encode_bits([1, 1])
    print(len(sig))
    plt.plot(np.unwrap(np.angle(sig)))
    plt.show()


def gmsk_encode_bytes(bytes, samples_per_bit=4, bandwidth_time=0.5):
    """Bytes are values between 0 and 255 encoded with MSB first."""
    assert all(isinstance(a, int) and 0 <= a <= 255 for a in bytes)
    bits = [((a >> i) & 1) * 2 - 1 for a in bytes for i in range(7, -1, -1)]
    return gmsk_encode_bits(bits, samples_per_bit, bandwidth_time)


def test_gmsk_encode_bytes():
    plt.plot(np.unwrap(np.angle(gmsk_encode_bytes([0xaa]))))
    plt.show()


smog_sync_bytes = [0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0x2d, 0xd4,
                   0x97, 0xfd, 0xd3, 0x7b, 0x0f, 0x1f, 0x6d, 0x08, 0xf7, 0x83,
                   0x5d, 0x9e, 0x59, 0x82, 0xc0, 0xfd, 0x1d, 0xca, 0xad, 0x3b,
                   0x5b, 0xeb, 0xd4, 0x93, 0xe1, 0x4a, 0x04, 0xd2, 0x28, 0xdd,
                   0xf9, 0x01, 0x53, 0xd2, 0xe6, 0x6c, 0x5b, 0x25, 0x65, 0x31,
                   0xc5, 0x7c, 0xe7, 0xf1, 0x38, 0x61, 0x2d, 0x5c, 0x03, 0x3a,
                   0xc6, 0x88, 0x90, 0xdb, 0x8c, 0x8c, 0x42, 0xf3, 0x51, 0x75,
                   0x43, 0xa0, 0x83, 0x93]


def load_cf32(filename):
    return np.fromfile(filename, dtype=np.complex64)


def rotate_to_dc(sig):
    pos = np.argmax(np.abs(np.fft.fft(sig)))
    rot = np.exp(1j * np.arange(len(sig), dtype=float)
                 * (-2.0 * np.pi * pos / len(sig)))
    return sig * rot


def test_rotate_to_dc():
    sig = load_cf32("/home/mmaroti/Downloads/ATL-1-14620.28347-HA5MRC.cf32")
    sig = sig[277 * 50000:278 * 50000]
    sig = rotate_to_dc(sig)
    fig, (ax0, ax1) = plt.subplots(ncols=2)
    ax0.plot(np.abs(sig))
    ax1.plot(np.abs(np.fft.fft(sig)))
    plt.show()


def test_detect():
    sig = load_cf32("/home/mmaroti/Downloads/ATL-1-14620.28347-HA5MRC.cf32")
    sig = sig[277 * 50000:278 * 50000]
    sig = rotate_to_dc(sig)

    maxes = []

    for i in range(55, len(smog_sync_bytes)):
        taps = np.conj(gmsk_encode_bytes(smog_sync_bytes[54:i], 40))[::-1]
        sig2 = np.abs(np.convolve(sig, taps)) / (i - 54)
        maxes.append(np.amax(sig2))

    plt.plot(maxes)
    plt.show()


def plot_response(fs, w, h, title):
    """Utility function to plot response functions"""
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(0.5 * fs * w / np.pi, 20 * np.log10(np.abs(h)))
    ax.set_ylim(-80, 5)
    ax.set_xlim(0, 0.5 * fs)
    ax.grid(True)
    ax.set_xlabel('Frequency (Hz)')
    ax.set_ylabel('Gain (dB)')
    ax.set_title(title)
    plt.show()


def design_filter(fs, pass_band, stop_band, numtaps):
    taps = signal.remez(numtaps,
                        [0, pass_band, stop_band, 0.5 * fs],
                        [1, 0],
                        Hz=fs,
                        weight=[10, 1])
    w, h = signal.freqz(taps, [1], worN=2000)
    plot_response(fs, w, h, "Low-pass Filter")
    print(repr(taps))
    print("length:", len(taps))


def test_ddc_filters():
    taps1 = [-0.0019545264, -0.0081854569, -0.0160511227, -0.0123682803,
             0.0217284272,  0.0931647992,  0.1810567463,  0.2426820045,
             0.2426820045,  0.1810567463,  0.0931647992,  0.0217284272,
             -0.0123682803, -0.0160511227, -0.0081854569, -0.0019545264]

    taps2 = [-0.0020597041, -0.0086770742, -0.0172482973, -0.0142027203,
             0.0200562700,  0.0929101250,  0.1829995159,  0.2462978400,
             0.2462978400,  0.1829995159,  0.0929101250,  0.0200562700,
             -0.0142027203, -0.0172482973, -0.0086770742, -0.0020597041]

    taps3 = [-0.0011189412, -0.0074104831, -0.0176916959, -0.0186767921,
             0.0127722812,  0.0883478736,  0.1865007221,  0.2570669602,
             0.2570669602,  0.1865007221,  0.0883478736,  0.0127722812,
             -0.0186767921, -0.0176916959, -0.0074104831, -0.0011189412]

    taps4 = [0.0039787512,  0.0019021335,  -0.0141966812, -0.0339871236,
             -0.0190288599, 0.0644374411,  0.1964761267,  0.2992741710,
             0.2992741710,  0.1964761267,  0.0644374411,  -0.0190288599,
             -0.0339871236, -0.0141966812, 0.0019021335,  0.0039787512]

    taps5 = [-0.0001203062, -0.0005253738, 0.0001099460,  0.0009576038,  0.0004122619,
             -0.0015139276, -0.0015817840, 0.0016402157,  0.0034667471,  -0.0006876029,
             -0.0056974175, -0.0020102602, 0.0073645641,  0.0068080093,  -0.0070464327,
             -0.0133897122, 0.0030296645,  0.0204839900,  0.0063639341,  -0.0257202955,
             -0.0225894872, 0.0254479817,  0.0474615898,  -0.0134567484, -0.0871726332,
             -0.0290053231, 0.1891364666,  0.3976305832,  0.3976305832,  0.1891364666,
             -0.0290053231, -0.0871726332, -0.0134567484, 0.0474615898,  0.0254479817,
             -0.0225894872, -0.0257202955, 0.0063639341,  0.0204839900,  0.0030296645,
             -0.0133897122, -0.0070464327, 0.0068080093,  0.0073645641,  -0.0020102602,
             -0.0056974175, -0.0006876029, 0.0034667471,  0.0016402157,  -0.0015817840,
             -0.0015139276, 0.0004122619,  0.0009576038,  0.0001099460,  -0.0005253738,
             -0.0001203062]

    w, h = signal.freqz(taps5, [1], worN=2000)
    plot_response(1600000 / 16, w, h, "Filter 5")


if __name__ == '__main__':
    # test_detect()
    # design_filter(62500, 0.25 * 1250, 0.75 * 1250, 319)  # spb 50
    # design_filter(62500, 0.25 * 5000, 0.75 * 5000, 79)  # spb 25
    # design_filter(62500, 0.25 * 12500, 0.75 * 12500, 33)  # spb 5
    # design_filter(50000, 0.25 * 1250, 0.75 * 1250, 255)  # spb 40
    # design_filter(50000, 0.25 * 2500, 0.75 * 2500, 127)  # spb 20
    # design_filter(50000, 0.25 * 5000, 0.75 * 5000, 65)  # spb 10
    # design_filter(50000, 0.25 * 12500, 0.75 * 12500, 27)  # spb 4
    test_ddc_filters()
