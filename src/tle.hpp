/*
 * Copyright 2020 Peter Horvath.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef TLE_HPP
#define TLE_HPP

#include "predict.h"
#include <json/json.h>
#include <cstdint>
#include <ctime>
#include <string>
#include <vector>

class sat_tracker
{
public:
    struct sat_t {
        sat_t(long catnum, uint32_t downlink) : catnum(catnum), downlink(downlink) {}
        sat_t(const sat_t& sat) = default;

        long catnum;
        uint32_t downlink;
    };

    sat_tracker(const std::vector<sat_t>& sats,
                bool update_tle,
                const std::string& tle_file_name);

    sat_tracker(const Json::Value& meta_root);

    bool is_trackable() const { return tracking_possible; }
    bool is_trackable(size_t idx) const
    {
        return tracking_possible && sats[idx].predict_idx >= 0;
    }

    void calculate(bool print = false);
    void calculate(double daynum);

    const char* get_name(size_t idx) const { return is_trackable(idx) ? sats[idx].name.c_str() : nullptr; }
    double get_daynum(size_t idx) const { return sats[idx].daynum; }
    float get_azimuth(size_t idx) const { return sats[idx].azimuth; }
    float get_elevation(size_t idx) const { return sats[idx].elevation; }
    float get_doppler(size_t idx) const { return sats[idx].doppler; }
    float get_range(size_t idx) const { return sats[idx].range; }

    std::string get_filename(size_t idx, const char* device) const;
    std::string tle_file_name;

    void add_json_data(size_t idx, Json::Value& root);
    void print_packet(double daynum_t1, double delta_t12, std::stringstream& msg);

    static double current_daynum();

protected:
    static const double tle_refresh_days;

    struct sat2_t {
        long catnum;
        uint32_t downlink;
        std::string name;
        std::string tle_line1;
        std::string tle_line2;
        int predict_idx; // negative if not trackable

        double daynum;
        float azimuth;
        float elevation;
        float doppler;
        float range;

        float pl0;
        float path_loss;
    };

    std::vector<sat2_t> sats;
    bool tracking_possible;

    struct memory_struct {
        char* memory;
        size_t size;
    };

    static int download_tle(const std::string& tle_file_name);
    static size_t
    curl_write_memory_callback(void* contents, size_t size, size_t nmemb, void* userp);

    void refresh_files(bool force_read);
    int read_files();
    double last_refresh_files;
    std::time_t tle_file_time;
    bool update_tle;

    station_data station;
};

#endif // TLE_HPP
