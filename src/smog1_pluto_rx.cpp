/*
 * Copyright 2019-2021 Peter Horvath, Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "ad936x_device.hpp"
#include "blocks.hpp"
#include "filter.hpp"
#include "predict.h"
#include "satellite.hpp"
#include "tle.hpp"
#include <unistd.h>
#include <atomic>
#include <cctype>
#include <chrono>
#include <csignal>
#include <fstream>
#include <iomanip>
#include <iostream>

#ifdef _WIN32
#include "getopt/getopt.h"
#endif

static std::atomic<bool> do_exit(false);

static const uint32_t TUNING_OFFSET = 55000;
static const uint32_t DEFAULT_SAMPLE_RATE = 1600000;
static const size_t DEFAULT_WORK_LENGTH = 2048;

void usage(void)
{
    std::cerr << "SMOG-P/ATL-1/SMOG-1 recorder for the PlutoSDR\n\n"
                 "Usage: smog_pluto_rx [-options]\n"
                 "\t-u URI context. Find out the uri by typing `iio_info -s`\n"
                 "\t-f forced continuous recording, no Doppler correction\n"
                 "\t-i track the given primary satellite ID (default: 99999)\n"
                 "\t-F downlink frequency for primary sat (default: 437345000.0 Hz)\n"
                 "\t-k download TLE data from celestrak.com\n"
                 "\t-O dump downconverted samples to STDOUT in binary cf32 format\n"
                 "\t-n download tle-new.txt instead of cubesat.txt from celestrak.com\n"
                 "\t-a download active.txt instead of cubesat.txt from celestrak.com\n"
                 "\t-s disable printing sat and signal statistics (default: false)\n"
                 "\t-e elevation limit for start recording (default: -2 deg)\n"
                 "\t-g disable AGC, set manual gain to <value>\n"
                 "\t-A switch to fast-attack AGC (default: slow-attack)"
                 "\t-h prints this help message\n";
}

static void sighandler(int) { do_exit = true; }

void write_json(const Json::Value& root, const std::string& basename)
{
    std::ofstream metafile;
    metafile.open(basename + ".meta", std::ofstream::trunc);
    metafile << root;
    metafile.close();
}

int main(int argc, char* argv[])
{
    bool update_tle = false;
    long sat1_id = 0;
    double sat1_downlink = 437345000;
    bool dump_samples = false;
    std::string iio_uri;
    const size_t file_buffer_size = 4 * 1024 * 1024;
    float elevation_limit = -2.0f;
    bool print_statistics = true;
    uint32_t sample_rate = DEFAULT_SAMPLE_RATE;
    std::string tle_file_name("cubesat.txt");
    std::ofstream azel_file_1;
    std::chrono::steady_clock::time_point start1;
    bool forced_mode = false;
    ad936x_device::rx_gain_mode agc_mode = ad936x_device::AGC_SLOW;
    bool agc_mode_given = false;
    float hw_gain = -1;

    int opt;
    while ((opt = getopt(argc, argv, "u:fki:F:e:Ohbasng:A")) != -1) {
        switch (opt) {
        case 'u':
            iio_uri = optarg;
            break;
        case 'k':
            update_tle = true;
            break;
        case 'f':
            forced_mode = true;
            break;
        case 'i':
            sat1_id = std::atol(optarg);
            break;
        case 'F':
            sat1_downlink = std::atof(optarg);
            break;
        case 'e':
            elevation_limit = std::atof(optarg);
            break;
        case 'O':
            dump_samples = true;
            break;
        case 'a':
            tle_file_name = "active.txt";
            break;
        case 'n':
            tle_file_name = "tle-new.txt";
            break;
        case 's':
            print_statistics = false;
            break;
        case 'g':
            agc_mode = ad936x_device::MANUAL;
            hw_gain = std::atof(optarg);
            break;
        case 'A':
            agc_mode = ad936x_device::AGC_FAST;
            agc_mode_given = true;
            break;
        case 'h':
        default:
            usage();
            return 1;
        }
    }

    if(hw_gain >= 0 && agc_mode_given)  {
        std::cerr << "ERROR: Options -A and -g are mutually exclusive, exiting..." << std::endl;
        return 1;
    }

    const uint32_t output_sample_rate = sample_rate / 32;

    uint32_t dongle_center_freq = std::round(sat1_downlink - TUNING_OFFSET);

    double real_center_freq = dongle_center_freq;

    sat_tracker tracker(
        { sat_tracker::sat_t(sat1_id, sat1_downlink) }, update_tle, tle_file_name);

    if (!tracker.is_trackable(0) && !forced_mode) {
        if (sat1_id == 0) {
            std::cerr << "ERROR: no satellite ID given." << std::endl;
        } else {
            std::cerr
                << "ERROR: no QTH file could be found and/or no TLE file could be found."
                << std::endl;
        }
        std::cerr << "ERROR: specify -f to force continuous recording with Doppler "
                     "correction disabled."
                  << std::endl;
        std::cerr << "ERROR: Exiting." << std::endl;
        return 1;
    }

    if (forced_mode) {
        std::cerr << "WARNING: Doppler correction disabled, using fixed RX frequency!"
                  << std::endl;
    }

    buffer_t buffer(2, 32 * DEFAULT_WORK_LENGTH * sizeof(float));
    ad936x_device pluto(
        buffer, iio_uri, 8 * DEFAULT_WORK_LENGTH * sizeof(int16_t), real_center_freq, agc_mode, hw_gain);

    Json::Value options;
    options["samp_rate"] = sample_rate;
    options["elevation_limit"] = elevation_limit;

    sat_recorder recorder(
        buffer, 0, output_sample_rate, DEFAULT_WORK_LENGTH, file_buffer_size);
    sat_recorder dumper(
        buffer, 1, output_sample_rate, DEFAULT_WORK_LENGTH, file_buffer_size);

    std::signal(SIGINT, sighandler);
    std::signal(SIGTERM, sighandler);
#ifndef _WIN32
    std::signal(SIGQUIT, sighandler);
    std::signal(SIGPIPE, sighandler);
#endif

    std::cerr << "INFO: allocating PlutoSDR resources..." << std::endl;
    bool pluto_init_res = pluto.init();
    if (!pluto_init_res) {
        std::cerr << "ERROR: PlutoSDR initialzation failed, exiting." << std::endl;
        return 1;
    }

    pluto.start();

    std::cerr << "INFO: real center frequency is " << std::setprecision(1) << std::fixed
              << real_center_freq << " Hz" << std::endl;

    if (dump_samples)
        dumper.start("-");

    if (!print_statistics)
        std::cerr << "INFO: printing of satellite and signal statistic are disabled"
                  << std::endl;

    unsigned int delay_count = 0;

    recorder.set_correction((real_center_freq - sat1_downlink) / sample_rate);

    while (!do_exit) {
        if (++delay_count >= 100)
            delay_count = 0;

        if (!forced_mode)
            tracker.calculate(print_statistics && (delay_count == 0));

        // this will be overwritten if the satellite is above
        float dumper_correction = (real_center_freq - sat1_downlink) / sample_rate;

        if (tracker.is_trackable(0) && sat1_downlink > 0 && !forced_mode) {
            float correction =
                (real_center_freq - sat1_downlink - tracker.get_doppler(0)) / sample_rate;
            recorder.set_correction(correction);
            if (tracker.get_elevation(0) >= elevation_limit)
                dumper_correction = correction;
        }

        if (!recorder.is_running() &&
            (tracker.get_elevation(0) >= elevation_limit || forced_mode)) {
            std::string basename;
            if (!forced_mode)
                // basename = tracker.get_filename(0, device);  // FIXME
                basename = tracker.get_filename(0, 0);
            else {
                if (tracker.is_trackable(0))
                    basename = tracker.get_name(0);
                else
                    basename = "SCN" + std::to_string(sat1_id);

                basename += "_";
                basename += std::to_string(sat_tracker::current_daynum());
            }

            recorder.start(basename.c_str());

            if (!forced_mode) {
                Json::Value root;
                root["options"] = options;
                tracker.add_json_data(0, root);
                write_json(root, basename);
            }
        } else if (recorder.is_running()) {
            if (!forced_mode && tracker.get_elevation(0) < elevation_limit) {
                recorder.stop();
            }
        }

        dumper.set_correction(dumper_correction);

        if (delay_count == 0) {
            if (print_statistics) {
                std::stringstream msg;
                msg << "RSSI = " << pluto.get_rssi_str()
                    << ", hwgain = " << pluto.get_hwgain_str() << std::endl;
                std::cerr << msg.str();
            }
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    if (do_exit)
        std::cerr << "INFO: aborted by signal" << std::endl;

    if (recorder.is_running())
        recorder.stop();

    if (dumper.is_running())
        dumper.stop();

    pluto.stop();

    std::cerr << "INFO: Exiting..." << std::endl;

    return 0;
}
