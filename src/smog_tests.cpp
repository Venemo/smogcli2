/*
 * Copyright 2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "blocks.hpp"
#include "buffer.hpp"
#include "coherent.hpp"
#include "filter.hpp"
#include "satellite.hpp"
#include <chrono>
#include <cmath>
#include <iostream>
#include <random>
#include <thread>

#ifndef M_PI
#define M_PI (3.14159265358979323846)
#endif

void buffer_worker1(const std::vector<unsigned int>& input, buffer_t& buffer)
{
    std::mt19937 rng(1);
    std::uniform_int_distribution<size_t> dist(0, 20);

    size_t pos = 0;
    while (pos < input.size()) {
        size_t num = dist(rng);
        num = std::min(input.size() - pos, num);

        buffer.write(input.data() + pos, num * sizeof(int));
        pos += num;

        if (dist(rng) <= 1)
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

void buffer_worker2(std::vector<unsigned int>& output, buffer_t& buffer, size_t reader)
{
    std::mt19937 rng(2);
    std::uniform_int_distribution<size_t> dist(0, 20);

    size_t pos = 0;
    while (pos < output.size()) {
        size_t num = dist(rng);
        num = std::min(output.size() - pos, num);

        buffer.read(reader, output.data() + pos, num * sizeof(int));
        pos += num;

        if (dist(rng) <= 1)
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

void test_buffer()
{
    std::vector<unsigned int> input(2000, 0);
    for (size_t i = 0; i < input.size(); i++)
        input[i] = i;

    std::vector<unsigned int> output(input.size(), 1);

    buffer_t buffer(3, 50);
    buffer.enable(2, false);

    std::thread thread1(buffer_worker1, input, std::ref(buffer));
    std::thread thread2(buffer_worker2, std::ref(output), std::ref(buffer), 0);
    std::thread thread3(buffer_worker2, std::ref(output), std::ref(buffer), 1);
    thread1.join();
    thread2.join();
    thread3.join();

    std::cout << "buffer_t: test " << (input == output ? "passed" : "FAILED")
              << std::endl;
}

void test_convert()
{
    {
        convert_u8_f32 convert;
        std::vector<uint8_t> input(53, 0);
        for (size_t i = 0; i < input.size(); i++)
            input[i] = 100 + i;
        std::vector<float> output(input.size(), 0.0f);
        convert.work(input, output);

        bool passed = true;
        for (size_t i = 0; i < input.size(); i++) {
            passed &= output[i] == (input[i] - 128) * (1.0 / 128);
        }

        std::cout << "convert_u8_f32: test " << (passed ? "passed" : "FAILED")
                  << std::endl;
    }
    {
        convert_u8_f32 convert;
        size_t length = 10000000;
        std::vector<uint8_t> input(length, 111);
        std::vector<float> output(length, 0.0f);
        auto start = std::chrono::high_resolution_clock::now();
        convert.work(input, output);
        float elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(
                            std::chrono::high_resolution_clock::now() - start)
                            .count();

        std::cout << "convert_u8_f32: scaled " << (length / elapsed * 1e-6f) << " msps"
                  << std::endl;
    }
    {
        convert_u8_f32 convert(1.0f);
        size_t length = 10000000;
        std::vector<uint8_t> input(length, 111);
        std::vector<float> output(length, 0.0f);
        auto start = std::chrono::high_resolution_clock::now();
        convert.work(input, output);
        float elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(
                            std::chrono::high_resolution_clock::now() - start)
                            .count();

        std::cout << "convert_u8_f32: unscaled " << (length / elapsed * 1e-6f) << " msps"
                  << std::endl;
    }
}

void test_sum_max()
{
    {
        std::random_device rd{};
        std::mt19937 gen{ rd() };
        std::uniform_int_distribution<int> dist(0, 255);

        sum_max_u8 sum_max;
        std::vector<uint8_t> input(32, 0);
        for (size_t i = 0; i < input.size(); i++)
            input[i] = static_cast<uint8_t>(dist(gen));
        sum_max.work(input);

        unsigned int sum = 0;
        unsigned int max = 0;
        for (size_t i = 0; i < input.size(); i++) {
            unsigned int a = static_cast<unsigned int>(std::abs(input[i] - 128));
            sum += a;
            max = std::max(max, a);
        }
        bool passed = (max == sum_max.max) && (sum == sum_max.sum);

        std::cout << "sum_max_u8: test " << (passed ? "passed" : "FAILED") << std::endl;
    }
    {
        sum_max_u8 sum_max;
        size_t length = 10000000;
        std::vector<uint8_t> input(length, 111);
        auto start = std::chrono::high_resolution_clock::now();
        sum_max.work(input);
        float elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(
                            std::chrono::high_resolution_clock::now() - start)
                            .count();

        std::cout << "sum_max_u8: " << (length / elapsed * 1e-6f) << " msps" << std::endl;
    }
}

void test_filter_fir_f32()
{
    bool passed = true;

    {
        filter_fir_f32 filter({ 1.0f, 2.0f, 3.0f }, 1, 1);
        std::vector<float> input({ 1.0f, -1.0f, 1.0f });
        std::vector<float> output(3);
        filter.work(input, output);
        std::vector<float> result({ 3.0f, -1.0f, 2.0f });
        passed &= output == result;
    }

    {
        filter_fir_f32 filter({ 1.0f, 10.0f, 100.0f }, 1, 2);
        std::vector<float> input({ 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f });
        std::vector<float> output(6);
        filter.work(input, output);
        std::vector<float> result({ 100.0f, 200.0f, 310.0f, 420.0f, 531.0f, 642.0f });
        passed &= output == result;
    }

    {
        filter_fir_f32 filter({ 1.0f, 10.0f, 100.0f, 1000.0f }, 2, 1);
        std::vector<float> input({ 1.0f, 2.0f, 3.0f, 4.0f });
        std::vector<float> output(2);
        filter.work(input, output);
        std::vector<float> result({ 2100.0f, 4321.0f });
        passed &= output == result;
    }

    {
        filter_fir_f32 filter({ 1.0f }, 2, 1);
        std::vector<float> input({ 1.0f, 2.0f, 3.0f, 4.0f });
        std::vector<float> output(2);
        filter.work(input, output);
        std::vector<float> result({ 2.0f, 4.0f });
        passed &= output == result;
    }

    std::cout << "filter_fir_f32: test " << (passed ? "passed" : "FAILED") << std::endl;

    {
        size_t length = 1000000;
        filter_fir_f32 filter(std::vector<float>(16, 1.0f), 2, 2);
        std::vector<float> input(4 * length, 1.0f);
        std::vector<float> output(2 * length, 0.0f);
        auto start = std::chrono::high_resolution_clock::now();
        filter.work(input, output);
        float elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(
                            std::chrono::high_resolution_clock::now() - start)
                            .count();
        std::cout << "filter_fir_f32: taps16 dec2 vec2 speed "
                  << (2 * length / elapsed * 1e-6f) << " msps" << std::endl;
    }
}

void test_filter_fir_vec2_f32()
{
    {
        bool passed = true;

        filter_fir_f32 filter1({ 1.0f, 10.0f, 100.0f, 20.0f, 4.0f }, 3, 2);
        filter_fir_vec2_f32 filter2(filter1.get_taps(), 3);

        std::vector<float> input(60, 0.0f);
        for (size_t i = 0; i < 60; i++)
            input[i] = i;
        std::vector<float> output1(20);
        std::vector<float> output2(20);

        for (int i = 0; i < 10; i++) {
            filter1.work(input, output1);
            filter2.work(input, output2);
            passed &= output1 == output2;
        }

        std::cout << "filter_fir_vec2_f32: test " << (passed ? "passed" : "FAILED")
                  << std::endl;
    }

    {
        size_t length = 1000000;
        filter_fir_vec2_f32 filter(std::vector<float>(16, 1.0f), 2);
        std::vector<float> input(4 * length, 1.0f);
        std::vector<float> output(2 * length, 0.0f);
        auto start = std::chrono::high_resolution_clock::now();
        filter.work(input, output);
        float elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(
                            std::chrono::high_resolution_clock::now() - start)
                            .count();

#if defined(__ARM_NEON__) && !defined(__NO_SIMD__)
        char opt[] = "neon";
#else
        char opt[] = "gen";
#endif
        std::cout << "filter_fir_vec2_f32: taps16 dec2 " << opt << " "
                  << (2 * length / elapsed * 1e-6f) << " msps" << std::endl;
    }
}

void test_filter_fir_interp_vec2_f32()
{
    {
        filter_fir_interp_vec2_f32 filter({ 1.0f, 10.0f, 100.0f, 1000.0f }, 2);
        std::vector<float> input = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f };
        std::vector<float> output(12);
        filter.work(input, output);
        std::vector<float> result = {
            100.0f,  200.0f,  1000.0f, 2000.0f, 301.0f,  402.0f,
            3010.0f, 4020.0f, 503.0f,  604.0f,  5030.0f, 6040.0f
        };

        bool passed = output == result;
        std::cout << "filter_fir_interp_vec2_f32: test " << (passed ? "passed" : "FAILED")
                  << std::endl;
    }

    {
        size_t length = 1000000;
        filter_fir_interp_vec2_f32 filter(std::vector<float>(16, 1.0f), 2);
        std::vector<float> input(2 * length, 1.0f);
        std::vector<float> output(4 * length, 0.0f);
        auto start = std::chrono::high_resolution_clock::now();
        filter.work(input, output);
        float elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(
                            std::chrono::high_resolution_clock::now() - start)
                            .count();

        std::cout << "filter_fir_interp_vec2_f32: taps16 interp2 "
                  << (2 * length / elapsed * 1e-6f) << " msps" << std::endl;
    }
}

void test_filter_fir_tap16_dec2_vec2_f32()
{
    bool passed = true;

    {
        filter_fir_f32 filter1({ 1.0f, 10.0f, 100.0f, 1000.0f, 10000.0f }, 2, 2);
        filter_fir_tap16_dec2_vec2_f32 filter2(filter1.get_taps());

        std::vector<float> input({ 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f });
        std::vector<float> output1(4);
        std::vector<float> output2(4);

        for (int i = 0; i < 10; i++) {
            filter1.work(input, output1);
            filter2.work(input, output2);
            passed &= output1 == output2;
        }
    }

    std::cout << "filter_fir_tap16_dec2_vec2_f32: test " << (passed ? "passed" : "FAILED")
              << std::endl;

    {
        size_t length = 1000000;
        filter_fir_tap16_dec2_vec2_f32 filter(std::vector<float>(16, 1.0f));
        std::vector<float> input(4 * length, 1.0f);
        std::vector<float> output(2 * length, 0.0f);
        auto start = std::chrono::high_resolution_clock::now();
        filter.work(input, output);
        float elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(
                            std::chrono::high_resolution_clock::now() - start)
                            .count();

#if defined(__SSE__) && !defined(__NO_SIMD__)
        char opt[] = "sse";
#elif defined(__ARM_NEON__) && !defined(__NO_SIMD__)
        char opt[] = "neon";
#else
        char opt[] = "gen";
#endif
        std::cout << "filter_fir_tap16_dec2_vec2_f32: " << opt << " "
                  << (2 * length / elapsed * 1e-6f) << " msps" << std::endl;
    }
}

void test_sinusoid_source_cf32()
{
    {
        sinusoid_source_cf32 source(0.25);
        std::vector<std::complex<float>> output(2);
        source.work(output);
        for (size_t i = 0; i < output.size(); i++) {
            output[i].real(std::round(output[i].real() * 1024.0f) / 1024.0f);
            output[i].imag(std::round(output[i].imag() * 1024.0f) / 1024.0f);
        }
        std::vector<std::complex<float>> result({ { 0.0f, 1.0f }, { -1.0f, 0.0f } });
        std::cout << "sinusoid_source_cf32: test "
                  << (output == result ? "passed" : "FAILED") << std::endl;
    }
    {
        size_t length = 1000000;
        sinusoid_source_cf32 source(0.1);
        std::vector<std::complex<float>> output(length, 0.0f);
        auto start = std::chrono::high_resolution_clock::now();
        source.work(output);
        float elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(
                            std::chrono::high_resolution_clock::now() - start)
                            .count();

        std::cout << "sinusoid_source_cf32: " << (length / elapsed * 1e-6f) << " msps"
                  << std::endl;
    }
}

void test_fft_cf32()
{
    bool passed = true;

    {
        fft_cf32 fft({ 1.0f, 1.0f, 1.0f, 1.0f }, 1, true, false);
        std::vector<float> input({ 1.0f, 0.0f });
        std::vector<float> output(8, 0.0f);
        fft.work(input, output);
        passed &= output == std::vector<float>(
                                { 1.0f, 0.0f, 0.0f, 1.0f, -1.0f, 0.0f, 0.0f, -1.0f });
        fft.work(input, output);
        passed &= output == std::vector<float>(
                                { 2.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, -1.0f, -1.0f });
        fft.work(input, output);
        passed &= output == std::vector<float>(
                                { 3.0f, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f });
        fft.work(input, output);
        passed &= output ==
                  std::vector<float>({ 4.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f });
    }

    {
        fft_cf32 fft({ 1.0f, 1.0f, 1.0f, 1.0f }, 3, true, false);
        std::vector<float> input({ 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f });
        std::vector<float> output(8, 0.0f);
        fft.work(input, output);
        passed &= output == std::vector<float>(
                                { 3.0f, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f, -1.0f, 0.0f });
        for (float& f : input)
            f *= -1.0f;
        fft.work(input, output);
        passed &= output ==
                  std::vector<float>({ -2.0f, 0.0f, 2.0f, 0.0f, 2.0f, 0.0f, 2.0f, 0.0f });
    }

    {
        fft_cf32 fft({ 1.0f, 0.0f }, 1, true, false);
        std::vector<float> input({ 1.0f, 0.0f });
        std::vector<float> output(4, 0.0f);
        fft.work(input, output);
        passed &= output == std::vector<float>({ 0.0f, 0.0f, 0.0f, 0.0f });
        input[0] = 2.0f;
        fft.work(input, output);
        passed &= output == std::vector<float>({ 1.0f, 0.0f, 1.0f, 0.0f });
        input[0] = 3.0f;
        fft.work(input, output);
        passed &= output == std::vector<float>({ 2.0f, 0.0f, 2.0f, 0.0f });
    }

    std::cout << "fft_cf32: test " << (passed ? "passed" : "FAILED") << std::endl;

    {
        size_t window = 2048;
        size_t stride = 512;
        size_t length = 1000;
        fft_cf32 fft(window_rectangular(window), stride, true, true);
        std::vector<std::complex<float>> input(stride * length);
        std::vector<std::complex<float>> output(window * length);
        auto start = std::chrono::high_resolution_clock::now();
        fft.work(input, output);
        float elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(
                            std::chrono::high_resolution_clock::now() - start)
                            .count();
        std::cout << "fft_cf32: win" << window << " str" << stride << " speed "
                  << (stride * length / elapsed * 1e-6f) << " msps" << std::endl;
    }
}

void test_norm_cf32()
{
    norm_cf32 norm;
    size_t length = 1000000;
    std::vector<std::complex<float>> input(length);
    std::vector<float> output(length);
    auto start = std::chrono::high_resolution_clock::now();
    norm.work(input, output);
    float elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(
                        std::chrono::high_resolution_clock::now() - start)
                        .count();

    std::cout << "norm_cf32: " << (length / elapsed * 1e-6f) << " msps" << std::endl;
}

void test_viterbi_demod()
{
    std::random_device rd{};
    std::mt19937 gen{ rd() };
    std::normal_distribution<> dist;

    bool passed = true;
    for (unsigned int spb = 4; spb <= 4; spb++) {
        size_t length = spb * 8 * 128 + 1;
        std::vector<std::complex<float>> input(length);
        for (size_t i = 0; i < input.size(); i++) {
            input[i].real(dist(gen));
            input[i].imag(dist(gen));
        }

        viterbi_demod demod1(spb, false);
        viterbi_demod demod2(spb, true);

        const std::vector<float>& output1 =
            demod1.demodulate(input.data(), input.size(), { 0xaa });
        const std::vector<float>& output2 =
            demod2.demodulate(input.data(), input.size(), { 0xaa });

        for (size_t i = 0; i < output1.size(); i++)
            passed &= std::abs(output1[i] - output2[i]) < 0.1;
    }
    std::cout << "viterbi_demod: test " << (passed ? "passed" : "FAILED") << std::endl;

    for (unsigned int spb = 4; spb <= 5; spb++) {
        size_t length = spb * 8 * 1024 + 1;
        std::vector<std::complex<float>> input(length);
        viterbi_demod demod(spb);
        auto start = std::chrono::high_resolution_clock::now();
        demod.demodulate(input.data(), input.size(), { 0xaa });
        float elapsed = std::chrono::duration_cast<std::chrono::duration<float>>(
                            std::chrono::high_resolution_clock::now() - start)
                            .count();

        std::cout << "viterbi_demod: spb" << demod.get_spb() << " "
                  << (length / elapsed * 1e-6f) << " msps" << std::endl;
    }
}

void test_ddc()
{
    const int work_len = 1024;
    const float fs_in = 1.6e6;

    sat_downconv_32 ddc(work_len);

    file_sink source(file_sink::CF32, 1600000);
    source.open("ddc_in1.cf32");

    file_sink sink(file_sink::CF32, 50000);
    sink.open("ddc_test1.cf32");
    ddc.set_correction(0.0f);

    int n = 0;
    for (int buf = 0; buf < 1; ++buf) {
        for (unsigned ii = 0; ii < ddc.input().size(); ++ii, ++n) {
            ddc.input()[ii] =
                std::complex<float>(50.0f * std::cos(2.0f * M_PI * 11e3f / fs_in * n),
                                    50.0f * std::sin(2.0f * M_PI * 11e3f / fs_in * n));
        }
        source.write(ddc.input());
        ddc.work();
        sink.write(ddc.output());
    }

    sink.close();
    source.close();
}

void test_ddc_2()
{
    const int work_len = 1024;
    const float fs_in = 1600e3;

    std::vector<std::complex<float>> buffer2(32 * work_len);
    sinusoid_source_cf32 sinusoid(0.0f);
    std::vector<std::complex<float>> buffer3(32 * work_len);
    multiply_cf32 multiply;
    std::vector<std::complex<float>> buffer4(32 * work_len, 0);

    filter_fir_tap16_dec2_vec2_f32 filter1(sat_downconv_32::filter1_taps);
    std::vector<std::complex<float>> buffer5(16 * work_len, 0);
    filter_fir_tap16_dec2_vec2_f32 filter2(sat_downconv_32::filter2_taps);
    std::vector<std::complex<float>> buffer6(8 * work_len, 0);
    filter_fir_tap16_dec2_vec2_f32 filter3(sat_downconv_32::filter3_taps);
    std::vector<std::complex<float>> buffer7(4 * work_len, 0);
    filter_fir_tap16_dec2_vec2_f32 filter4(sat_downconv_32::filter4_taps);
    std::vector<std::complex<float>> buffer8(2 * work_len, 0);
    filter_fir_vec2_f32 filter5(sat_downconv_32::filter5_taps, 2);
    std::vector<std::complex<float>> buffer9(work_len, 0);

    const std::vector<std::complex<float>>& output = buffer9;

    sinusoid.set_frequency(0);

    file_sink source(file_sink::CF32, fs_in);
    source.open("ddc_in.cf32");
    file_sink sink(file_sink::CF32, 50000);
    sink.open("ddc_out.cf32");

    std::random_device rd{};
    std::mt19937 gen{ rd() };
    std::normal_distribution<float> d{ 0.0f, 20.0f };

    for (int b = 0; b < 1024; ++b) {
        for (unsigned ii = 0; ii < 32 * work_len; ++ii) {
            buffer2[ii] = std::complex<float>(d(gen), d(gen));
        }

        sinusoid.work(buffer3);
        multiply.work(buffer2, buffer3, buffer4);
        filter1.work(buffer4, buffer5);
        filter2.work(buffer5, buffer6);
        filter3.work(buffer6, buffer7);
        filter4.work(buffer7, buffer8);
        filter5.work(buffer8, buffer9);

        source.write(buffer2);
        sink.write(output);
    }

    sink.close();
    source.close();
}

void test_ddc_3()
{
    const int work_len = 2048;
    const float fs_in = 1600e3;

    std::vector<std::complex<float>> buffer2(32 * work_len);
    sinusoid_source_cf32 sinusoid(0.0f);
    std::vector<std::complex<float>> buffer3(32 * work_len);
    multiply_cf32 multiply;
    std::vector<std::complex<float>> buffer4(32 * work_len, 0);

    filter_fir_tap16_dec2_vec2_f32 filter1(sat_downconv_32::filter1_taps);
    std::vector<std::complex<float>> buffer5(16 * work_len, 0);
    filter_fir_tap16_dec2_vec2_f32 filter2(sat_downconv_32::filter2_taps);
    std::vector<std::complex<float>> buffer6(8 * work_len, 0);
    filter_fir_tap16_dec2_vec2_f32 filter3(sat_downconv_32::filter3_taps);
    std::vector<std::complex<float>> buffer7(4 * work_len, 0);
    filter_fir_tap16_dec2_vec2_f32 filter4(sat_downconv_32::filter4_taps);
    std::vector<std::complex<float>> buffer8(2 * work_len, 0);
    filter_fir_vec2_f32 filter5(sat_downconv_32::filter5_taps, 2);
    std::vector<std::complex<float>> buffer9(work_len, 0);

    const std::vector<std::complex<float>>& output = buffer9;

    sinusoid.set_frequency(0);

    file_sink source(file_sink::CF32, fs_in);
    source.open("ddc_in.cf32");
    file_sink sink(file_sink::CF32, 50000);
    sink.open("ddc_out.cf32");

    int m = -16 * work_len + 1;
    const float u = 0.25;
    for (unsigned ii = 0; ii < 32 * work_len; ++ii, ++m) {
        buffer2[ii] = std::complex<float>(60.0f * std::cos(M_PI * u * m * m / 32768.0f),
                                          60.0f * std::sin(M_PI * u * m * m / 32768.0f));
    }

    sinusoid.work(buffer3);
    multiply.work(buffer2, buffer3, buffer4);
    filter1.work(buffer4, buffer5);
    filter2.work(buffer5, buffer6);
    filter3.work(buffer6, buffer7);
    filter4.work(buffer7, buffer8);
    filter5.work(buffer8, buffer9);

    source.write(buffer2);
    sink.write(output);

    sink.close();
    source.close();
}

int main()
{
    test_buffer();
    test_convert();
    test_sum_max();
    test_filter_fir_f32();
    test_filter_fir_vec2_f32();
    test_filter_fir_interp_vec2_f32();
    test_filter_fir_tap16_dec2_vec2_f32();
    test_sinusoid_source_cf32();
    test_fft_cf32();
    test_norm_cf32();
    test_viterbi_demod();
    // test_ddc_3();
    return 0;
}
