/*
 * Copyright 2019-2020 Peter Horvath.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef AD936X_DEVICE_HPP
#define AD936X_DEVICE_HPP

#include <iio.h>
#include <string>
#include <thread>

#include "buffer.hpp"

struct stream_cfg {
    long long bw_hz;    // Analog banwidth in Hz
    long long fs_hz;    // Baseband sample rate in Hz
    long long lo_hz;    // Local oscillator frequency in Hz
    const char* rfport; // Port name
    int hw_gain;        // hardware gain
};

class ad936x_device
{
public:
    enum rx_gain_mode {AGC_SLOW, AGC_FAST, MANUAL};

    ad936x_device(buffer_t& buffer,
                  const std::string& context_uri,
                  size_t rx_buffer_size,
                  float lo_freq = 2.5e9,
                  rx_gain_mode agc_mode = AGC_SLOW,
                  int hw_gain = -1);
    virtual ~ad936x_device();
    bool init();
    bool destroy();

    void start();
    void stop();

    std::string get_rssi_str();
    std::string get_hwgain_str();

private:
    enum iodev { RX, TX };
    float lo_freq;
    rx_gain_mode agc_mode;
    int hw_gain;

    iio_context* ctx;
    std::string uri;
    iio_device* rx;
    iio_device* tx;

    iio_device* phy;
    iio_channel* rx0_i;
    iio_channel* rx0_q;
    iio_channel* tx0_i;
    iio_channel* tx0_q;
    iio_buffer* rxbuf;

    stream_cfg rx_cfg, tx_cfg;

    static char tmpstr[64];
    size_t rx_buffer_size;
    std::atomic<bool> do_exit;
    std::thread input_thread;

    buffer_t& buffer;
    convert_i16_f32 convert;
    static const int convert_buffer_size = 65536;
    std::vector<float> temp;
    bool init_done;

    std::mutex mutex;
    static const int rssi_buf_len = 12;
    char rssi_buf[rssi_buf_len];
    static const int hwgain_buf_len = 15;
    char hwgain_buf[hwgain_buf_len];

    void worker();
    bool get_ad9361_stream_dev(iio_context* ctx, iodev d, iio_device** dev);
    bool cfg_ad9361_streaming_ch(stream_cfg* cfg, iodev type, int chid);
    bool get_phy_chan(iodev d, int chid, iio_channel** chn);
    iio_device* get_ad9361_phy(iio_context* ctx);
    char* get_ch_name(const char* type, int id);
    bool get_ad9361_stream_ch(__notused iio_context* ctx,
                              iodev d,
                              iio_device* dev,
                              int chid,
                              struct iio_channel** chn);
    static void errchk(int v, const char* what);
    static void wr_ch_lli(iio_channel* chn, const char* what, long long val);
    static void wr_ch_ld(iio_channel* chn, const char* what, double val);
    static void wr_ch_str(iio_channel* chn, const char* what, const char* str);
    bool get_lo_chan(iodev d, iio_channel** chn);
    void err_str(int ret);
    void print_libiio_info();
    iio_context* autodetect_context(bool rtn);

    std::ofstream dbgfile;
    static unsigned char smogcli_16_ftr[];
    static unsigned int smogcli_16_ftr_len;

    static unsigned char smogcli_08_ftr[];
    static unsigned int smogcli_08_ftr_len;
};

#endif
