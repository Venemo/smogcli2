/*
 * Copyright 2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef COHERENT_HPP
#define COHERENT_HPP

#include "blocks.hpp"
#include "filter.hpp"
#include <fftw3.h>
#include <array>
#include <complex>
#include <deque>
#include <sstream>
#include <vector>

// Returns bits.size() * spb + 1 many samples
std::vector<std::complex<float>>
gmsk_encode_bits(const std::vector<bool>& bits, unsigned int spb, float bt = 0.5f);

// Returns bytes.size() * 8 * spb + 1 many samples
std::vector<std::complex<float>>
gmsk_encode_bytes(const std::vector<uint8_t>& bytes, unsigned int spb, float bt = 0.5f);

// Takes size * spb + 1 many samples, and returns size many soft bits
std::vector<float> gmsk_soft_decode2(const std::vector<std::complex<float>>& samples,
                                     unsigned int spb);

std::vector<uint8_t> soft_to_hard(const std::vector<float>& bits);

// Takes size * 8 * spb + 1 many samples, and returns size many bytes
std::vector<uint8_t> gmsk_hard_decode2(const std::vector<std::complex<float>>& samples,
                                       unsigned int spb);

class signal_downsampler
{
public:
    signal_downsampler(size_t input_spb, size_t output_spb);

    static bool is_supported(size_t input_spb, size_t output_spb);

    size_t get_decim() const { return filter_block.get_decim(); }
    const std::vector<float>& get_taps() const { return filter_block.get_taps(); }
    size_t get_delay() const
    {
        // dark magic, think about it
        return get_taps().size() / 2 - (get_decim() - 1);
    }

    void set_frequency(float freq) { sinusoid_block.set_frequency(freq); }
    void clear_history()
    {
        sinusoid_block.reset();
        filter_block.clear_history();
    }

    void
    work(const std::complex<float>* input, std::complex<float>* output, size_t length);

    void work(const std::vector<std::complex<float>>& input,
              std::vector<std::complex<float>>& output)
    {
        assert(input.size() == output.size() * get_decim());
        work(input.data(), output.data(), output.size());
    }

protected:
    static const std::vector<float> taps_spb50;
    static const std::vector<float> taps_spb40;
    static const std::vector<float> taps_spb25;
    static const std::vector<float> taps_spb20;
    static const std::vector<float> taps_spb10;
    static const std::vector<float> taps_spb5;
    static const std::vector<float> taps_spb4;
    static const std::vector<float>& gmsk_filter_taps(size_t spb);

    const size_t input_spb;
    sinusoid_source_cf32 sinusoid_block;
    multiply_cf32 multiply_block;
    filter_fir_vec2_f32 filter_block;

    std::vector<std::complex<float>> sinusoid_data;
    std::vector<std::complex<float>> multiply_data;
};

class preamble_detector
{
public:
    preamble_detector(const std::vector<float>& window,
                      size_t stride,
                      size_t input_spb,
                      size_t output_spb,
                      size_t max_freq_bin,
                      const std::vector<uint8_t>& sync_bytes,
                      float tone_snr_db,
                      float sync_snr_db,
                      size_t output_bytes);

    const std::vector<float>& get_window() const { return fft_block.get_window(); }
    size_t get_window_size() const { return get_window().size(); }
    size_t get_stride() const { return fft_block.get_stride(); }
    size_t get_sync_size() const { return sync_bytes.size(); }
    const std::vector<uint8_t>& get_sync_bytes() const { return sync_bytes; }
    size_t get_input_spb() const { return input_spb; }
    size_t get_output_spb() const { return output_spb; }

    size_t get_tone_detected() const { return tone_detected; }
    size_t get_sync_detected() const { return sync_detected; }

    struct packet_t {
        uint64_t position;
        float freq_offset;
        float tone_snr_db;
        float sync_snr_db;
        std::vector<std::complex<float>> samples;
    };

    // input: stride
    const std::vector<packet_t>& work(const std::complex<float>* input);

    const std::vector<packet_t>& work(const std::vector<std::complex<float>>& input)
    {
        assert(input.size() == get_stride());
        return work(input.data());
    }

    void
    print_packet(float samp_rate, const packet_t& packet, std::stringstream& msg) const;
    void print_samples(const packet_t& packet, std::stringstream& msg) const;
    void print_packet(const packet_t& pkt) const;

protected:
    void find_tones(const std::complex<float>* history_output);
    void find_syncs(const std::complex<float>* history_output,
                    float tone_freq,
                    float tone_snr_db);

    bool equiv_det(const packet_t& pkt1, const packet_t& pkt2) const;
    bool better_det(const packet_t& pkt1, const packet_t& pkt2) const;
    void merge_detections();

    const size_t tone_halfband;
    const size_t input_spb;
    const size_t output_spb;
    const size_t max_freq_bin;
    std::vector<uint8_t> sync_bytes;
    const float tone_snr_scale;
    const float sync_snr_scale;
    const size_t pos_accuracy;
    const float freq_accuracy;
    const size_t output_bytes;
    const size_t history_size1;
    const size_t history_size2;

    uint64_t position;
    history_base<std::complex<float>> history_block;

    fft_cf32 fft_block;
    std::vector<std::complex<float>> freq_data;
    norm_cf32 norm_block;
    std::vector<float> norm_data;

    signal_downsampler downsampler_block;
    std::vector<std::complex<float>> downsampler_data;
    filter_match2_vec1_cf32 match_block;
    std::vector<float> match_data;

    std::vector<packet_t> old_packets;
    std::vector<packet_t> new_packets;

    size_t tone_detected;
    size_t sync_detected;
};

// This works, but not used currently, use the samples embedded in the detector packet
class packet_extractor
{
public:
    packet_extractor(size_t stride,
                     size_t input_spb,
                     size_t output_spb,
                     size_t num_bytes);

    struct packet_t {
        uint64_t position;
        float freq_offset;
        float tone_snr_db;
        float sync_snr_db;
        std::vector<std::complex<float>> samples;
    };

    // input: stride
    const std::vector<packet_t>&
    work(const std::complex<float>* input,
         const std::vector<preamble_detector::packet_t>& new_detector_packets);

    const std::vector<packet_t>&
    work(const std::vector<std::complex<float>>& input,
         const std::vector<preamble_detector::packet_t>& new_detector_packets)
    {
        assert(input.size() == get_stride());
        return work(input.data(), new_detector_packets);
    }

    size_t get_stride() const { return stride; }

protected:
    const size_t stride;
    const size_t input_spb;
    const size_t output_spb;
    const size_t num_bytes;

    uint64_t position;
    signal_downsampler downsampler_block;
    std::vector<std::complex<float>> downsampler_data;
    history_base<std::complex<float>> history_block;

    std::deque<preamble_detector::packet_t> detector_packets;
    std::vector<packet_t> packets;
};

class viterbi_demod
{
public:
    viterbi_demod(size_t spb, bool force_gen = false);

    size_t get_node_bits() const { return NODE_BITS; };
    size_t get_spb() const { return spb; }

    const std::vector<float>& demodulate(const std::complex<float>* samples,
                                         size_t samples_size,
                                         const std::vector<uint8_t>& init_bytes);
    void print_stats() const;

protected:
    const size_t spb;
    const bool force_gen;

    enum {
        NODE_BITS = 4,
        NODES = 1 << NODE_BITS,
        EDGE_BITS = NODE_BITS + 1,
        EDGES = 1 << EDGE_BITS,
    };

    std::vector<std::complex<float>> edge_samples;
    static std::vector<std::complex<float>> create_edge_samples(size_t spb);

    float get_edge_weight_gen(const std::complex<float>* samples,
                              size_t step,
                              size_t edge) const;

    float get_edge_weight_21(const std::complex<float>* samples,
                             size_t step,
                             size_t edge) const;

    float get_edge_weight_25(const std::complex<float>* samples,
                             size_t step,
                             size_t edge) const;

    float get_edge_weight_26(const std::complex<float>* samples,
                             size_t step,
                             size_t edge) const;

    float get_edge_weight_31(const std::complex<float>* samples,
                             size_t step,
                             size_t edge) const;

    std::vector<float> weights;
    void find_weights(const std::complex<float>* samples, size_t samples_size);

    std::vector<std::array<float, NODES>> forward;
    std::array<float, NODES> backward0;
    std::array<float, NODES> backward1;
    std::vector<float> soft_bits;
    void find_paths(size_t init_node);
};

#endif // COHERENT_HPP
