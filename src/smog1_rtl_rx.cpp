/*
 * Copyright 2019-2021 Peter Horvath, Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "blocks.hpp"
#include "filter.hpp"
#include "predict.h"
#include "rtl_dongle.hpp"
#include "satellite.hpp"
#include "tle.hpp"
#include <unistd.h>
#include <atomic>
#include <cctype>
#include <chrono>
#include <csignal>
#include <fstream>
#include <iomanip>
#include <iostream>

#ifdef _WIN32
#include "getopt/getopt.h"
#endif

static std::atomic<bool> do_exit(false);

static const uint32_t TUNING_OFFSET = 55000;
static const uint32_t DEFAULT_SAMPLE_RATE = 1600000;
static const size_t DEFAULT_WORK_LENGTH = 2048;

void usage(void)
{
    std::cerr << "SMOG-1 recorder for RTL2832 based DVB-T receivers\n\n"
                 "Usage: smog1_rtl_rx [-options]\n"
                 "\t-d device_index (default: 0)\n"
                 "\t-T enable bias-T on GPIO PIN 0 (works for rtl-sdr.com v3 dongles)\n"
                 "\t-g tuner gain (default: automatic, NOT RECOMMENDED)\n"
                 "\t-p kalibrate-sdr reported fractional ppm error (default: 0.0)\n"
                 "\t-f forced continuous recording, no Doppler correction\n"
                 "\t-i track the given primary satellite ID (default: 99999)\n"
                 "\t-F downlink frequency for primary sat (default: 437345000.0 Hz)\n"
                 "\t-k download TLE data from celestrak.com\n"
                 "\t-S use 2 Msps/62.5 ksps mode (default: 1.6 Msps/50 ksps)\n"
                 "\t-O dump downconverted samples to STDOUT in binary cf32 format\n"
                 "\t-b disable the 1/128 rescaling of raw samples\n"
                 "\t-n download tle-new.txt instead of cubesat.txt from celestrak.com\n"
                 "\t-a download active.txt instead of cubesat.txt from celestrak.com\n"
                 "\t-s disable printing sat and signal statistics (default: false)\n"
                 "\t-B file buffer size, useful for slow SD cards (default: 4 Mb)\n"
                 "\t-e elevation limit for start recording (default: -2 deg)\n"
                 "\t-h prints this help message\n";
}

static void sighandler(int) { do_exit = true; }

void write_json(const Json::Value& root, const std::string& basename)
{
    std::ofstream metafile;
    metafile.open(basename + ".meta", std::ofstream::trunc);
    metafile << root;
    metafile.close();
}

int main(int argc, char* argv[])
{
    bool enable_biastee = false;
    bool update_tle = false;
    int dongle_gain = -1;
    float real_ppm_error = 0.0f;
    long sat1_id = 0;
    double sat1_downlink = 437345000;
    bool dump_samples = false;
    const char* device = NULL;
    size_t file_buffer_size = 4 * 1024 * 1024;
    float elevation_limit = -2.0f;
    bool print_statistics = true;
    uint32_t sample_rate = DEFAULT_SAMPLE_RATE;
    float scaling = 1.0f / 128.0f;
    std::string tle_file_name("cubesat.txt");
    std::ofstream azel_file_1;
    std::chrono::steady_clock::time_point start1;
    bool forced_mode = false;

    int opt;
    while ((opt = getopt(argc, argv, "fd:g:p:Tki:F:e:B:OShbasn")) != -1) {
        switch (opt) {
        case 'd':
            device = optarg;
            break;
        case 'g':
            dongle_gain = std::atof(optarg) * 10;
            break;
        case 'p':
            real_ppm_error = std::atof(optarg);
            break;
        case 'T':
            enable_biastee = true;
            break;
        case 'k':
            update_tle = true;
            break;
        case 'f':
            forced_mode = true;
            break;
        case 'i':
            sat1_id = std::atol(optarg);
            break;
        case 'F':
            sat1_downlink = std::atof(optarg);
            break;
        case 'e':
            elevation_limit = std::atof(optarg);
            break;
        case 'O':
            dump_samples = true;
            break;
        case 'S':
            sample_rate = 2000000;
            break;
        case 'b':
            scaling = 1.0f;
            break;
        case 'a':
            tle_file_name = "active.txt";
            break;
        case 'n':
            tle_file_name = "tle-new.txt";
            break;
        case 's':
            print_statistics = false;
            break;
        case 'B':
            file_buffer_size =
                1024 * 1024 * static_cast<size_t>(std::max(std::atol(optarg), 0L));
            break;
        case 'h':
        default:
            usage();
            return 1;
        }
    }

    const uint32_t output_sample_rate = sample_rate / 32;

    uint32_t dongle_center_freq = std::round(sat1_downlink - TUNING_OFFSET);
    int dongle_ppm_error = std::round(real_ppm_error);

    double real_center_freq =
        dongle_center_freq * (1.0 - (dongle_ppm_error - real_ppm_error) * 1e-6);

    sat_tracker tracker(
        { sat_tracker::sat_t(sat1_id, sat1_downlink) }, update_tle, tle_file_name);

    if (!tracker.is_trackable(0) && !forced_mode) {
        if (sat1_id == 0) {
            std::cerr << "ERROR: no satellite ID given." << std::endl;
        } else {
            std::cerr
                << "ERROR: no QTH file could be found and/or no TLE file could be found."
                << std::endl;
        }
        std::cerr << "ERROR: specify -f to force continuous recording with Doppler "
                     "correction disabled."
                  << std::endl;
        std::cerr << "ERROR: Exiting." << std::endl;
        return 1;
    }

    if (forced_mode) {
        std::cerr << "WARNING: Doppler correction disabled, using fixed RX frequency!"
                  << std::endl;
    }

    buffer_t buffer(2, 16 * 32 * DEFAULT_WORK_LENGTH * sizeof(float));
    std::cerr << "INFO: dongle buffer size: " << buffer.size()
              << ", file buffer size: " << file_buffer_size << std::endl;

    rtl_dongle dongle(buffer, scaling, device);
    if (!dongle.is_opened())
        return 2;

    dongle.set_sampling_rate(sample_rate);
    dongle.set_tuner_gain(dongle_gain);
    dongle.set_bias_tee(enable_biastee);
    dongle.set_center_freq(dongle_center_freq);
    dongle.set_ppm_error(dongle_ppm_error);

    Json::Value options;
    options["device"] = dongle.get_device_name();
    options["gain"] = dongle_gain;
    options["ppm_error"] = real_ppm_error;
    options["file_buffer"] = static_cast<unsigned int>(file_buffer_size);
    options["samp_rate"] = sample_rate;
    options["elevation_limit"] = elevation_limit;
    options["biastee"] = enable_biastee;

    std::cerr << "INFO: real center frequency is " << std::setprecision(1) << std::fixed
              << real_center_freq << " Hz" << std::endl;

    sat_recorder recorder(
        buffer, 0, output_sample_rate, DEFAULT_WORK_LENGTH, file_buffer_size);
    sat_recorder dumper(
        buffer, 1, output_sample_rate, DEFAULT_WORK_LENGTH, file_buffer_size);
    
    dongle.start();

    std::signal(SIGINT, sighandler);
    std::signal(SIGTERM, sighandler);
#ifndef _WIN32
    std::signal(SIGQUIT, sighandler);
    std::signal(SIGPIPE, sighandler);
#endif

    if (dump_samples)
        dumper.start("-");

    if (!print_statistics)
        std::cerr << "INFO: printing of satellite and signal statistic are disabled"
                  << std::endl;

    unsigned int delay_count = 0;
    unsigned int last_overruns = 0;
    float last_max_signal = 0.0;
    int last_max_signal_count = 0;

    recorder.set_correction((real_center_freq - sat1_downlink) / sample_rate);

    while (!do_exit) {
        if (++delay_count >= 100)
            delay_count = 0;

        if (!forced_mode)
            tracker.calculate(print_statistics && (delay_count == 0));

        // this will be overwritten if the satellite is above
        float dumper_correction = (real_center_freq - sat1_downlink) / sample_rate;

        if (tracker.is_trackable(0) && sat1_downlink > 0 && !forced_mode) {
            float correction =
                (real_center_freq - sat1_downlink - tracker.get_doppler(0)) / sample_rate;
            recorder.set_correction(correction);
            if (tracker.get_elevation(0) >= elevation_limit)
                dumper_correction = correction;
        }

        if (!recorder.is_running() &&
            (tracker.get_elevation(0) >= elevation_limit || forced_mode)) {
            std::string basename;
            if (!forced_mode)
                basename = tracker.get_filename(0, device);
            else {
                if (tracker.is_trackable(0))
                    basename = tracker.get_name(0);
                else
                    basename = "SCN" + std::to_string(sat1_id);

                basename += "_";
                basename += std::to_string(sat_tracker::current_daynum());
            }

            recorder.start(basename.c_str());

            if (!forced_mode) {
                Json::Value root;
                root["options"] = options;
                tracker.add_json_data(0, root);
                write_json(root, basename);
            }
        } else if (recorder.is_running()) {
            if (!forced_mode && tracker.get_elevation(0) < elevation_limit) {
                recorder.stop();
            }
        }

        dumper.set_correction(dumper_correction);

        if (delay_count == 0) {
            std::array<float, 2> stat = dongle.get_statistics();
            unsigned int overruns = dongle.get_overruns();

            if (print_statistics) {
                std::stringstream msg;
                msg << "INFO: signal amplitude maximum: " << std::setprecision(5)
                    << std::fixed << stat[0] << ", average: " << stat[1]
                    << ", total overruns: " << overruns << std::endl;
                std::cerr << msg.str();
            } else {
                if (overruns != last_overruns) {
                    std::stringstream msg;
                    msg << "WARNING: total overruns: " << overruns << std::endl;
                    std::cerr << msg.str();
                    last_overruns = overruns;
                }
                if (stat[0] != last_max_signal) {
                    last_max_signal = stat[0];
                    last_max_signal_count = 0;
                } else if (++last_max_signal_count >= 10) {
                    std::stringstream msg;
                    msg << "WARNING: signal amplitude max is permanently: "
                        << std::setprecision(5) << std::fixed << stat[0] << ", ";
                    if (stat[0] <= 0.0f)
                        msg << "check the rtl dongle";
                    else if (stat[0] >= 1.0f)
                        msg << "reduce the gain";
                    else
                        msg << "check the antenna";
                    msg << std::endl;
                    std::cerr << msg.str();
                    last_max_signal_count = 0;
                }
            }
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    if (do_exit)
        std::cerr << "INFO: aborted by signal" << std::endl;

    if (recorder.is_running())
        recorder.stop();

    if (dumper.is_running())
        dumper.stop();

    dongle.stop();

    return 0;
}
