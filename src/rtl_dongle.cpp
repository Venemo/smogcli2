/*
 * Copyright 2019-2020 Peter Horvath.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "rtl_dongle.hpp"
#include <cstring>
#include <iomanip>
#include <iostream>

rtl_dongle::rtl_dongle(buffer_t& buffer, float scaling, const char* device_string)
    : dev(NULL),
      center_freq(0),
      sampling_rate(0),
      convert(scaling),
      temp(4096, 0.0f),
      buffer(buffer),
      samples_max_sum(0),
      samples_max_cnt(0),
      samples_avg_sum(0),
      samples_avg_cnt(0),
      overruns(0)
{
    if (device_string == NULL)
        device_string = "0";

    int dev_index = verbose_device_search(device_string);
    if (dev_index < 0)
        return;

    if (rtlsdr_open(&dev, (uint32_t)dev_index) < 0)
        std::cerr << "ERROR: failed to open rtl-sdr device #" << dev_index << std::endl;
    else {
        char vendor[256], product[256], serial[256];
        rtlsdr_get_usb_strings(dev, vendor, product, serial);

        std::stringstream msg;
        msg << dev_index << ": " << vendor << " " << product << ", "
            << "SN: " << serial;
        device_name = msg.str();
    }
}

rtl_dongle::~rtl_dongle()
{
    set_bias_tee(false);
    rtlsdr_close(dev);
}

int rtl_dongle::set_center_freq(uint32_t freq)
{
    if (dev == nullptr)
        return -1;

    int r;
    if ((r = rtlsdr_set_center_freq(dev, freq)) < 0) {
        std::cerr << "ERROR: failed to set center frequency " << std::endl;
    } else {
        std::cerr << "INFO: setting dongle center frequency to " << std::fixed
                  << std::setprecision(3) << (freq * 1e-6) << " MHz" << std::endl;
        center_freq = freq;
    }
    return r;
}

int rtl_dongle::set_sampling_rate(uint32_t rate)
{
    if (dev == nullptr)
        return -1;

    int r;
    if ((r = rtlsdr_set_sample_rate(dev, rate)) < 0) {
        std::cerr << "ERROR: failed to set dongle sample rate " << rate << std::endl;
    } else {
        uint32_t real_rate;
        if ((real_rate = rtlsdr_get_sample_rate(dev)) != rate) {
            std::cerr << "WARNING: requested dongle sample rate: " << rate
                      << ", real sample rate: " << real_rate << std::endl;
            sampling_rate = real_rate;
        } else {
            std::cerr << "INFO: setting dongle sample rate to " << rate << std::endl;
            sampling_rate = rate;
        }
    }
    return r;
}

int rtl_dongle::set_tuner_gain(int gain)
{
    if (dev == nullptr)
        return -1;

    int r;
    if (gain < 0) {
        if ((r = rtlsdr_set_tuner_gain_mode(dev, 0)) < 0) {
            std::cerr << "WARNING: failed to set auto gain" << std::endl;
        } else {
            std::cerr << "WARNING: setting auto gain (NOT RECOMMENDED)" << std::endl;
        }
    } else {
        gain = nearest_gain(gain);
        if ((r = rtlsdr_set_tuner_gain(dev, gain)) < 0) {
            std::cerr << "WARNING: failed to set fixed gain " << gain << std::endl;
        } else {
            std::cerr << "INFO: setting fixed gain " << gain << std::endl;
        }
    }
    return r;
}

int rtl_dongle::set_bias_tee(bool enable)
{
    if (dev == nullptr)
        return -1;

    int r;
    if ((r = rtlsdr_set_bias_tee(dev, enable)) < 0) {
        std::cerr << "WARNING: failed to set bias_tee" << std::endl;
    } else {
        std::cerr << "INFO: " << (enable ? "enabled" : "disabled")
                  << " bias-T on GPIO PIN 0" << std::endl;
    }
    return r;
}

int rtl_dongle::set_ppm_error(int corr)
{
    if (dev == nullptr)
        return -1;
    if (corr == 0)
        return 0;

    int r;
    if ((r = rtlsdr_set_freq_correction(dev, corr)) < 0) {
        std::cerr << "WARNING: failed to set ppm correction " << corr << std::endl;
    } else {
        std::cerr << "INFO: setting dongle ppm correction to " << corr << std::endl;
    }
    return r;
}

int rtl_dongle::verbose_device_search(const char* s)
{
    int device_count, device;
    char* s2;
    char vendor[256], product[256], serial[256];

    device_count = static_cast<int>(rtlsdr_get_device_count());
    if (device_count <= 0) {
        std::cerr << "ERROR: No supported rtl-sdr devices found" << std::endl;
        return -1;
    }

    std::cerr << "INFO: found " << device_count << " device(s):" << std::endl;
    for (int i = 0; i < device_count; i++) {
        rtlsdr_get_device_usb_strings(static_cast<uint32_t>(i), vendor, product, serial);
        std::cerr << i << ": " << vendor << " " << product << ", "
                  << "SN: " << serial << std::endl;
    }

    /* does string look like raw id number */
    device = (int)strtol(s, &s2, 0);
    if (s2[0] == '\0' && device >= 0 && device < device_count) {
        std::cerr << "INFO: using device " << device << ": "
                  << rtlsdr_get_device_name((uint32_t)device) << std::endl;
        return device;
    }

    /* does string exact match a serial */
    for (int i = 0; i < device_count; i++) {
        rtlsdr_get_device_usb_strings(static_cast<uint32_t>(i), vendor, product, serial);
        if (strcmp(s, serial) != 0) {
            continue;
        }
        device = i;
        std::cerr << "INFO: using device " << device << ": "
                  << rtlsdr_get_device_name((uint32_t)device) << std::endl;
        return device;
    }

    /* does string prefix match a serial */
    for (int i = 0; i < device_count; i++) {
        rtlsdr_get_device_usb_strings(static_cast<uint32_t>(i), vendor, product, serial);
        if (strncmp(s, serial, strlen(s)) != 0) {
            continue;
        }
        device = i;
        std::cerr << "INFO: using device " << device << ": "
                  << rtlsdr_get_device_name((uint32_t)device) << std::endl;
        return device;
    }

    /* does string suffix match a serial */
    for (int i = 0; i < device_count; i++) {
        rtlsdr_get_device_usb_strings(static_cast<uint32_t>(i), vendor, product, serial);
        int offset = static_cast<int>(strlen(serial) - strlen(s));
        if (offset < 0) {
            continue;
        }
        if (strncmp(s, serial + offset, strlen(s)) != 0) {
            continue;
        }
        device = i;
        std::cerr << "INFO: using device " << device << ": "
                  << rtlsdr_get_device_name((uint32_t)device) << std::endl;
        return device;
    }

    std::cerr << "ERROR: no matching devices found" << std::endl;
    return -1;
}

int rtl_dongle::nearest_gain(int target_gain)
{
    if (dev == nullptr)
        return 0;

    if (rtlsdr_set_tuner_gain_mode(dev, 1) < 0) {
        std::cerr << "WARNING: failed to enable manual gain" << std::endl;
        return 0;
    }

    int count = rtlsdr_get_tuner_gains(dev, NULL);
    if (count <= 0) {
        return 0;
    }

    int* gains = new int[count];
    count = rtlsdr_get_tuner_gains(dev, gains);
    int nearest = gains[0];
    for (int i = 0; i < count; i++) {
        int err1 = abs(target_gain - nearest);
        int err2 = abs(target_gain - gains[i]);
        if (err2 < err1) {
            nearest = gains[i];
        }
    }
    delete[] gains;

    return nearest;
}

void rtl_dongle::buffer_writer(uint8_t* buf, uint32_t len, void* ctx)
{
    rtl_dongle* dongle = static_cast<rtl_dongle*>(ctx);
    dongle->buffer_writer2(buf, len);
}

void rtl_dongle::buffer_writer2(uint8_t* buf, uint32_t len)
{
    sum_max.work(buf, len);

    unsigned int o = 0;
    uint32_t pos = 0;
    while (pos < len) {
        size_t num = std::min(static_cast<size_t>(len - pos), temp.size());
        convert.work(buf + pos, temp.data(), num);

        // once we have overflow, then write with blocking to utilize RTL-SDR buffers
        size_t not_written = buffer.write(temp.data(), num * sizeof(float), o > 0);
        if (not_written > 0) {
            o = 1;
            assert(not_written % sizeof(float) == 0);
            assert(num * sizeof(float) <= not_written);
            num -= not_written / sizeof(float);
        }

        pos += num;
    }

    std::lock_guard<std::mutex> lock(mutex);
    samples_max_sum += sum_max.max;
    samples_max_cnt += 1;
    samples_avg_sum += sum_max.sum;
    samples_avg_cnt += len;
    overruns += o;
}

std::array<float, 2> rtl_dongle::get_statistics()
{
    uint64_t m;
    unsigned int d;
    uint64_t a;
    unsigned int c;
    {
        std::lock_guard<std::mutex> lock(mutex);
        m = samples_max_sum;
        samples_max_sum = 0;
        d = samples_max_cnt;
        samples_max_cnt = 0;
        a = samples_avg_sum;
        samples_avg_sum = 0;
        c = samples_avg_cnt;
        samples_avg_cnt = 0;
    }

    return { m * (1.0f / 128) / std::max(d, 1u), a * (1.0f / 128) / std::max(c, 1u) };
}

void rtl_dongle::worker()
{
    if (rtlsdr_reset_buffer(dev) < 0) {
        std::stringstream msg;
        msg << "WARNING: failed to reset rtl-sdr buffers" << std::endl;
        std::cerr << msg.str();
    }
    buffer.clear();
    rtlsdr_read_async(dev, rtl_dongle::buffer_writer, this, 0, 0);
}

void rtl_dongle::start()
{
    if (dev == nullptr)
        return;

    std::stringstream msg;
    msg << "INFO: starting dongle thread with " << buffer.size() << " bytes of buffer"
        << std::endl;
    std::cerr << msg.str();

    thread = std::thread(&rtl_dongle::worker, this);
}

void rtl_dongle::stop()
{
    if (dev == nullptr)
        return;

    std::stringstream msg;
    msg << "INFO: stopping dongle thread..." << std::endl;
    std::cerr << msg.str();

    rtlsdr_cancel_async(dev);
    thread.join();
    buffer.clear();
}
