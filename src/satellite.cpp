/*
 * Copyright 2019-2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "satellite.hpp"
#include <iostream>

std::vector<float> sat_downconv_32::filter1_taps = {
    -0.002082756423, -0.008784980946, -0.017511709457, -0.014608365510,
    0.019681066529,  0.092838159563,  0.183401412459,  0.247061393601,
    0.247061393601,  0.183401412459,  0.092838159563,  0.019681066529,
    -0.014608365510, -0.017511709457, -0.008784980946, -0.002082756423
};

std::vector<float> sat_downconv_32::filter2_taps = {
    0.002112595299,  0.004616092258,  -0.003990906170, -0.027988808060,
    -0.032591738149, 0.040454283127,  0.192486566147,  0.324892684587,
    0.324892684587,  0.192486566147,  0.040454283127,  -0.032591738149,
    -0.027988808060, -0.003990906170, 0.004616092258,  0.002112595299
};

std::vector<float> sat_downconv_32::filter3_taps = {
    -0.000505713644, 0.002707589221,  0.008450813497,  -0.008353996948,
    -0.047260911338, -0.012708152656, 0.174546722480,  0.383130427831,
    0.383130427831,  0.174546722480,  -0.012708152656, -0.047260911338,
    -0.008353996948, 0.008450813497,  0.002707589221,  -0.000505713644
};

std::vector<float> sat_downconv_32::filter4_taps = {
    -0.002118249860, -0.001671932888, 0.013734509359,  0.011369985519,
    -0.047594445131, -0.052684729586, 0.153361676611,  0.425576055678,
    0.425576055678,  0.153361676611,  -0.052684729586, -0.047594445131,
    0.011369985519,  0.013734509359,  -0.001671932888, -0.002118249860
};

std::vector<float> sat_downconv_32::filter5_taps = {
    -0.000569011031, -0.001189090067, 0.000868175649,  0.001409018577,  -0.000449318079,
    -0.002461313287, -0.000208051786, 0.003462680542,  0.001598143690,  -0.004260351849,
    -0.003770720750, 0.004435727655,  0.006705636034,  -0.003512417651, -0.010182522581,
    0.000978581012,  0.013758035061,  0.003652953916,  -0.016745470549, -0.010818817014,
    0.018195077447,  0.020954401641,  -0.016798915065, -0.034755076443, 0.010494420264,
    0.054101972321,  0.005268236912,  -0.086181199801, -0.048039748537, 0.180103890249,
    0.413195637908,  0.413195637908,  0.180103890249,  -0.048039748537, -0.086181199801,
    0.005268236912,  0.054101972321,  0.010494420264,  -0.034755076443, -0.016798915065,
    0.020954401641,  0.018195077447,  -0.010818817014, -0.016745470549, 0.003652953916,
    0.013758035061,  0.000978581012,  -0.010182522581, -0.003512417651, 0.006705636034,
    0.004435727655,  -0.003770720750, -0.004260351849, 0.001598143690,  0.003462680542,
    -0.000208051786, -0.002461313287, -0.000449318079, 0.001409018577,  0.000868175649,
    -0.001189090067, -0.000569011031
};

sat_downconv_32::sat_downconv_32(size_t work_length)
    : buffer2(32 * work_length),
      sinusoid(0.0f),
      buffer3(32 * work_length),
      buffer4(32 * work_length),
      filter1(filter1_taps),
      buffer5(16 * work_length),
      filter2(filter2_taps),
      buffer6(8 * work_length),
      filter3(filter3_taps),
      buffer7(4 * work_length),
      filter4(filter4_taps),
      buffer8(2 * work_length),
      filter5(filter5_taps, 2),
      buffer9(work_length)
{
    assert(work_length > 0);
}

sat_downconv_32::~sat_downconv_32() {}

void sat_downconv_32::work()
{
    sinusoid.work(buffer3);
    multiply.work(buffer2, buffer3, buffer4);
    filter1.work(buffer4, buffer5);
    filter2.work(buffer5, buffer6);
    filter3.work(buffer6, buffer7);
    filter4.work(buffer7, buffer8);
    filter5.work(buffer8, buffer9);
}

sat_recorder::sat_recorder(buffer_t& buffer,
                           size_t reader,
                           uint32_t sample_rate,
                           size_t work_length,
                           size_t file_buffer_length)
    : buffer(buffer),
      reader(reader),
      ddc(work_length),
      sink(file_sink::CF32, sample_rate, file_buffer_length),
      running(false)
{
    buffer.enable(reader, false);
}

void sat_recorder::worker()
{
    std::string filename1;
    std::string filename2;
    if (basename == "-") {
        filename1 = "STDOUT";
        filename2 = "-";
    } else {
        filename1 = basename + ".cf32";
        filename2 = filename1;
    }

    {
        std::stringstream msg;
        msg << "INFO: ***** started recording to " << filename1 << std::endl;
        std::cerr << msg.str();
    }

    sink.open(filename2.c_str());
    buffer.enable(reader, true);

    while (running) {
        if (buffer.read(reader, ddc.input()) != 0)
            break;

        ddc.set_correction(correction);
        ddc.work();
        sink.write(ddc.output());
    }

    buffer.enable(reader, false);
    sink.close();

    {
        std::stringstream msg;
        msg << "INFO: ***** finished recording to " << filename1 << std::endl;
        std::cerr << msg.str();
    }
}

void sat_recorder::start(const char* basename)
{
    this->basename = basename;
    assert(!running);
    running = true;
    thread = std::thread(&sat_recorder::worker, this);
}

void sat_recorder::stop()
{
    assert(running);
    running = false;
    buffer.enable(reader, false);
    thread.join();
}

std::vector<float> samples_dumper::filter1_taps = {
    0.002071166610,  0.005619206027,  -0.001277127255, -0.027227304193,
    -0.038911462700, 0.030794210479,  0.191808039764,  0.337074740372,
    0.337074740372,  0.191808039764,  0.030794210479,  -0.038911462700,
    -0.027227304193, -0.001277127255, 0.005619206027,  0.002071166610
};

std::vector<float> samples_dumper::filter2_taps = {
    -0.002874605675, 0.000002976062,  0.015899254355,  0.006313002843,
    -0.052689061440, -0.043743260808, 0.161983744514,  0.415099682150,
    0.415099682150,  0.161983744514,  -0.043743260808, -0.052689061440,
    0.006313002843,  0.015899254355,  0.000002976062,  -0.002874605675
};

std::vector<float> samples_dumper::filter3_taps = {
    0.001092399430,  -0.000263613465, -0.002529488436, -0.001362273744, 0.003754523942,
    0.004374306203,  -0.004197279412, -0.009674495472, 0.001516901343,  0.016108227687,
    0.006157490650,  -0.021676445234, -0.021013526141, 0.022399117547,  0.045228438204,
    -0.011591966475, -0.085092644381, -0.030089854675, 0.187651580641,  0.398623869771,
    0.398623869771,  0.187651580641,  -0.030089854675, -0.085092644381, -0.011591966475,
    0.045228438204,  0.022399117547,  -0.021013526141, -0.021676445234, 0.006157490650,
    0.016108227687,  0.001516901343,  -0.009674495472, -0.004197279412, 0.004374306203,
    0.003754523942,  -0.001362273744, -0.002529488436, -0.000263613465, 0.001092399430
};

samples_dumper::samples_dumper(buffer_t& buffer, size_t reader, size_t work_length)
    : buffer(buffer),
      reader(reader),
      buffer2(8 * work_length),
      sinusoid(0.0f),
      buffer3(8 * work_length),
      buffer4(8 * work_length),
      filter1(filter1_taps),
      buffer5(4 * work_length),
      filter2(filter2_taps),
      buffer6(2 * work_length),
      filter3(filter3_taps, 2),
      buffer7(work_length),
      running(false),
      correction(0.0f)
{
    assert(work_length > 0);
    buffer.enable(reader, false);
}

void samples_dumper::worker()
{
    {
        std::stringstream msg;
        msg << "INFO: ***** started dumping to STDOUT" << std::endl;
        std::cerr << msg.str();
    }

    buffer.enable(reader, true);

    while (running) {
        if (buffer.read(reader, buffer2) != 0)
            break;

        sinusoid.set_frequency(correction);
        sinusoid.work(buffer3);
        multiply.work(buffer2, buffer3, buffer4);
        filter1.work(buffer4, buffer5);
        filter2.work(buffer5, buffer6);
        filter3.work(buffer6, buffer7);

        std::cout.write(
            reinterpret_cast<const char*>(buffer7.data()),
            static_cast<std::streamsize>(buffer7.size() * sizeof(std::complex<float>)));
    }

    buffer.enable(reader, false);

    {
        std::stringstream msg;
        msg << "INFO: ***** finished dumping to STDOUT" << std::endl;
        std::cerr << msg.str();
    }
}

void samples_dumper::start()
{
    assert(!running);
    running = true;
    thread = std::thread(&samples_dumper::worker, this);
}

void samples_dumper::stop()
{
    assert(running);
    running = false;
    buffer.enable(reader, false);
    thread.join();
}
