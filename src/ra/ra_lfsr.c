/* Copyright (C) Miklos Maroti 2015-2016 */

#include "ra_lfsr.h"
#include <assert.h>

static ra_index_t ra_lfsr_mask;
static ra_index_t ra_lfsr_state;
static ra_index_t ra_lfsr_offset;

/* last element returned will be seqno */
void ra_lfsr_init(uint8_t seqno)
{
    /* make sure that ra_length_init is called */
    assert(ra_data_length > 0);

    ra_lfsr_mask = ra_lfsr_masks[seqno];
    ra_lfsr_offset = ra_data_length >> (1 + seqno);
    ra_lfsr_state = 1 + seqno + ra_lfsr_offset;
}

ra_index_t ra_lfsr_next()
{
    ra_index_t b;

    /* this loop runs at most twice on average */
    do {
        b = ra_lfsr_state & 0x1;
        ra_lfsr_state >>= 1;
        ra_lfsr_state ^= (-b) & ra_lfsr_mask;
    } while (ra_lfsr_state > ra_data_length);

    b = ra_lfsr_state - 1;
    if (b < ra_lfsr_offset)
        b += ra_data_length;
    b -= ra_lfsr_offset;
    return b;
}

ra_index_t ra_lfsr_prev()
{
    ra_index_t b;

    /* this loop runs at most twice on average */
    do {
        b = ra_lfsr_state >> ra_lfsr_highbit;
        ra_lfsr_state <<= 1;
        ra_lfsr_state ^= (-b) & (0x01 | ra_lfsr_mask << 1);
    } while (ra_lfsr_state > ra_data_length);

    b = ra_lfsr_state - 1;
    if (b < ra_lfsr_offset)
        b += ra_data_length;
    b -= ra_lfsr_offset;
    return b;
}
