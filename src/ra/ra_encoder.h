/* Copyright (C) Miklos Maroti 2015-2016 */

#ifndef __RA_ENCODER_H__
#define __RA_ENCODER_H__

#include "ra_config.h"

#ifdef __cplusplus
extern "C" {
#endif

void ra_encoder_init(const ra_word_t* packet);

/* call this ra_code_length times to get all code words */
ra_word_t ra_encoder_next();

/* this calls the above two functions */
void ra_encoder(const ra_word_t* packet, ra_word_t* output);

#ifdef __cplusplus
}
#endif

#endif //__RA_ENCODER_H__
