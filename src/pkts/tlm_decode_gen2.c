/*
 * Packet interpreter for SMOG-1 pocketqube satellites
 * Copyright (C) 2020-2021 szlldm, Peter Horvath, Miklos Maroti
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#if defined(_WIN32)
#define __USE_MINGW_ANSI_STDIO
#endif

#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#ifndef BUILD_SMOG1
#error BUILD TARGET NOT DEFINED!
#endif

#define DOWNLINK_PCKT_TYPE_TELEMETRY_1 (1)
#define DOWNLINK_PCKT_TYPE_TELEMETRY_2 (2)
#define DOWNLINK_PCKT_TYPE_TELEMETRY_3 (3)
#define DOWNLINK_PCKT_TYPE_BEACON (4)
#define DOWNLINK_PCKT_TYPE_SPECTRUM_RESULT (5)
#define DOWNLINK_PCKT_TYPE_FILE_INFO (6)
#define DOWNLINK_PCKT_TYPE_FILE_FRAGMENT (7)

#define DOWNLINK_PCKT_TYPE_RESERVED_SYNC (151) // SYNC_PCKT first byte

#if defined(_WIN32)
time_t timegm(struct tm* timeptr) { return _mkgmtime(timeptr); }

// On Windows, Visual Studio does not define gmtime_r. However, mingw might
// do (or might not do).
#ifndef gmtime_r
struct tm* gmtime_r(const time_t* t, struct tm* r)
{
    // gmtime is threadsafe in windows because it uses TLS
    struct tm* theTm = gmtime(t);
    if (theTm) {
        *r = *theTm;
        return r;
    } else {
        return 0;
    }
}
#endif // gmtime_r
#endif // _WIN32

uint16_t crc16_calc(uint8_t* data, size_t len)
{
    uint16_t crc = 0;
    for (size_t i = 0; i < len; i++) {
        // we do not reverse the bits, so 0x8005 becomes 0xa001
        crc ^= data[i];

        for (int j = 0; j < 8; j++) {
            if ((crc & 0x0001) != 0) {
                crc = (crc >> 1) ^ 0xa001;
            } else {
                crc = crc >> 1;
            }
        }
    }
    return crc;
}

double helper_ACK_INFO_RSSI_CONVERT(uint8_t u8)
{
    double tmp;

    tmp = u8;
    tmp /= 2.0;
    tmp -= 131.0;

    return tmp;
}

bool hex_check(char c)
{
    if ((c >= '0') && (c <= '9'))
        return true;
    if ((c >= 'A') && (c <= 'F'))
        return true;

    return false;
}

uint8_t hex_to_num(char c)
{
    if ((c >= '0') && (c <= '9'))
        return (uint8_t)(c - '0');
    if ((c >= 'A') && (c <= 'F'))
        return (uint8_t)(c + 10 - 'A');
    return 0;
}

char upper_case(char c)
{
    if (c < 'a')
        return c;
    if (c > 'z')
        return c;
    return c - ('a' - 'A');
}

typedef struct {
    int rssi;
    int pckt_len;
    uint8_t pckt[1024];
} pckt_t;


static const char* const_mppt_panel_names[6] = { "Bottom", "Front", "Right",
                                                 "Left",   "Back",  "Top" };

void pckt_proc_DOWNLINK_PCKT_TYPE_TELEMETRY_1(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mTelemetry 1\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint16_t u16;
    uint8_t u8;
    int i, p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];

    uint8_t* buf = pckt->pckt;

    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);

    for (i = 0; i < 6; i++) {
        printf("\tMPPT-%d (%s):\n", i + 1, const_mppt_panel_names[i]);

        memcpy(&i32, &buf[p], 4);
        p += 4;

        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        printf("\t\tTimestamp: %s\n", timestr);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        double tmpd = ((int16_t)u16) / 10.0;
        printf("\t\tTemperature: %.1fC°\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tLight sensor: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tInput current: %humA\n", u16);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tInput voltage: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tOutput current: %humA\n", u16);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tOutput voltage: %.3fV\n", tmpd);
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\t\tPanel number: %hhu\n", (uint8_t)((u8 >> 5) & 0x07));
        switch ((u8 >> 3) & 0x03) {
        case 0:
            printf("\t\tAntenna status: closed\n");
            break;
        case 1:
            printf("\t\tAntenna status: open\n");
            break;
        case 2:
            printf("\t\tAntenna status: not monitored\n");
            break;
        case 3:
            printf("\t\tAntenna status: INVALID!\n");
            break;
        }
    }

    printf("\tACK-INFO: ");
    for (i = 0; i < 3; i++) {
        memcpy(&u16, &buf[p], 2);
        p += 2;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("%hu (%.0f dBm RSSI)\t", u16, helper_ACK_INFO_RSSI_CONVERT(u8));
    }
    printf("\n");
    memcpy(&i32, &buf[p], 4);
    printf("\tOBC RMU_RSTCAUSE: %d\n", i32);
}

void pckt_proc_DOWNLINK_PCKT_TYPE_TELEMETRY_2(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mTelemetry 2\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint16_t u16;
    uint8_t u8;
    int i, p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];
    double tmpd;

    uint8_t* buf = pckt->pckt;

    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);

    for (i = 0; i < 2; i++) {
        printf("\tPCU-%d DEP telemetry:\n", i + 1);

        memcpy(&i32, &buf[p], 4);
        p += 4;

        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        printf("\t\tTimestamp: %s\n", timestr);

        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\t\tDeployment Switch 1 status: %d\n", (u8 & 0x80) ? 1 : 0);
        printf("\t\tDeployment Switch 2 status: %d\n", (u8 & 0x40) ? 1 : 0);
        printf("\t\tRemove Before Flight status: %d\n", (u8 & 0x20) ? 1 : 0);
        printf("\t\tPCU deployment status: %d\n", (u8 & 0x08) ? 1 : 0);
        printf("\t\tAntenna deployment status: %d\n", (u8 & 0x04) ? 1 : 0);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tPCU boot counter: %hu\n", u16);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tPCU uptime: %hu minutes\n", u16);
    }

    for (i = 0; i < 2; i++) {
        printf("\tPCU-%d SDC telemetry:\n", i + 1);

        memcpy(&i32, &buf[p], 4);
        p += 4;

        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        printf("\t\tTimestamp: %s\n", timestr);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tSDC1 input current: %humA\n", u16);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tSDC1 output current: %humA\n", u16);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tSDC1 output voltage: %.3fV\n", tmpd);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tSDC2 input current: %humA\n", u16);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tSDC2 output current: %humA\n", u16);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tSDC2 output voltage: %.3fV\n", tmpd);

        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\t\tSDC1 current limiter overcurrent status: %d\n", (u8 & 0x80) ? 1 : 0);
        printf("\t\tSDC1 voltage limiter overvoltage status: %d\n", (u8 & 0x40) ? 1 : 0);
        printf("\t\tSDC2 current limiter overcurrent status: %d\n", (u8 & 0x20) ? 1 : 0);
        printf("\t\tSDC2 voltage limiter overvoltage status: %d\n", (u8 & 0x10) ? 1 : 0);
    }

    for (i = 0; i < 2; i++) {
        printf("\tPCU-%d BAT telemetry:\n", i + 1);

        memcpy(&i32, &buf[p], 4);
        p += 4;

        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        printf("\t\tTimestamp: %s\n", timestr);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tBattery voltage: %.3fV\n", tmpd);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tBattery charge current: %humA\n", u16);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tBattery discharge current: %humA\n", u16);

        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\t\tCharge overcurrent status: %d\n", (u8 & 0x80) ? 1 : 0);
        printf("\t\tCharge overvoltage status: %d\n", (u8 & 0x40) ? 1 : 0);
        printf("\t\tDischarge overcurrent status: %d\n", (u8 & 0x20) ? 1 : 0);
        printf("\t\tDischarge overvoltage status: %d\n", (u8 & 0x10) ? 1 : 0);
        printf("\t\tCharge enabled: %d\n", (u8 & 0x08) ? 1 : 0);
        printf("\t\tDischarge enabled: %d\n", (u8 & 0x04) ? 1 : 0);
    }

    for (i = 0; i < 2; i++) {
        printf("\tPCU-%d BUS telemetry:\n", i + 1);

        memcpy(&i32, &buf[p], 4);
        p += 4;

        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        printf("\t\tTimestamp: %s\n", timestr);

        memcpy(&u16, &buf[114 + (i * 2)], 2); // peek forward
        tmpd = u16 / 1000.0;
        printf("\t\tPCU-%d voltage: %.3fV\n", i + 1, tmpd);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        // printf("\t\tUnregulated bus voltage: %.3fV\n", tmpd);
        printf("\t\t\x1b[1m\x1b[33mUnregulated bus voltage: %.3fV\n\x1b[0m", tmpd);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tRegulated bus voltage: %.3fV\n", tmpd);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tOBC 1 current consumption: %humA\n", u16);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tOBC 2 current consumption: %humA\n", u16);

        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\t\tOBC 1 current limiter overcurrent status: %d\n", (u8 & 0x80) ? 1 : 0);
        printf("\t\tOBC 2 current limiter overcurrent status: %d\n", (u8 & 0x40) ? 1 : 0);
    }


    printf("\tACK-INFO: ");
    for (i = 0; i < 3; i++) {
        memcpy(&u16, &buf[p], 2);
        p += 2;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("%hu (%.0f dBm RSSI)\t", u16, helper_ACK_INFO_RSSI_CONVERT(u8));
    }
    printf("\n");
}

void pckt_proc_DOWNLINK_PCKT_TYPE_TELEMETRY_3(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mTelemetry 3\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint32_t u32;
    (void)u32;
    uint16_t u16;
    uint8_t u8;
    int i, p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];
    double tmpd;
    int16_t vector[3];

    uint8_t* buf = pckt->pckt;

    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = u16 / 1000.0;
    printf("\tOBC supply voltage: %.3fV\n", tmpd);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = ((int16_t)u16) / 10.0;
    printf("\tRTCC-1 Temperature: %.1fC°\n", tmpd);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = ((int16_t)u16) / 10.0;
    printf("\tRTCC-2 Temperature: %.1fC°\n", tmpd);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = ((int16_t)u16) / 10.0;
    printf("\tOBC Temperature: %.1fC°\n", tmpd);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = ((int16_t)u16) / 10.0;
    printf("\tEPS2 panel A temperature 1: %.1fC°\n", tmpd);
    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = ((int16_t)u16) / 10.0;
    printf("\tEPS2 panel A temperature 2: %.1fC°\n", tmpd);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tCurrent COM data rate = %hhu, TX power level = %hhu\n",
           (uint8_t)((u8 >> 5) & 0x07),
           (uint8_t)((u8 >> 1) & 0x0f));

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tCurrent COM TX current consumption: %humA\n", u16);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tCurrent COM RX current consumption: %humA\n", u16);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tCOM 1 overcurrent status: %d\n", (u8 & 0x80) ? 1 : 0);
    printf("\tCOM 1 limiter switch status: %d\n", (u8 & 0x20) ? 1 : 0);
    printf("\tCOM 1 limiter switch override status: %d\n", (u8 & 0x08) ? 1 : 0);
    printf("\tCOM 2 overcurrent status: %d\n", (u8 & 0x40) ? 1 : 0);
    printf("\tCOM 2 limiter switch status: %d\n", (u8 & 0x10) ? 1 : 0);
    printf("\tCOM 2 limiter switch override status: %d\n", (u8 & 0x04) ? 1 : 0);

    for (i = 0; i < 2; i++) {
        printf("\tMSEN OBC-%d BUS telemetry:\n", i + 1);
        memcpy(&vector[0], &buf[p], 2);
        p += 2;
        memcpy(&vector[1], &buf[p], 2);
        p += 2;
        memcpy(&vector[2], &buf[p], 2);
        p += 2;
        printf("\t\tGyroscope: % 5.0f\t% 5.0f\t% 5.0f\n",
               1.0 * vector[0],
               1.0 * vector[1],
               1.0 * vector[2]);

        memcpy(&vector[0], &buf[p], 2);
        p += 2;
        memcpy(&vector[1], &buf[p], 2);
        p += 2;
        memcpy(&vector[2], &buf[p], 2);
        p += 2;
        printf("\t\tMagneto:   % 5.0f\t% 5.0f\t% 5.0f\n",
               1.0 * vector[0],
               1.0 * vector[1],
               1.0 * vector[2]);

        memcpy(&vector[0], &buf[p], 2);
        p += 2;
        memcpy(&vector[1], &buf[p], 2);
        p += 2;
        memcpy(&vector[2], &buf[p], 2);
        p += 2;
        printf("\t\tAccelerometer:   % 5.0f\t% 5.0f\t% 5.0f\n",
               1.0 * vector[0],
               1.0 * vector[1],
               1.0 * vector[2]);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = ((int16_t)u16) / 10.0;
        printf("\t\tTemperature: %.1fC°\n", tmpd);
    }

    memcpy(&u8, &buf[p], 1);
    p += 1;

    printf("\tCurrent OBC: OBC-%d\n", (u8 & 0x02) ? 2 : 1);
    printf("\tCurrent COM: COM-%d\n", (u8 & 0x01) ? 2 : 1);
    printf("\tFLASH-1 functional: %d\n", (u8 & 0x20) ? 1 : 0);
    printf("\tFLASH-2 functional: %d\n", (u8 & 0x10) ? 1 : 0);
    printf("\tMSEN-1  functional: %d\n", (u8 & 0x80) ? 1 : 0);
    printf("\tMSEN-2  functional: %d\n", (u8 & 0x40) ? 1 : 0);
    printf("\tRTCC-1  functional: %d\n", (u8 & 0x08) ? 1 : 0);
    printf("\tRTCC-2  functional: %d\n", (u8 & 0x04) ? 1 : 0);


    // COM telemetry
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tLast COM telemetry: %s\n", timestr);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tSWR bridge reading: %hhu\n", u8);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tLast received packet RSSI: %hhd\n", (int8_t)u8);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tSpectrum analyzer status: %d\n", (u8 & 0x01) ? 1 : 0);
    tmpd = (u8 & 0x7F) * 30.0 / 1000.0;
    printf("\tSpectrum analyzer voltage: %.3fV\n", tmpd);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = u16 / 1000.0;
    printf("\tActive COM voltage: %.3fV\n", tmpd);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = ((int16_t)u16) / 10.0;
    printf("\tActive COM temperature: %.1fC°\n", tmpd);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = ((int16_t)u16) / 10.0;
    printf("\tActive COM spectrum analyzer temperature: %.1fC°\n", tmpd);

    for (i = 0; i < 2; i++) {
        memcpy(&i32, &buf[p], 4);
        p += 4;
        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;
        printf("\tTID-%d measurement:\n", (i + 1));
        printf("\t\tTimestamp: %s\n", timestr);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = ((int16_t)u16) / 10.0;
        printf("\t\tTemperature: %.1fC°\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVoltage: %.3fV\n", tmpd);
        u32 = (uint32_t)((buf[p] << 16) + (buf[p + 1] << 8) + (buf[p + 2] << 0));
        p += 3;
        printf("\t\tRadFET-1 voltage: %uuV\n", u32);
        u32 = (uint32_t)((buf[p] << 16) + (buf[p + 1] << 8) + (buf[p + 2] << 0));
        p += 3;
        printf("\t\tRadFET-2 voltage: %uuV\n", u32);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tMeasurement serial number: %hu\n", u16);
    }

    printf("\tACK-INFO: ");
    for (i = 0; i < 3; i++) {
        memcpy(&u16, &buf[p], 2);
        p += 2;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("%hu (%.0f dBm RSSI)\t", u16, helper_ACK_INFO_RSSI_CONVERT(u8));
    }
    printf("\n");
}

void pckt_proc_DOWNLINK_PCKT_TYPE_BEACON(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mBeacon\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint16_t u16;
    uint8_t u8;
    uint32_t uptime;
    uint8_t uptime_arr[3];
    int i, p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];
    char version[8];

    uint8_t* buf = pckt->pckt;

    char beacon_msg[81];

    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);

    memcpy(beacon_msg, &buf[p], 80);
    p += 80;
    beacon_msg[80] = 0;
    printf("\tBeacon message: %s\n", beacon_msg);

    // 17 bytes diagnostic

    // unused bit
    p += 1;

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tRX garbage packets: %hhd\n", (int8_t)u8);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tRX bad serial packets: %hhd\n", (int8_t)u8);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tRX invalid packets: %hhd\n", (int8_t)u8);

    memcpy(&i32, &buf[p], 4);
    p += 4;
    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;
    printf("\tLast received uplink command timestamp: %s\n", timestr);

    uptime = 0;
    memcpy(&uptime_arr[0], &buf[p], 1);
    p += 1;
    memcpy(&uptime_arr[1], &buf[p], 1);
    p += 1;
    memcpy(&uptime_arr[2], &buf[p], 1);
    p += 1;
    uptime = uptime_arr[0] * pow(2, 16) + uptime_arr[1] * pow(2, 8) + uptime_arr[2];
    printf("\tOBC uptime: %u\n", uptime);

    uptime = 0;
    memcpy(&uptime_arr[0], &buf[p], 1);
    p += 1;
    memcpy(&uptime_arr[1], &buf[p], 1);
    p += 1;
    memcpy(&uptime_arr[2], &buf[p], 1);
    p += 1;
    uptime = uptime_arr[0] * pow(2, 16) + uptime_arr[1] * pow(2, 8) + uptime_arr[2];
    printf("\tCOM uptime: %u\n", uptime);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tRegulated bus voltage drop in TX mode: %hhumV\n", u8 * 10);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tNumber of timed tasks: %hhu\n", u8);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tCurrent flash: FLASH-%d\n", (u8 & 0x04) ? 2 : 1);
    printf("\t\tfilesystem works: %d\n", (u8 & 0x08) ? 1 : 0);
    printf("\t\tTCXO works: %d\n", (u8 & 0x10) ? 1 : 0);
    printf("\t\tEnergy mode: %d\n", ((u8 & 0xe0)) >> 5);

    // 7 bytes version
    memcpy(version, &buf[p], 7);
    p += 7;
    version[7] = 0;
    printf("\tVersion: %s\n", version);


    printf("\tACK-INFO: ");
    for (i = 0; i < 3; i++) {
        memcpy(&u16, &buf[p], 2);
        p += 2;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("%hu (%.0f dBm RSSI)\t", u16, helper_ACK_INFO_RSSI_CONVERT(u8));
    }
    printf("\n");
}

void pckt_proc_DOWNLINK_PCKT_TYPE_SPECTRUM_RESULT(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mSpectrum result\n\x1b[0m");
    if (pckt->pckt_len < 256) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    int p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];

    uint32_t freqs[2];
    uint8_t rbw, pckt_cnt, pckt_index;
    uint16_t spectrum_len, measid;
    double startfreq, stopfreq, stepfreq;

    uint8_t* buf = pckt->pckt;


    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);

    memcpy(&freqs[0], &buf[p], 4);
    p += 4;
    memcpy(&freqs[1], &buf[p], 4);
    p += 4;
    memcpy(&rbw, &buf[p], 1);
    p += 1;
    memcpy(&pckt_index, &buf[p], 1);
    p += 1;
    memcpy(&pckt_cnt, &buf[p], 1);
    p += 1;
    memcpy(&spectrum_len, &buf[p], 2);
    p += 2;

    p += 2;

    memcpy(&measid, &buf[p], 2);

    startfreq = freqs[0];
    stepfreq = freqs[1];
    startfreq += 224.0 * pckt_index * stepfreq;
    stopfreq = startfreq + (stepfreq * spectrum_len);

    printf("\tMeasured %hu samples between %.6fMHz ... %.6fMHz with %.3fkHz stepsize and "
           "%hhu RBW\n",
           spectrum_len,
           startfreq / 1000000.0,
           stopfreq / 1000000.0,
           stepfreq / 1000.0,
           rbw);
    printf("\tSending fragment %hhu/%hhu of measurement \"%hu\"...\n",
           pckt_index,
           pckt_cnt,
           measid);
}

void pckt_proc_DOWNLINK_PCKT_TYPE_FILE_INFO(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mFile Info\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint8_t u8;
    int i, p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];

    uint8_t* buf = pckt->pckt;

    uint8_t file_id, file_type;
    uint16_t page_addr;
    char file_name[11];

    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);

    for (i = 0; i < 5; i++) {
        memcpy(&file_id, &buf[p], 1);
        p += 1;
        memcpy(&file_type, &buf[p], 1);
        p += 1;
        memcpy(&page_addr, &buf[p], 2);
        p += 2;

        uint32_t file_size = 0;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        file_size += u8;
        file_size <<= 8;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        file_size += u8;
        file_size <<= 8;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        file_size += u8;

        memcpy(&i32, &buf[p], 4);
        p += 4;
        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        memcpy(file_name, &buf[p], 10);
        p += 10;
        file_name[10] = 0;

        printf("\tFile ID=%hhu, type=%hhu, name=\"%s\" size=%u, starting at page %hu. "
               "Timestamp: %s\n",
               file_id,
               file_type,
               file_name,
               file_size,
               page_addr,
               timestr);
    }
}

void pckt_proc_DOWNLINK_PCKT_TYPE_FILE_FRAGMENT(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mFile Fragment\n\x1b[0m");
    if (pckt->pckt_len < 256) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint8_t u8;
    int p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];

    uint8_t* buf = pckt->pckt;

    uint16_t pckt_index, pckt_cnt;
    uint8_t file_type;
    uint16_t page_addr;
    uint32_t file_size;
    char file_name[11];

    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);

    memcpy(&pckt_index, &buf[p], 2);
    p += 2;
    memcpy(&pckt_cnt, &buf[p], 2);
    p += 2;
    memcpy(&file_type, &buf[p], 1);
    p += 1;
    memcpy(&page_addr, &buf[p], 2);
    p += 2;

    file_size = 0;
    memcpy(&u8, &buf[p], 1);
    p += 1;
    file_size += u8;
    file_size <<= 8;
    memcpy(&u8, &buf[p], 1);
    p += 1;
    file_size += u8;
    file_size <<= 8;
    memcpy(&u8, &buf[p], 1);
    p += 1;
    file_size += u8;

    memcpy(&i32, &buf[p], 4);
    p += 4;
    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    memcpy(file_name, &buf[p], 10);
    file_name[10] = 0;

    printf("\tDownloading %hu/%hu fragment of the file \"%s\" with type=%hhu, size=%u at "
           "page %hu. Timestamp: %s\n",
           pckt_index,
           pckt_cnt,
           file_name,
           file_type,
           file_size,
           page_addr,
           timestr);

    if ((pckt_cnt == 0) || (pckt_index >= pckt_cnt) || ((file_size / 217) > pckt_cnt)) {
        printf("\n\tERROR: inconsistent file parameter(s)!\n");
        return;
    }
}

void file_error()
{
    printf("File error\n");
    exit(-1);
}

bool packet_check(uint8_t* pckt, int pckt_len)
{
    if (!pckt)
        return false;
    if (pckt_len < 128)
        return false;

    uint16_t tmpcrc;
    uint8_t crc[2];

    switch (pckt[0]) {
    case DOWNLINK_PCKT_TYPE_TELEMETRY_1:
    case DOWNLINK_PCKT_TYPE_TELEMETRY_2:
    case DOWNLINK_PCKT_TYPE_TELEMETRY_3:
    case DOWNLINK_PCKT_TYPE_BEACON:
    case DOWNLINK_PCKT_TYPE_FILE_INFO:
        if (pckt_len != 128)
            return false;
        tmpcrc = crc16_calc(pckt, (size_t)(pckt_len - 2));
        crc[0] = tmpcrc & 0x00ff;
        crc[1] = (tmpcrc >> 8) & 0x00ff;
        return (memcmp(&pckt[pckt_len - 2], crc, 2) == 0);
    case DOWNLINK_PCKT_TYPE_SPECTRUM_RESULT:
        if (pckt_len != 256)
            return false;
        tmpcrc = crc16_calc(pckt, (size_t)(pckt_len - 2));
        crc[0] = tmpcrc & 0x00ff;
        crc[1] = (tmpcrc >> 8) & 0x00ff;
        return (memcmp(&pckt[pckt_len - 2], crc, 2) == 0);
    case DOWNLINK_PCKT_TYPE_FILE_FRAGMENT:
        tmpcrc = crc16_calc(pckt, (size_t)(pckt_len - 2));
        crc[0] = tmpcrc & 0x00ff;
        crc[1] = (tmpcrc >> 8) & 0x00ff;
        return (memcmp(&pckt[pckt_len - 2], crc, 2) == 0);
    default:
        return false;
    }
}

int main(int argc, char** argv)
{
    if (argc < 2) {
        printf("Usage: %s filename\n", argv[0]);
        exit(-1);
    }

    FILE* f = fopen(argv[1], "rb");
    if (f == NULL)
        file_error();
    fseek(f, 0, SEEK_END);
    size_t fsize = (size_t)ftell(f);
    fseek(f, 0, SEEK_SET);

    char* fdata = malloc(fsize);
    if (fdata == NULL)
        file_error();
    if ((fread(fdata, 1, fsize, f) != fsize))
        file_error();
    fclose(f);

    pckt_t* received_pckt = malloc(sizeof(pckt_t));
    char* fstr = fdata;
    char* pdata = NULL;
    int i;
    char c;

    while ((pdata = strstr(fstr, "\"data\": \""))) {
        pdata += strlen("\"data\": \"");
        for (i = 0; i < 1024; i++) {
            c = *pdata;
            if (c == 0)
                break;
            c = upper_case(c);
            if (!hex_check(c))
                break;
            received_pckt->pckt[i] = hex_to_num(c);
            received_pckt->pckt[i] <<= 4;
            pdata += 1;
            c = *pdata;
            if (c == 0)
                break;
            c = upper_case(c);
            if (!hex_check(c))
                break;
            received_pckt->pckt[i] += hex_to_num(c);
            pdata += 1;
        }
        if (i > 0) {
            received_pckt->pckt_len = i;
            if (packet_check(received_pckt->pckt, received_pckt->pckt_len)) {
                printf("Received %db long packet\n", received_pckt->pckt_len);
                switch (received_pckt->pckt[0]) {
                case DOWNLINK_PCKT_TYPE_TELEMETRY_1:
                    pckt_proc_DOWNLINK_PCKT_TYPE_TELEMETRY_1(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_TELEMETRY_2:
                    pckt_proc_DOWNLINK_PCKT_TYPE_TELEMETRY_2(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_TELEMETRY_3:
                    pckt_proc_DOWNLINK_PCKT_TYPE_TELEMETRY_3(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_BEACON:
                    pckt_proc_DOWNLINK_PCKT_TYPE_BEACON(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_SPECTRUM_RESULT:
                    pckt_proc_DOWNLINK_PCKT_TYPE_SPECTRUM_RESULT(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_FILE_INFO:
                    pckt_proc_DOWNLINK_PCKT_TYPE_FILE_INFO(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_FILE_FRAGMENT:
                    pckt_proc_DOWNLINK_PCKT_TYPE_FILE_FRAGMENT(received_pckt);
                    break;
                // case DOWNLINK_PCKT_TYPE_RESERVED_SYNC:
                //	tsprintf("\tPacket type: Possible SYNC!!\n");
                //	break;
                default:
                    printf("\tInvalid packet type: %hhu!\n", received_pckt->pckt[0]);
                    break;
                }
            }
        }
        fstr = pdata;
    }


    free(received_pckt);
    free(fdata);
}
