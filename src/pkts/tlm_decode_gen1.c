/*
 * Packet interpreter for Smog-P & ATL-1 pocketqube satellites
 * Copyright (C) 2020 szlldm, Peter Horvath
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#if defined(_WIN32)
#define __USE_MINGW_ANSI_STDIO
#endif

#include "packet_check.h"
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#if defined(BUILD_ATL1) + defined(BUILD_SMOGP) != 1
#error BUILD TARGET NOT DEFINED!
#endif

#define DOWNLINK_PCKT_TYPE_TELEMETRY_1 (1)
#define DOWNLINK_PCKT_TYPE_TELEMETRY_2 (2)
#define DOWNLINK_PCKT_TYPE_TELEMETRY_3 (3)
#define DOWNLINK_PCKT_TYPE_BEACON (4)
#define DOWNLINK_PCKT_TYPE_SPECTRUM_RESULT (5)
#define DOWNLINK_PCKT_TYPE_FILE_INFO (6)
#define DOWNLINK_PCKT_TYPE_FILE_FRAGMENT (7)

#define DOWNLINK_PCKT_TYPE_SMOGP_TELEMETRY_1 (33)
#define DOWNLINK_PCKT_TYPE_SMOGP_TELEMETRY_2 (34)

// ATL-1 specific packet types - MSb == 1
#define DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_1 (129)
#define DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_2 (130)
#define DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_3 (131)

#define DOWNLINK_PCKT_TYPE_RESERVED_SYNC (151) // SYNC_PCKT first byte

#if defined(_WIN32)
time_t timegm(struct tm* timeptr) { return _mkgmtime(timeptr); }

// On Windows, Visual Studio does not define gmtime_r. However, mingw might
// do (or might not do).
#ifndef gmtime_r
struct tm* gmtime_r(const time_t* t, struct tm* r)
{
    // gmtime is threadsafe in windows because it uses TLS
    struct tm* theTm = gmtime(t);
    if (theTm) {
        *r = *theTm;
        return r;
    } else {
        return 0;
    }
}
#endif // gmtime_r
#endif // _WIN32

double helper_ACK_INFO_RSSI_CONVERT(uint8_t u8)
{
    double tmp;

    tmp = u8;
    tmp /= 2.0;
    tmp -= 131.0;

    return tmp;
}

const double Ibat_calibr[4][2] = {
    { 0.0091578069, 305.6611509242 },
    { 0.0092536381, 308.7354923643 },
    { 0.0092357733, 306.997781812 },
    { 0.0092097441, 308.1479044269 },
};

double helper_DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_3_calc_battery_current(int panel,
                                                                      uint16_t adc)
{
    if (panel < 0)
        return NAN;
    if (panel > 3)
        return NAN;
    // 'panel' will be used for compensation by calibration values...

    // const double Urefp = 2.044;
    // const double Uref2p = 1.039;
    // const double Urefm = 0.0;
    // const double Rshunt = 0.034;
    // const double lsb = (Urefp - Urefm) / 65536.0;

    double Ibat;
    // Ibat = (adc*lsb - Uref2p) / (100.0 * Rshunt);
    // kompenzáció értelmezése végett az egyenletet rendezni kell
    // Ibat = adc*(lsb/(100*Rshunt)) - (Uref2p/(100*Rshunt))
    Ibat = adc * Ibat_calibr[panel][0] - Ibat_calibr[panel][1];

    return Ibat / 1000.0;
}

const double resist_correction[4][5][3] = {
    {
        { -0.0006651322, -0.0122560584, 0.751697002 },
        { -0.0001612352, -0.054850064, 0.4956892858 },
        { 0.0005175386, -0.0151862359, 1.584156215 },
        { -0.0005616397, -0.0035293076, 1.2312336719 },
        { -0.0006161208, 0.0329681997, 2.3084111427 },
    },
    {
        { -0.0008810698, -0.0005768639, 1.4462896472 },
        { 0.0002554855, -0.0497453659, 1.2655688326 },
        { -0.0002372522, 0.0156908555, 2.3968181466 },
        { -0.0005689415, 0.0082033445, 2.05142517512 },
        { -0.000324599, 0.0315826991, 2.973868843 },
    },
    {
        { -0.0005846387, -0.0212703774, 1.6960469081 },
        { -0.0005227575, -0.029222141, 0.6464118577 },
        { -0.0003972915, 0.0164286754, 2.2502388179 },
        { 0.0003169663, -0.0174945998, 1.7588053065 },
        { -0.0004615105, 0.0385790667, 2.7849059873 },
    },
    {
        { -0.0006318885, -0.0047932305, 1.6301837316 },
        { -0.000244109, -0.039820197, 1.3241749543 },
        { -0.0005641744, 0.022177914, 2.4338965601 },
        { -0.0004342717, 0.0057748108, 1.9869618945 },
        { -0.0005652303, 0.0447351507, 3.0252586698 },
    },
};

double helper_DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_3_calc_temperature(int panel,
                                                                  uint16_t adc_ref,
                                                                  uint16_t adc,
                                                                  int channel)
{
    if (panel < 0)
        return NAN;
    if (panel > 3)
        return NAN;
    if (channel < 0)
        return NAN;
    if (channel > 4)
        return NAN;
    // 'panel' will be used for compensation by calibration values...

    const double Urefp = 2.046;
    const double Urefm = 0.0;
    const double R0 = 100.0;
    const double Rref = 72.9;
    const double A = 3.9083e-3;
    const double B = -5.775e-7;
    const double lsb = (Urefp - Urefm) / 65536.0;

    double Itemp, Rtemp;
    double Rt;
    double Tpt100;

    Itemp = (adc_ref * lsb) / (100.0 * Rref);
    Rtemp = ((adc * lsb) / (Itemp * 100.0));
    Rtemp = Rtemp - ((Rtemp * Rtemp * resist_correction[panel][channel][0]) +
                     (Rtemp * resist_correction[panel][channel][1]) +
                     (resist_correction[panel][channel][2]));
    Rt = Rref + Rtemp;
    Tpt100 = ((-1.0 * R0 * A) + sqrt((R0 * R0 * A * A) - (4.0 * R0 * B * (R0 - Rt)))) /
             (2.0 * R0 * B);

    return Tpt100;
}

bool hex_check(char c)
{
    if ((c >= '0') && (c <= '9'))
        return true;
    if ((c >= 'A') && (c <= 'F'))
        return true;

    return false;
}

uint8_t hex_to_num(char c)
{
    if ((c >= '0') && (c <= '9'))
        return (uint8_t)(c - '0');
    if ((c >= 'A') && (c <= 'F'))
        return (uint8_t)(c + 10 - 'A');
    return 0;
}

char upper_case(char c)
{
    if (c < 'a')
        return c;
    if (c > 'z')
        return c;
    return c - ('a' - 'A');
}


typedef struct {
    int rssi;
    int pckt_len;
    uint8_t pckt[1024];
} pckt_t;

#ifdef BUILD_SMOGP
static const char* const_mppt_panel_names[6] = { "Bottom", "Front", "Right",
                                                 "Left",   "Back",  "Top" };
#endif
#ifdef BUILD_ATL1
static const char* const_mppt_panel_names[6] = { "Bottom", "Left", "Right",
                                                 "Front",  "Back", "Top" };
#endif

void pckt_proc_DOWNLINK_PCKT_TYPE_TELEMETRY_1(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mTelemetry 1\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint16_t u16;
    uint8_t u8;
    int i, p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];
    double tmpd;

    uint8_t* buf = pckt->pckt;


    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);


    for (i = 0; i < 6; i++) {
        printf("\tMPPT-%d (%s):\n", i + 1, const_mppt_panel_names[i]);

        memcpy(&i32, &buf[p], 4);
        p += 4;

        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        printf("\t\tTimestamp: %s\n", timestr);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = ((int16_t)u16) / 10.0;
        printf("\t\tTemperature: %.1fC°\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tLight sensor: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tInput current: %humA\n", u16);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tInput voltage: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tOutput current: %humA\n", u16);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tOutput voltage: %.3fV\n", tmpd);
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\t\tPanel number: %hhu\n", (uint8_t)((u8 >> 5) & 0x07));
        switch ((u8 >> 3) & 0x03) {
        case 0:
            printf("\t\tAntenna status: closed\n");
            break;
        case 1:
            printf("\t\tAntenna status: open\n");
            break;
        case 2:
            printf("\t\tAntenna status: not monitored\n");
            break;
        case 3:
            printf("\t\tAntenna status: INVALID!\n");
            break;
        }
    }


    printf("\tACK-INFO: ");
    for (i = 0; i < 3; i++) {
        memcpy(&u16, &buf[p], 2);
        p += 2;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("%hu (%.0f dBm RSSI)\t", u16, helper_ACK_INFO_RSSI_CONVERT(u8));
    }
    printf("\n");
}

void pckt_proc_DOWNLINK_PCKT_TYPE_TELEMETRY_2(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mTelemetry 2\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint16_t u16;
    uint8_t u8;
    int i, p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];
    double tmpd;

    uint8_t* buf = pckt->pckt;


    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);

    for (i = 0; i < 2; i++) {
        printf("\tPCU-%d DEP telemetry:\n", i + 1);

        memcpy(&i32, &buf[p], 4);
        p += 4;

        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        printf("\t\tTimestamp: %s\n", timestr);

        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\t\tDeployment Switch 1 status: %d\n", (u8 & 0x80) ? 1 : 0);
        printf("\t\tDeployment Switch 2 status: %d\n", (u8 & 0x40) ? 1 : 0);
        printf("\t\tRemove Before Flight status: %d\n", (u8 & 0x20) ? 1 : 0);
        printf("\t\tPCU deployment status: %d\n", (u8 & 0x08) ? 1 : 0);
        printf("\t\tAntenna deployment status: %d\n", (u8 & 0x04) ? 1 : 0);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tPCU boot counter: %hu\n", u16);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tPCU uptime: %hu minutes\n", u16);
    }

    for (i = 0; i < 2; i++) {
        printf("\tPCU-%d SDC telemetry:\n", i + 1);

        memcpy(&i32, &buf[p], 4);
        p += 4;

        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        printf("\t\tTimestamp: %s\n", timestr);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tSDC1 input current: %humA\n", u16);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tSDC1 output current: %humA\n", u16);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tSDC1 output voltage: %.3fV\n", tmpd);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tSDC2 input current: %humA\n", u16);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tSDC2 output current: %humA\n", u16);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tSDC2 output voltage: %.3fV\n", tmpd);

        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\t\tSDC1 current limiter overcurrent status: %d\n", (u8 & 0x80) ? 1 : 0);
        printf("\t\tSDC1 voltage limiter overvoltage status: %d\n", (u8 & 0x40) ? 1 : 0);
        printf("\t\tSDC2 current limiter overcurrent status: %d\n", (u8 & 0x20) ? 1 : 0);
        printf("\t\tSDC2 voltage limiter overvoltage status: %d\n", (u8 & 0x10) ? 1 : 0);
    }

    for (i = 0; i < 2; i++) {
#ifdef BUILD_ATL1
        printf("\tPCU-%d BAT telemetry: not used on ATL-1\n", i + 1);
        p += 11;
#endif
#ifdef BUILD_SMOGP
        printf("\tPCU-%d BAT telemetry:\n", i + 1);

        memcpy(&i32, &buf[p], 4);
        p += 4;

        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        printf("\t\tTimestamp: %s\n", timestr);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tBattery voltage: %.3fV\n", tmpd);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tBattery charge current: %humA\n", u16);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tBattery discharge current: %humA\n", u16);

        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\t\tBattery charge overcurrent status: %d\n", (u8 & 0x80) ? 1 : 0);
        printf("\t\tBattery charge overvoltage status: %d\n", (u8 & 0x40) ? 1 : 0);
        printf("\t\tBattery discharge overcurrent status: %d\n", (u8 & 0x20) ? 1 : 0);
        printf("\t\tBattery discharge overvoltage status: %d\n", (u8 & 0x10) ? 1 : 0);
        printf("\t\tBattery charge enabled: %d\n", (u8 & 0x08) ? 1 : 0);
        printf("\t\tBattery discharge enabled: %d\n", (u8 & 0x04) ? 1 : 0);
#endif
    }

    for (i = 0; i < 2; i++) {
        printf("\tPCU-%d BUS telemetry:\n", i + 1);

        memcpy(&i32, &buf[p], 4);
        p += 4;

        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        printf("\t\tTimestamp: %s\n", timestr);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        // printf("\t\tUnregulated bus voltage: %.3fV\n", tmpd);
        printf("\t\t\x1b[1m\x1b[33mUnregulated bus voltage: %.3fV\n\x1b[0m", tmpd);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tRegulated bus voltage: %.3fV\n", tmpd);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tOBC 1 current consumption: %humA\n", u16);

        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tOBC 2 current consumption: %humA\n", u16);

        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\t\tOBC 1 current limiter overcurrent status: %d\n", (u8 & 0x80) ? 1 : 0);
        printf("\t\tOBC 2 current limiter overcurrent status: %d\n", (u8 & 0x40) ? 1 : 0);
    }


    printf("\tACK-INFO: ");
    for (i = 0; i < 3; i++) {
        memcpy(&u16, &buf[p], 2);
        p += 2;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("%hu (%.0f dBm RSSI)\t", u16, helper_ACK_INFO_RSSI_CONVERT(u8));
    }
    printf("\n");
}

void pckt_proc_DOWNLINK_PCKT_TYPE_TELEMETRY_3(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mTelemetry 3\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint32_t u32;
    (void)u32;
    uint16_t u16;
    uint8_t u8;
    int i, p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];
    double tmpd;
    int16_t vector[3];

    uint8_t* buf = pckt->pckt;

    uint8_t curr_obc;

    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);


    curr_obc = ((buf[p + 58] & 0x02) ? 1 : 0); // peek forward

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = u16 / 1000.0;
    printf("\tOBC supply voltage: %.3fV\n", tmpd);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    if (curr_obc == 0) {
        printf("\tRTCC-1 Temperature: %hdC°\n", ((int16_t)u16));
    }

    memcpy(&u16, &buf[p], 2);
    p += 2;
    if (curr_obc == 1) {
        printf("\tRTCC-2 Temperature: %hdC°\n", ((int16_t)u16));
    }

    p += 2; // OBC temperature not available

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = ((int16_t)u16) / 10.0;
    printf("\tEPS2 panel A temperature 1: %.1fC°\n", tmpd);
    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = ((int16_t)u16) / 10.0;
    printf("\tEPS2 panel A temperature 2: %.1fC°\n", tmpd);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tCurrent COM data rate = %hhu, TX power level = %hhu\n",
           (uint8_t)((u8 >> 5) & 0x07),
           (uint8_t)((u8 >> 3) & 0x03));

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tCurrent COM TX current consumption: %humA\n", u16);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tCurrent COM RX current consumption: %humA\n", u16);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tCOM 1 overcurrent status: %d\n", (u8 & 0x80) ? 1 : 0);
    printf("\tCOM 1 limiter switch status: %d\n", (u8 & 0x20) ? 1 : 0);
    printf("\tCOM 1 limiter switch override status: %d\n", (u8 & 0x08) ? 1 : 0);
    printf("\tCOM 2 overcurrent status: %d\n", (u8 & 0x40) ? 1 : 0);
    printf("\tCOM 2 limiter switch status: %d\n", (u8 & 0x10) ? 1 : 0);
    printf("\tCOM 2 limiter switch override status: %d\n", (u8 & 0x04) ? 1 : 0);

    if (curr_obc == 1)
        p += 20; // if OBC2 skip first part

    memcpy(&vector[0], &buf[p], 2);
    p += 2;
    memcpy(&vector[1], &buf[p], 2);
    p += 2;
    memcpy(&vector[2], &buf[p], 2);
    p += 2;
    printf("\tMSEN Gyroscope: % 5.0f\t% 5.0f\t% 5.0f\n",
           1.0 * vector[0],
           1.0 * vector[1],
           1.0 * vector[2]);

    memcpy(&vector[0], &buf[p], 2);
    p += 2;
    memcpy(&vector[1], &buf[p], 2);
    p += 2;
    memcpy(&vector[2], &buf[p], 2);
    p += 2;
    printf("\tMSEN Magneto:   % 5.0f\t% 5.0f\t% 5.0f\n",
           1.0 * vector[0],
           1.0 * vector[1],
           1.0 * vector[2]);

    p += 6; // accelerometer not used

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = ((int16_t)u16) / 10.0;
    printf("\tMSEN Temperature: %.1fC°\n", tmpd);

    if (curr_obc == 0)
        p += 20; // if OBC1 skip second part

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (curr_obc == 1) {
        printf("\tMSEN  functional: %d\n", (u8 & 0x40) ? 1 : 0);
        printf("\tFLASH functional: %d\n", (u8 & 0x10) ? 1 : 0);
        printf("\tRTCC  functional: %d\n", (u8 & 0x04) ? 1 : 0);
        printf("\t Current OBC: OBC-2\n");
    } else {
        printf("\tMSEN  functional: %d\n", (u8 & 0x80) ? 1 : 0);
        printf("\tFLASH functional: %d\n", (u8 & 0x20) ? 1 : 0);
        printf("\tRTCC  functional: %d\n", (u8 & 0x08) ? 1 : 0);
        printf("\tCurrent OBC: OBC-1\n");
    }

    printf("\tCurrent COM: COM-%d\n", (u8 & 0x01) ? 2 : 1);


    // COM telemetry
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tLast COM telemetry: %s\n", timestr);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tSWR bridge reading: %hhu\n", u8);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tLast received packet RSSI: %hhd\n", (int8_t)u8);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tSpectrum analyzer status: %hhu\n", u8);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = u16 / 1000.0;
    printf("\tActive COM voltage: %.3fV\n", tmpd);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = ((int16_t)u16) / 10.0;
    printf("\tActive COM temperature: %.1fC°\n", tmpd);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = ((int16_t)u16) / 10.0;
    printf("\tActive COM spectrum analyzer temperature: %.1fC°\n", tmpd);

#ifdef BUILD_ATL1
    p += 32; // TID telemetry not available on ATL-1
#endif
#ifdef BUILD_SMOGP
    // TID telemetry	p += 32;
    for (i = 0; i < 2; i++) {
        memcpy(&i32, &buf[p], 4);
        p += 4;
        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;
        printf("\tTID-%d measurement:\n", (i + 1));
        printf("\t\tTimestamp: %s\n", timestr);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = ((int16_t)u16) / 10.0;
        printf("\t\tTemperature: %.1fC°\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVoltage: %.3fV\n", tmpd);
        u32 = (uint32_t)((buf[p] << 16) + (buf[p + 1] << 8) + (buf[p + 2] << 0));
        p += 3;
        printf("\t\tRadFET-1 voltage: %uuV\n", u32);
        u32 = (uint32_t)((buf[p] << 16) + (buf[p + 1] << 8) + (buf[p + 2] << 0));
        p += 3;
        printf("\t\tRadFET-2 voltage: %uuV\n", u32);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        printf("\t\tMeasurement serial number: %hu\n", u16);
    }
#endif

    printf("\tACK-INFO: ");
    for (i = 0; i < 3; i++) {
        memcpy(&u16, &buf[p], 2);
        p += 2;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("%hu (%.0f dBm RSSI)\t", u16, helper_ACK_INFO_RSSI_CONVERT(u8));
    }
    printf("\n");
}

void pckt_proc_DOWNLINK_PCKT_TYPE_BEACON(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mBeacon\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint16_t u16;
    uint32_t u32;
    uint8_t u8;
    int i, p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];

    uint8_t* buf = pckt->pckt;

    char beacon_msg[81];


    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);

    memcpy(beacon_msg, &buf[p], 80);
    p += 80;
    beacon_msg[80] = 0;
    printf("\tBeacon message: %s\n", beacon_msg);


    printf("\tUP-Link statistics:\n");

    memcpy(&i32, &buf[p], 4);
    p += 4;
    printf("\t\tValid packets: %d\n", i32);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\t\tRX-Error / wrong size: %hu\n", u16);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\t\tRX-Error / golay-failed: %hu\n", u16);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\t\tRX-Error / wrong signature: %hu\n", u16);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\t\tRX-Error / invalid serial: %hu\n", u16);

    memcpy(&u32, &buf[p], 4);
    p += 4;
    printf("\tOBC-COM TRX error statistic: %u\n", u32);


    printf("\tACK-INFO: ");
    for (i = 0; i < 3; i++) {
        memcpy(&u16, &buf[p], 2);
        p += 2;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("%hu (%.0f dBm RSSI)\t", u16, helper_ACK_INFO_RSSI_CONVERT(u8));
    }
    printf("\n");
}

typedef struct {
    int32_t timestamp;
    double startfreq;
    double stepfreq;
    uint8_t rbw;
    uint8_t pckt_index;
    uint8_t pckt_cnt;
    uint16_t measid;
    uint16_t spectrum_len;
    uint8_t* data;
} asap_spectrum_t;

void pckt_proc_DOWNLINK_PCKT_TYPE_SPECTRUM_RESULT(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mSpectrum result\n\x1b[0m");
    if (pckt->pckt_len < 256) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    int p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];

    uint32_t freqs[2];
    uint8_t rbw, pckt_cnt, pckt_index;
    uint16_t spectrum_len, measid;
    double startfreq, stopfreq, stepfreq;

    uint8_t* buf = pckt->pckt;


    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);

    memcpy(&freqs[0], &buf[p], 4);
    p += 4;
    memcpy(&freqs[1], &buf[p], 4);
    p += 4;
    memcpy(&rbw, &buf[p], 1);
    p += 1;
    memcpy(&pckt_index, &buf[p], 1);
    p += 1;
    memcpy(&pckt_cnt, &buf[p], 1);
    p += 1;
    memcpy(&spectrum_len, &buf[p], 2);
    p += 2;

    p += 2;

    memcpy(&measid, &buf[p], 2);
    p += 2;

    startfreq = freqs[0];
    stepfreq = freqs[1];
    startfreq += 224.0 * pckt_index * stepfreq;
    stopfreq = startfreq + (stepfreq * spectrum_len);

    printf("\tMeasured %hu samples between %.6fMHz ... %.6fMHz with %.3fkHz stepsize and "
           "%hhu RBW\n",
           spectrum_len,
           startfreq / 1000000.0,
           stopfreq / 1000000.0,
           stepfreq / 1000.0,
           rbw);
    printf("\tSending fragment %hhu/%hhu of measurement \"%hu\"...\n",
           pckt_index,
           pckt_cnt,
           measid);
}

void pckt_proc_DOWNLINK_PCKT_TYPE_FILE_INFO(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mFile Info\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint8_t u8;
    int i, p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];

    uint8_t* buf = pckt->pckt;

    uint8_t file_id, file_type;
    uint16_t page_addr;
    uint32_t file_size;
    char file_name[11];

    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);

    for (i = 0; i < 5; i++) {
        memcpy(&file_id, &buf[p], 1);
        p += 1;
        memcpy(&file_type, &buf[p], 1);
        p += 1;
        memcpy(&page_addr, &buf[p], 2);
        p += 2;

        file_size = 0;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        file_size += u8;
        file_size <<= 8;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        file_size += u8;
        file_size <<= 8;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        file_size += u8;

        memcpy(&i32, &buf[p], 4);
        p += 4;
        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        memcpy(file_name, &buf[p], 10);
        p += 10;
        file_name[10] = 0;

        printf("\tFile ID=%hhu, type=%hhu, name=\"%s\" size=%u, starting at page %hu. "
               "Timestamp: %s\n",
               file_id,
               file_type,
               file_name,
               file_size,
               page_addr,
               timestr);
    }
}

void pckt_proc_DOWNLINK_PCKT_TYPE_FILE_FRAGMENT(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mFile Fragment\n\x1b[0m");
    if (pckt->pckt_len < 256) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint8_t u8;
    int p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];

    uint8_t* buf = pckt->pckt;

    uint16_t pckt_index, pckt_cnt;
    uint8_t file_type;
    uint16_t page_addr;
    uint32_t file_size;
    char file_name[11];

    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);

    memcpy(&pckt_index, &buf[p], 2);
    p += 2;
    memcpy(&pckt_cnt, &buf[p], 2);
    p += 2;
    memcpy(&file_type, &buf[p], 1);
    p += 1;
    memcpy(&page_addr, &buf[p], 2);
    p += 2;

    file_size = 0;
    memcpy(&u8, &buf[p], 1);
    p += 1;
    file_size += u8;
    file_size <<= 8;
    memcpy(&u8, &buf[p], 1);
    p += 1;
    file_size += u8;
    file_size <<= 8;
    memcpy(&u8, &buf[p], 1);
    p += 1;
    file_size += u8;

    memcpy(&i32, &buf[p], 4);
    p += 4;
    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    memcpy(file_name, &buf[p], 10);
    p += 10;
    file_name[10] = 0;

    printf("\tDownloading %hu/%hu fragment of the file \"%s\" with type=%hhu, size=%u at "
           "page %hu. Timestamp: %s\n",
           pckt_index,
           pckt_cnt,
           file_name,
           file_type,
           file_size,
           page_addr,
           timestr);

    if ((pckt_cnt == 0) || (pckt_index >= pckt_cnt) || ((file_size / 217) > pckt_cnt)) {
        printf("\n\tERROR: inconsistent file parameter(s)!\n");
        return;
    }
}


char* ET1_local_function_mppt_bus_state_interpretter(int8_t x)
{
    switch (x) {
    case 0:
        return "no data available yet";
    case 1:
        return "valid data";
    case -1:
        return "channel number mismatch";
    case -2:
        return "checksum error";
    case -3:
        return "no response";
    case -4:
        return "BUS ERROR";
    default:
        return "invalid status value!";
    }
}

char* ET1_local_function_pcudoz_bus_state_interpretter(int8_t x)
{
    switch (x) {
    case 0:
        return "unknown";
    case 1:
        return "OK";
    case -1:
        return "bit error";
    case -2:
        return "no response";
    case -3:
        return "BUS ERROR";
    default:
        return "invalid status value!";
    }
}


void pckt_proc_DOWNLINK_PCKT_TYPE_SMOGP_TELEMETRY_1(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mSMOGP-Telemetry 1\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint16_t u16;
    uint8_t u8;
    int i, p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];
    double tmpd;

    uint8_t* buf = pckt->pckt;


    p = 1;

    memcpy(&i32, &buf[p], 4);
    p += 4;
    printf("\tUptime: %d\n", i32);

    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tSystem time: %s\n", timestr);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 == '0')
        printf("\tOBC ID: 1\n");
    else if (u8 == '1')
        printf("\tOBC ID: 2\n");
    else
        printf("\tOBC ID: invalid!\n");

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 == 'E')
        printf("\tOscillator: external\n");
    else if (u8 == 'I')
        printf("\tOscillator: internal\n");
    else
        printf("\tOscillator: invalid!\n");

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 == 'V') {
        memcpy(&i32, &buf[p], 4);
        p += 4;

        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        printf("\tADC results at %s:\n", timestr);

        printf("\tADC results with internal reference:\n");
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVcc: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVbg: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVcore: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVextref: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN1: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN2: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN3: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN4: %.3fV\n", tmpd);

        printf("\tADC results with external reference:\n");
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVcc: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVbg: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVcore: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVextref: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN1: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN2: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN3: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN4: %.3fV\n", tmpd);
    } else {
        printf("\tADC results: not valid!\n");
        p += 37;
    }

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 & 0x80)
        printf("\tSPI-MUX status: functional\n");
    else
        printf("\tSPI-MUX status: not functional\n");
    printf("\t\tMSEN CS pin select: %s\n", ((u8 & 0x40) ? "FAILED" : "OK"));
    printf("\t\tRTCC CS pin select: %s\n", ((u8 & 0x20) ? "FAILED" : "OK"));
    printf("\t\tFLASH CS pin select: %s\n", ((u8 & 0x10) ? "FAILED" : "OK"));
    printf("\t\tAll CS pin deselect: %s\n", ((u8 & 0x08) ? "FAILED" : "OK"));
    printf("\t\tMISO pin test: %s\n", ((u8 & 0x04) ? "FAILED" : "OK"));
    printf("\t\tMOSI pin test: %s\n", ((u8 & 0x02) ? "FAILED" : "OK"));
    printf("\t\tSCLK pin test: %s\n", ((u8 & 0x01) ? "FAILED" : "OK"));

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 == 0)
        printf("\tSPI/FLASH status: not functional\n");
    else
        printf("\tSPI/FLASH status: functional, startcount == %hhu\n", u8);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 == 0)
        printf("\tSPI/MSEN status: not functional\n");
    else
        printf("\tSPI/MSEN status: functional, startcount == %hhu\n", u8);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 == 0)
        printf("\tSPI/RTCC status: not functional\n");
    else
        printf("\tSPI/RTCC status: functional, startcount == %hhu\n", u8);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tRandom number: %02hhX\n", u8);

    for (i = 0; i < 6; i++) {
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\tMPPT-%d (%s) bus status: %s\n",
               (i + 1),
               const_mppt_panel_names[i],
               ET1_local_function_mppt_bus_state_interpretter((int8_t)u8));
    }

    for (i = 0; i < 2; i++) {
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\tTID-%d bus status: %s\n",
               (i + 1),
               ET1_local_function_pcudoz_bus_state_interpretter((int8_t)u8));
    }

    for (i = 0; i < 2; i++) {
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\tPCU-%d bus status: %s\n",
               (i + 1),
               ET1_local_function_pcudoz_bus_state_interpretter((int8_t)u8));
    }

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tCurrent COM: COM-%hhu\n", u8);

    memcpy(&i32, &buf[p], 4);
    p += 4;
    printf("\tCOM uptime: %d seconds\n", i32);


    static const int const_tx_pwr_level_to_mw[] = { 10, 11, 12, 13, 14, 15, 16, 25,
                                                    29, 33, 38, 42, 46, 50, 75, 100 };

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 > 15) {
        printf("\tCOM-TX power level: INVALID (%hhu)\n", u8);
    } else {
        printf("\tCOM-TX power level: %dmW (%hhu)\n", const_tx_pwr_level_to_mw[u8], u8);
    }

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tCOM-TX Current: %humA\n", u16);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tCOM-RX Current: %humA\n", u16);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = u16 / 1000.0;
    printf("\tCOM-TX Voltage Drop: %.3fV\n", tmpd);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tScheduled Spectrum Analysis queue: %hu request(s)\n", u16);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tScheduled File Download queue: %hu request(s)\n", u16);


    int32_t emm_rc, emm_sleep;
    uint8_t emm_mp;
    memcpy(&u8, &buf[p], 1);
    p += 1;
    memcpy(&emm_mp, &buf[p], 1);
    p += 1;
    memcpy(&emm_rc, &buf[p], 4);
    p += 4;
    memcpy(&emm_sleep, &buf[p], 4);
    p += 4;
    switch (u8) {
    case 0:
        printf("\x1b[1m\x1b[32m"); // bold green
        printf("\tEnergy Management Mode: normal (morse period: %hhu; radio cycle: "
               "%.1fs; sleep: 0)",
               emm_mp,
               emm_rc / 1e6);
        break;
    case 1:
        printf("\x1b[1m\x1b[32m"); // bold green
        printf("\tEnergy Management Mode: normal, reduced (morse period: %hhu; radio "
               "cycle: %.1fs; sleep: %.1fs)",
               emm_mp,
               emm_rc / 1e6,
               emm_sleep / 1e6);
        break;
    case 2:
        printf("\x1b[1m\x1b[33m"); // bold yellow
        printf("\tEnergy Management Mode: energy saving (NO morse; radio cycle: %.1fs; "
               "sleep: %.1fs",
               emm_rc / 1e6,
               emm_sleep / 1e6);
        break;
    case 3:
        printf("\x1b[1m\x1b[101m"); // red background
        printf("\tEnergy Management Mode: EMERGENCY (NO morse; radio cycle: %.1fs; "
               "sleep: %.1fs",
               emm_rc / 1e6,
               emm_sleep / 1e6);
        break;
    default:
        printf("\x1b[1m\x1b[101m"); // red background
        printf("\tEnergy Management Mode: INVALID!");
        break;
    }
    printf("\x1b[0m");
    printf("\n");

    memcpy(&i32, &buf[p], 4);
    p += 4;
    if (i32 == -1) {
        printf("\tLast Telecommand: none received yet!\n");
    } else {
        printf("\tLast Telecommand: %d seconds ago\n", i32);
    }

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tAutomatic Antenna Opening Attempts: %hu\n", u16);

    int32_t cpui, cpuw;
    memcpy(&u16, &buf[p], 2);
    p += 2;
    memcpy(&cpui, &buf[p], 4);
    p += 4;
    memcpy(&cpuw, &buf[p], 4);
    p += 4;
    printf("\tCPU usage: %uus iddle, %uus work over %hu cycles\n", cpui, cpuw, u16);
    double load, cyc;
    load = (double)cpui / ((double)cpui + (double)cpuw);
    cyc = u16;
    printf("\t\tLoad: %.1f%%\n\t\tIddle/c: %.3fus\n\t\tWork/c: %.3fus\n",
           load * 100.0,
           cpui / cyc,
           cpuw / cyc);

    uint32_t chksum, chksum_prev_diff;
    memcpy(&chksum, &buf[p], 4);
    p += 4;
    memcpy(&chksum_prev_diff, &buf[p], 4);
    p += 4;
    printf("\tOBC-FLASH checksum: %08X (%08X)\n", chksum, chksum_prev_diff);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tScheduled Datalog queue: %hu request(s)\n", u16);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tCurrent Scheduled Datalog: 0x%04hX\n", u16);
}

void pckt_proc_DOWNLINK_PCKT_TYPE_SMOGP_TELEMETRY_2(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mSMOGP-Telemetry 2\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint16_t u16;
    uint8_t u8;
    int i, p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];

    int16_t vector[6];
    float float32;
    double tmpd, dvector[3];

    uint8_t* buf = pckt->pckt;


    p = 1;

    memcpy(&u8, &buf[p], 1);

    if (u8 == 'V') {
        p += 1; // (valid flag)

        memcpy(&i32, &buf[p], 4);
        p += 4;

        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        printf("\tTimestamp: %s\n", timestr);

        memcpy(&float32, &buf[p], 4);
        p += 4;
        tmpd = float32;
        printf("\tTemperature: %.2fC°\n", tmpd);

        for (i = 0; i < 3; i++) {
            memcpy(&float32, &buf[p], 4);
            p += 4;
            dvector[i] = float32;
        }
        printf("\tMSEN Gyroscope:   %f,\t%f,\t%f\n", dvector[0], dvector[1], dvector[2]);

        for (i = 0; i < 3; i++) {
            memcpy(&u16, &buf[p], 2);
            p += 2;
            vector[i] = (int16_t)u16;
        }
        printf("\tMSEN Magneto RAW:   % 5.0f\t% 5.0f\t% 5.0f\n",
               1.0 * vector[0],
               1.0 * vector[1],
               1.0 * vector[2]);

        memcpy(&u8, &buf[p], 1);
        p += 1;

        if (u8 == 'Y') {
            for (i = 0; i < 6; i++) {
                memcpy(&u16, &buf[p], 2);
                p += 2;
                vector[i] = (int16_t)u16;
            }
            printf("\tMSEN Magneto MIN-MAX: [% 5.0f ... % 5.0f], [% 5.0f ... % 5.0f], [% "
                   "5.0f ... % 5.0f]\n",
                   1.0 * vector[0],
                   1.0 * vector[3],
                   1.0 * vector[1],
                   1.0 * vector[4],
                   1.0 * vector[2],
                   1.0 * vector[5]);
        } else {
            printf("\tMSEN Magneto MIN-MAX not valid yet!\n");
            p += 12;
        }

        for (i = 0; i < 3; i++) {
            memcpy(&float32, &buf[p], 4);
            p += 4;
            dvector[i] = float32;
        }
        printf("\tMSEN Magneto Scale:   % 5.0f\t% 5.0f\t% 5.0f\n",
               dvector[0],
               dvector[1],
               dvector[2]);

        for (i = 0; i < 3; i++) {
            memcpy(&float32, &buf[p], 4);
            p += 4;
            dvector[i] = float32;
        }
        printf("\tMSEN Magneto:   % 5.0f\t% 5.0f\t% 5.0f\n",
               dvector[0],
               dvector[1],
               dvector[2]);

    } else {
        p += 64;
        printf("\tNo MSEN data are available!\n");
    }

    printf("\tACK-INFO: ");
    for (i = 0; i < 17; i++) {
        memcpy(&u16, &buf[p], 2);
        p += 2;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("%hu (%.0f dBm RSSI)\t", u16, helper_ACK_INFO_RSSI_CONVERT(u8));
    }
    printf("\n");
}

void pckt_proc_DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_1(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mATL-Telemetry 1\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint16_t u16;
    uint8_t u8;
    int i, p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];
    double tmpd;

    uint8_t* buf = pckt->pckt;

    p = 1;

    memcpy(&i32, &buf[p], 4);
    p += 4;
    printf("\tUptime: %d\n", i32);

    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tSystem time: %s\n", timestr);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 == '0')
        printf("\tOBC ID: 1\n");
    else if (u8 == '1')
        printf("\tOBC ID: 2\n");
    else
        printf("\tOBC ID: invalid!\n");

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 == 'E')
        printf("\tOscillator: external\n");
    else if (u8 == 'I')
        printf("\tOscillator: internal\n");
    else
        printf("\tOscillator: invalid!\n");

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 == 'V') {
        memcpy(&i32, &buf[p], 4);
        p += 4;

        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        printf("\tADC results at %s:\n", timestr);

        printf("\tADC results with internal reference:\n");
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVcc: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVbg: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVcore: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVextref: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN1: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN2: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN3: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN4: %.3fV\n", tmpd);

        printf("\tADC results with external reference:\n");
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVcc: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVbg: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVcore: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tVextref: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN1: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN2: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN3: %.3fV\n", tmpd);
        memcpy(&u16, &buf[p], 2);
        p += 2;
        tmpd = u16 / 1000.0;
        printf("\t\tAN4: %.3fV\n", tmpd);
    } else {
        printf("\tADC results: not valid!\n");
        p += 37;
    }

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 & 0x80)
        printf("\tSPI-MUX status: functional\n");
    else
        printf("\tSPI-MUX status: not functional\n");
    printf("\t\tMSEN CS pin select: %s\n", ((u8 & 0x40) ? "FAILED" : "OK"));
    printf("\t\tRTCC CS pin select: %s\n", ((u8 & 0x20) ? "FAILED" : "OK"));
    printf("\t\tFLASH CS pin select: %s\n", ((u8 & 0x10) ? "FAILED" : "OK"));
    printf("\t\tAll CS pin deselect: %s\n", ((u8 & 0x08) ? "FAILED" : "OK"));
    printf("\t\tMISO pin test: %s\n", ((u8 & 0x04) ? "FAILED" : "OK"));
    printf("\t\tMOSI pin test: %s\n", ((u8 & 0x02) ? "FAILED" : "OK"));
    printf("\t\tSCLK pin test: %s\n", ((u8 & 0x01) ? "FAILED" : "OK"));

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 == 0)
        printf("\tSPI/FLASH status: not functional\n");
    else
        printf("\tSPI/FLASH status: functional, startcount == %hhu\n", u8);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 == 0)
        printf("\tSPI/MSEN status: not functional\n");
    else
        printf("\tSPI/MSEN status: functional, startcount == %hhu\n", u8);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 == 0)
        printf("\tSPI/RTCC status: not functional\n");
    else
        printf("\tSPI/RTCC status: functional, startcount == %hhu\n", u8);

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tRandom number: %02hhX\n", u8);

    for (i = 0; i < 6; i++) {
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\tMPPT-%d (%s) bus status: %s\n",
               (i + 1),
               const_mppt_panel_names[i],
               ET1_local_function_mppt_bus_state_interpretter((int8_t)u8));
    }

    for (i = 0; i < 2; i++) {
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\tACCU-%d bus status: %s\n",
               (i + 1),
               ET1_local_function_pcudoz_bus_state_interpretter((int8_t)u8));
    }

    for (i = 0; i < 2; i++) {
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("\tPCU-%d bus status: %s\n",
               (i + 1),
               ET1_local_function_pcudoz_bus_state_interpretter((int8_t)u8));
    }

    memcpy(&u8, &buf[p], 1);
    p += 1;
    printf("\tCurrent COM: COM-%hhu\n", u8);

    memcpy(&i32, &buf[p], 4);
    p += 4;
    printf("\tCOM uptime: %d seconds\n", i32);


    static const int const_tx_pwr_level_to_mw[] = { 10, 25, 50, 100 };

    memcpy(&u8, &buf[p], 1);
    p += 1;
    if (u8 > 3) {
        printf("\tCOM-TX power level: INVALID (%hhu)\n", u8);
    } else {
        printf("\tCOM-TX power level: %dmW (%hhu)\n", const_tx_pwr_level_to_mw[u8], u8);
    }

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tCOM-TX Current: %humA\n", u16);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tCOM-RX Current: %humA\n", u16);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    tmpd = u16 / 1000.0;
    printf("\tCOM-TX Voltage Drop: %.3fV\n", tmpd);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tScheduled Spectrum Analysis queue: %hu request(s)\n", u16);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tScheduled File Download queue: %hu request(s)\n", u16);


    int32_t emm_rc, emm_sleep;
    uint8_t emm_mp;
    memcpy(&u8, &buf[p], 1);
    p += 1;
    memcpy(&emm_mp, &buf[p], 1);
    p += 1;
    memcpy(&emm_rc, &buf[p], 4);
    p += 4;
    memcpy(&emm_sleep, &buf[p], 4);
    p += 4;
    switch (u8) {
    case 0:
        printf("\x1b[1m\x1b[32m"); // bold green
        printf("\tEnergy Management Mode: normal (morse period: %hhu; radio cycle: "
               "%.1fs; sleep: 0)",
               emm_mp,
               emm_rc / 1e6);
        break;
    case 1:
        printf("\x1b[1m\x1b[32m"); // bold green
        printf("\tEnergy Management Mode: normal, reduced (morse period: %hhu; radio "
               "cycle: %.1fs; sleep: %.1fs)",
               emm_mp,
               emm_rc / 1e6,
               emm_sleep / 1e6);
        break;
    case 2:
        printf("\x1b[1m\x1b[33m"); // bold yellow
        printf("\tEnergy Management Mode: energy saving (NO morse; radio cycle: %.1fs; "
               "sleep: %.1fs",
               emm_rc / 1e6,
               emm_sleep / 1e6);
        break;
    case 3:
        printf("\x1b[1m\x1b[101m"); // red background
        printf("\tEnergy Management Mode: EMERGENCY (NO morse; radio cycle: %.1fs; "
               "sleep: %.1fs",
               emm_rc / 1e6,
               emm_sleep / 1e6);
        break;
    default:
        printf("\x1b[1m\x1b[101m"); // red background
        printf("\tEnergy Management Mode: INVALID!");
        break;
    }
    printf("\x1b[0m");
    printf("\n");

    memcpy(&i32, &buf[p], 4);
    p += 4;
    if (i32 == -1) {
        printf("\tLast Telecommand: none received yet!\n");
    } else {
        printf("\tLast Telecommand: %d seconds ago\n", i32);
    }

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tAutomatic Antenna Opening Attempts: %hu\n", u16);

    int32_t cpui, cpuw;
    memcpy(&u16, &buf[p], 2);
    p += 2;
    memcpy(&cpui, &buf[p], 4);
    p += 4;
    memcpy(&cpuw, &buf[p], 4);
    p += 4;
    printf("\tCPU usage: %uus iddle, %uus work over %hu cycles\n", cpui, cpuw, u16);
    double load, cyc;
    load = (double)cpui / ((double)cpui + (double)cpuw);
    cyc = u16;
    printf("\t\tLoad: %.1f%%\n\t\tIddle/c: %.3fus\n\t\tWork/c: %.3fus\n",
           load * 100.0,
           cpui / cyc,
           cpuw / cyc);

    uint32_t chksum, chksum_prev_diff;
    memcpy(&chksum, &buf[p], 4);
    p += 4;
    memcpy(&chksum_prev_diff, &buf[p], 4);
    p += 4;
    printf("\tOBC-FLASH checksum: %08X (%08X)\n", chksum, chksum_prev_diff);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tScheduled Datalog queue: %hu request(s)\n", u16);

    memcpy(&u16, &buf[p], 2);
    p += 2;
    printf("\tCurrent Scheduled Datalog: 0x%04hX\n", u16);
}

void pckt_proc_DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_2(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mATL-Telemetry 2\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint16_t u16;
    uint8_t u8;
    int i, p;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];

    int16_t vector[6];
    float float32;
    double tmpd, dvector[3];

    uint8_t* buf = pckt->pckt;


    p = 1;

    memcpy(&u8, &buf[p], 1);

    if (u8 == 'V') {
        p += 1; // (valid flag)

        memcpy(&i32, &buf[p], 4);
        p += 4;

        tmp_time = i32;
        strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
        timestr[19] = 0;

        printf("\tTimestamp: %s\n", timestr);

        memcpy(&float32, &buf[p], 4);
        p += 4;
        tmpd = float32;
        printf("\tTemperature: %.2fC°\n", tmpd);

        for (i = 0; i < 3; i++) {
            memcpy(&float32, &buf[p], 4);
            p += 4;
            dvector[i] = float32;
        }
        printf("\tMSEN Gyroscope:   %f,\t%f,\t%f\n", dvector[0], dvector[1], dvector[2]);

        for (i = 0; i < 3; i++) {
            memcpy(&u16, &buf[p], 2);
            p += 2;
            vector[i] = (int16_t)u16;
        }
        printf("\tMSEN Magneto RAW:   % 5.0f\t% 5.0f\t% 5.0f\n",
               1.0 * vector[0],
               1.0 * vector[1],
               1.0 * vector[2]);

        memcpy(&u8, &buf[p], 1);
        p += 1;

        if (u8 == 'Y') {
            for (i = 0; i < 6; i++) {
                memcpy(&u16, &buf[p], 2);
                p += 2;
                vector[i] = (int16_t)u16;
            }
            printf("\tMSEN Magneto MIN-MAX: [% 5.0f ... % 5.0f], [% 5.0f ... % 5.0f], [% "
                   "5.0f ... % 5.0f]\n",
                   1.0 * vector[0],
                   1.0 * vector[3],
                   1.0 * vector[1],
                   1.0 * vector[4],
                   1.0 * vector[2],
                   1.0 * vector[5]);
        } else {
            printf("\tMSEN Magneto MIN-MAX not valid yet!\n");
            p += 12;
        }

        for (i = 0; i < 3; i++) {
            memcpy(&float32, &buf[p], 4);
            p += 4;
            dvector[i] = float32;
        }
        printf("\tMSEN Magneto Scale:   % 5.0f\t% 5.0f\t% 5.0f\n",
               dvector[0],
               dvector[1],
               dvector[2]);

        for (i = 0; i < 3; i++) {
            memcpy(&float32, &buf[p], 4);
            p += 4;
            dvector[i] = float32;
        }
        printf("\tMSEN Magneto:   % 5.0f\t% 5.0f\t% 5.0f\n",
               dvector[0],
               dvector[1],
               dvector[2]);

    } else {
        p += 64;
        printf("\tNo MSEN data are available!\n");
    }

    printf("\tACK-INFO: ");
    for (i = 0; i < 17; i++) {
        memcpy(&u16, &buf[p], 2);
        p += 2;
        memcpy(&u8, &buf[p], 1);
        p += 1;
        printf("%hu (%.0f dBm RSSI)\t", u16, helper_ACK_INFO_RSSI_CONVERT(u8));
    }
    printf("\n");
}

// moved to "main.h":
// double helper_DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_3_calc_battery_current(int panel,
// uint16_t adc) double helper_DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_3_calc_temperature(int
// panel, uint16_t adc_ref, uint16_t adc)


void pckt_proc_DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_3(pckt_t* pckt)
{
    if (pckt == NULL)
        return;
    printf("\tPacket type: \x1b[1m\x1b[32mATL-Telemetry 3\n\x1b[0m");
    if (pckt->pckt_len < 128) {
        printf("\tERROR: too short\n");
        return;
    }

    int32_t i32;
    uint16_t u16, u16_vect[6];
    uint8_t u8;
    int i, p, n;
    struct tm tmp_tm;
    time_t tmp_time;
    char timestr[20];

    uint8_t* buf = pckt->pckt;

    static const char* const_accu_panel_names[] = {
        "porszal (A2)", "kapton", "szalas (SZ2)", "por (K2)"
    };

    p = 1;
    memcpy(&i32, &buf[p], 4);
    p += 4;

    tmp_time = i32;
    strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
    timestr[19] = 0;

    printf("\tTimestamp: %s\n", timestr);

    for (i = 0; i < 4; i++) {
        if (buf[p] == 1) {
            p += 1;

            memcpy(&u8, &buf[p], 1);
            p += 1;

            memcpy(&i32, &buf[p], 4);
            p += 4;

            tmp_time = i32;
            strftime(timestr, 20, "%Y-%m-%d %H:%M:%S", gmtime_r(&tmp_time, &tmp_tm));
            timestr[19] = 0;

            memcpy(&u16, &buf[p], 2);
            p += 2;

            for (n = 0; n < 6; n++) {
                memcpy(&u16_vect[n], &buf[p], 2);
                p += 2;
            }

            double ibat =
                helper_DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_3_calc_battery_current(i, u16);
            double temp[5];
            for (n = 0; n < 5; n++) {
                temp[n] = helper_DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_3_calc_temperature(
                    i, u16_vect[0], u16_vect[n + 1], n);
            }

            printf("\tACCU-%d (%s) measurement: valid at %s [on 1WBus-%hhu]\n\t\tADC "
                   "values: %hu, %hu, %hu, %hu, %hu, %hu, %hu\n\t\tBattery Current: "
                   "%.3fA\n\t\tTemperature 1 (right) : %.2f°C\n\t\tTemperature 2 (Top): "
                   "%.2f°C\n\t\tTemperature 3 (left): %.2f°C\n\t\tTemperature 4 (back): "
                   "%.2f°C\n\t\tTemperature 5 (panel): %.2f°C\n",
                   (i + 1),
                   const_accu_panel_names[i],
                   timestr,
                   (uint8_t)(u8 + 1),
                   u16,
                   u16_vect[0],
                   u16_vect[1],
                   u16_vect[2],
                   u16_vect[3],
                   u16_vect[4],
                   u16_vect[5],
                   ibat,
                   temp[0],
                   temp[1],
                   temp[2],
                   temp[3],
                   temp[4]);
        } else {
            printf("\tACCU-%d (%s) measurement: not valid\n",
                   (i + 1),
                   const_accu_panel_names[i]);
            p += 20;
        }
    }
}


void file_error()
{
    printf("File error\n");
    exit(-1);
}

int main(int argc, char** argv)
{
    if (argc < 2) {
        printf("Usage: %s filename\n", argv[0]);
        exit(-1);
    }

    FILE* f = fopen(argv[1], "rb");
    if (f == NULL)
        file_error();
    fseek(f, 0, SEEK_END);
    size_t fsize = (size_t)ftell(f);
    fseek(f, 0, SEEK_SET);
    if (fsize <= 0)
        file_error();

    char* fdata = malloc(fsize);
    if (fdata == NULL)
        file_error();
    if ((fread(fdata, 1, fsize, f) != fsize))
        file_error();
    fclose(f);

    pckt_t* received_pckt = malloc(sizeof(pckt_t));
    char* fstr = fdata;
    char* pdata = NULL;
    int i;
    char c;

    while ((pdata = strstr(fstr, "\"data\": \""))) {
        pdata += strlen("\"data\": \"");
        for (i = 0; i < 1024; i++) {
            c = *pdata;
            if (c == 0)
                break;
            c = upper_case(c);
            if (!hex_check(c))
                break;
            received_pckt->pckt[i] = hex_to_num(c);
            received_pckt->pckt[i] <<= 4;
            pdata += 1;
            c = *pdata;
            if (c == 0)
                break;
            c = upper_case(c);
            if (!hex_check(c))
                break;
            received_pckt->pckt[i] += hex_to_num(c);
            pdata += 1;
        }
        if (i > 0) {
            received_pckt->pckt_len = i;
            if (packet_check(received_pckt->pckt, received_pckt->pckt_len) >= 0) {
                printf("Received %db long packet\n", received_pckt->pckt_len);
                switch (received_pckt->pckt[0]) {
                case DOWNLINK_PCKT_TYPE_TELEMETRY_1:
                    pckt_proc_DOWNLINK_PCKT_TYPE_TELEMETRY_1(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_TELEMETRY_2:
                    pckt_proc_DOWNLINK_PCKT_TYPE_TELEMETRY_2(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_TELEMETRY_3:
                    pckt_proc_DOWNLINK_PCKT_TYPE_TELEMETRY_3(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_BEACON:
                    pckt_proc_DOWNLINK_PCKT_TYPE_BEACON(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_SPECTRUM_RESULT:
                    pckt_proc_DOWNLINK_PCKT_TYPE_SPECTRUM_RESULT(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_FILE_INFO:
                    pckt_proc_DOWNLINK_PCKT_TYPE_FILE_INFO(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_FILE_FRAGMENT:
                    pckt_proc_DOWNLINK_PCKT_TYPE_FILE_FRAGMENT(received_pckt);
                    break;
#ifdef BUILD_SMOGP
                case DOWNLINK_PCKT_TYPE_SMOGP_TELEMETRY_1:
                    pckt_proc_DOWNLINK_PCKT_TYPE_SMOGP_TELEMETRY_1(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_SMOGP_TELEMETRY_2:
                    pckt_proc_DOWNLINK_PCKT_TYPE_SMOGP_TELEMETRY_2(received_pckt);
                    break;
#endif
#ifdef BUILD_ATL1
                case DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_1:
                    pckt_proc_DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_1(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_2:
                    pckt_proc_DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_2(received_pckt);
                    break;
                case DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_3:
                    pckt_proc_DOWNLINK_PCKT_TYPE_ATL_TELEMETRY_3(received_pckt);
                    break;
#endif
                // case DOWNLINK_PCKT_TYPE_RESERVED_SYNC:
                //	tsprintf("\tPacket type: Possible SYNC!!\n");
                //	break;
                default:
                    printf("\tInvalid packet type: %hhu!\n", received_pckt->pckt[0]);
                    break;
                }
            }
        }
        fstr = pdata;
    }


    free(received_pckt);
    free(fdata);
}
