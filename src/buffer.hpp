/*
 * Copyright 2020 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef BUFFER_HPP
#define BUFFER_HPP

#include "blocks.hpp"
#include <condition_variable>
#include <atomic>
#include <cstdint>
#include <thread>
#include <vector>

class buffer_t
{
public:
    buffer_t(size_t readers, size_t size);

    // returns the size of the buffer in bytes
    size_t size() const { return buffer.size(); }

    // returns the amount of bytes NOT written, zero if fully written
    size_t write(const void* input, size_t length, bool blocking = true);
    void clear();

    template <typename ELEM>
    size_t write(const std::vector<ELEM>& input, bool blocking = true)
    {
        return write(input.data(), input.size() * sizeof(ELEM), blocking);
    }

    void enable(size_t reader, bool enable);
    bool is_enabled(size_t reader);

    // returns the amount of bytes NOT read, zero if fully read
    size_t read(size_t reader, void* output, size_t length, bool blocking = true);

    template <typename ELEM>
    size_t read(size_t reader, std::vector<ELEM>& output, bool blocking = true)
    {
        return read(reader, output.data(), output.size() * sizeof(ELEM), blocking);
    }

    void force_async(bool enable);

protected:
    std::mutex mutex;
    std::condition_variable not_empty;
    std::condition_variable not_full;

    std::vector<uint8_t> buffer;
    size_t write_pos;
    std::vector<size_t> unreads;
    bool forced_async;
};

class buffered_file_sink
{
public:
    buffered_file_sink(file_sink::format_t format,
                       uint32_t sample_rate,
                       size_t buffer_size);

    void open(const char* filename);
    void close();
    bool is_open() const;

    // returns the amount of bytes NOT written, zero if fully written
    size_t write(const void* input, size_t length);
    size_t byte_count() const { return count; }

    template <typename ELEM>
    size_t write(const std::vector<ELEM>& input)
    {
        size_t unwritten = write(input.data(), input.size() * sizeof(ELEM));
        return (unwritten + sizeof(ELEM) - 1) / sizeof(ELEM);
    }

protected:
    const size_t buffer_size;

    buffer_t buffer;
    file_sink sink;
    size_t count;
    std::thread thread;
    std::atomic<bool> running;
    std::string filename;

    void worker();
};

#endif // BUFFER_HPP
